import defaultSettings from './defaultSettings'; // https://umijs.org/config/

import slash from 'slash2';
import webpackPlugin from './plugin.config';
const { pwa, primaryColor } = defaultSettings; // preview.pro.ant.design only do not use in your production ;
// preview.pro.ant.design 专用环境变量，请不要在你的项目中使用它。

const { ANT_DESIGN_PRO_ONLY_DO_NOT_USE_IN_YOUR_PRODUCTION } = process.env;
const isAntDesignProPreview = ANT_DESIGN_PRO_ONLY_DO_NOT_USE_IN_YOUR_PRODUCTION === 'site';
const plugins = [
  [
    'umi-plugin-react',
    {
      antd: true,
      dva: {
        hmr: true,
      },
      locale: {
        // default false
        enable: true,
        // default zh-CN
        default: 'zh-CN',
        // default true, when it is true, will use `navigator.language` overwrite default
        baseNavigator: true,
      },
      dynamicImport: {
        loadingComponent: './components/PageLoading/index',
        webpackChunkName: true,
        level: 3,
      },
      pwa: pwa
        ? {
            workboxPluginMode: 'InjectManifest',
            workboxOptions: {
              importWorkboxFrom: 'local',
            },
          }
        : false, // default close dll, because issue https://github.com/ant-design/ant-design-pro/issues/4665
      // dll features https://webpack.js.org/plugins/dll-plugin/
      // dll: {
      //   include: ['dva', 'dva/router', 'dva/saga', 'dva/fetch'],
      //   exclude: ['@babel/runtime', 'netlify-lambda'],
      // },
    },
  ],
  [
    'umi-plugin-pro-block',
    {
      moveMock: false,
      moveService: false,
      modifyRequest: true,
      autoAddMenu: true,
    },
  ],
]; // 针对 preview.pro.ant.design 的 GA 统计代码

if (isAntDesignProPreview) {
  plugins.push([
    'umi-plugin-ga',
    {
      code: 'UA-72788897-6',
    },
  ]);
  plugins.push([
    'umi-plugin-pro',
    {
      serverUrl: 'https://ant-design-pro.netlify.com',
    },
  ]);
}

export default {
  plugins,
  block: {
    defaultGitUrl: 'https://github.com/ant-design/pro-blocks',
  },
  hash: true,
  targets: {
    ie: 11,
  },
  devtool: isAntDesignProPreview ? 'source-map' : false,
  // umi routes: https://umijs.org/zh/guide/router.html
  // routes: [
  //   {
  //     path: '/',
  //     component: '../layouts/BasicLayout',
  //     Routes: ['src/pages/Authorized'],
  //     authority: ['admin', 'user'],
  //     routes: [
  //       {
  //         path: '/',
  //         name: 'welcome',
  //         icon: 'smile',
  //         component: './Welcome',
  //       },
  //       {
  //         component: './404',
  //       },
  //     ],
  //   },
  //   {
  //     component: './404',
  //   },
  // ],
  routes: [
    // user
    {
      path: '/user',
      component: '../layouts/UserLayout',
      routes: [
        { path: '/user', redirect: '/user/login' },
        { path: '/user/login', name: 'login', component: './User/Login' },
        { path: '/user/register', name: 'register', component: './User/Register' },
        {
          path: '/user/register-result',
          name: 'register.result',
          component: './User/RegisterResult',
        },
        {
          component: '404',
        },
      ],
    },
    // index
    {
      path: '/index',
      component: '../layouts/IndexsLayout',
      routes: [
        // { path: '/index', redirect: '/index/index' },
        { path: '/index', name: 'index', component: './Indexs/Index' },
        { path: '/index/detail', name: 'detail', component: './Indexs/Detail' },
        { path: '/index/detail-show', name: 'detail-show', component: './Indexs/DetailShow' },
        { path: '/index/pages', name: 'yellow-pages', component: './Indexs/PageInfos' },
        { path: '/index/news', name: 'news-show-list', component: './News/NewsListShow' },
        { path: '/index/news/:newsId', hideInMenu: true, component: './News/$newsId.js' },
        { path: '/index/columns/:columnsId', hideInMenu: true, component: './News/$columns.js' },
        { path: '/index/dashboard/analysis', name: 'analysis', component: './Dashboard/Report' },
        {
          component: '404',
        },
      ],
    },
    // personal
    {
      path: '/personal',
      component: '../layouts/PersonalLayout',
      routes: [
        { path: '/personal', redirect: '/personal/homePage' },
        { path: '/personal/homePage', name: 'personal', component: './Personal/HomePage' },
        { path: '/personal/transfer', name: 'transfer', component: './Transfer/TransferPage' },
        {
          component: '404',
        },
      ],
    },
    // app
    {
      path: '/',
      component: '../layouts/BasicLayout',
      Routes: ['src/pages/Authorized'],
      authority: ['admin', 'user'],
      routes: [
        // index
        { path: '/', redirect: '/index' },

        // dashboard
        {
          path: '/dashboard',
          name: 'dashboard',
          icon: 'dashboard',
          routes: [
            {
              path: '/dashboard/analysis',
              name: 'analysis',
              component: './Dashboard/Report',
            },
            // {
            //   path: '/dashboard/monitor',
            //   name: 'monitor',
            //   component: './Dashboard/Monitor',
            // },
            // {
            //   path: '/dashboard/workplace',
            //   name: 'workplace',
            //   component: './Dashboard/Workplace',
            // },
          ],
        },
        {
          name: 'account',
          icon: 'user',
          path: '/account',
          routes: [
            {
              path: '/account/center',
              name: 'center',
              component: './Account/Center/Center',
              routes: [
                {
                  path: '/account/center',
                  redirect: '/account/center/trade-news',
                },
                {
                  path: '/account/center/trade-news',
                  component: './Account/Center/Articles',
                },
                {
                  path: '/account/center/policy-release',
                  component: './Account/Center/Policy',
                },
                {
                  path: '/account/center/risk-warning',
                  component: './Account/Center/RiskWarning',
                },
              ],
            },
            {
              path: '/account/settings',
              name: 'settings',
              component: './Account/Settings/Info',
              routes: [
                {
                  path: '/account/settings',
                  redirect: '/account/settings/base',
                },
                {
                  path: '/account/settings/base',
                  component: './Account/Settings/BaseView',
                },
                {
                  path: '/account/settings/security',
                  component: './Account/Settings/SecurityView',
                },
                {
                  path: '/account/settings/binding',
                  component: './Account/Settings/BindingView',
                },
                {
                  path: '/account/settings/notification',
                  component: './Account/Settings/NotificationView',
                },
              ],
            },
          ],
        },
        // 新闻管理
        {
          name: 'newsManage',
          icon: 'setting',
          // hideInMenu: true,
          path: '/news',
          authority: ['admin'],
          routes: [
            { path: '/news/news-list', name: 'newsList', component: './News/NewsList' },
            { path: '/news/detail/:newsId', hideInMenu: true, component: './News/$newsId.js' },
            { path: '/news/columns-list', name: 'columnsList', component: './News/ColumnsList' },
            { path: '/news/class-list', name: 'classList', component: './News/ClassList' },
            {
              path: '/news/news-detail',
              hideInMenu: true,
              component: './News/NewsDetail',
            },
          ],
        },
        // 黄页管理
        {
          name: 'yellowPageManage',
          icon: 'setting',
          // hideInMenu: true,
          path: '/yellowPage',
          authority: ['admin'],
          routes: [
            {
              path: '/yellowPage/title-list',
              name: 'titleList',
              component: './YellowPage/TitleList',
            },
            { path: '/yellowPage/web-list', name: 'webList', component: './YellowPage/WebList' },
          ],
        },
        // 系统管理
        {
          name: 'systemManage',
          icon: 'setting',
          path: '/system',
          authority: ['admin'],
          routes: [
            {
              path: '/system/company-list',
              name: 'companyManage',
              component: './System/CompanyList',
            },
            { path: '/system/user-list', name: 'userManage', component: './System/UserList' },
            { path: '/system/role-list', name: 'roleManage', component: './System/RoleList' },
            {
              path: '/system/resource-list',
              name: 'resourceManage',
              component: './System/ResourceList',
            },
          ],
        },
        {
          component: '404',
        },
      ],
    },
  ],
  // Theme for antd: https://ant.design/docs/react/customize-theme-cn
  theme: {
    'primary-color': primaryColor,
  },
  define: {
    ANT_DESIGN_PRO_ONLY_DO_NOT_USE_IN_YOUR_PRODUCTION:
      ANT_DESIGN_PRO_ONLY_DO_NOT_USE_IN_YOUR_PRODUCTION || '', // preview.pro.ant.design only do not use in your production ; preview.pro.ant.design 专用环境变量，请不要在你的项目中使用它。
  },
  ignoreMomentLocale: true,
  lessLoaderOptions: {
    javascriptEnabled: true,
  },
  disableRedirectHoist: true,
  cssLoaderOptions: {
    modules: true,
    getLocalIdent: (context, _, localName) => {
      if (
        context.resourcePath.includes('node_modules') ||
        context.resourcePath.includes('ant.design.pro.less') ||
        context.resourcePath.includes('global.less')
      ) {
        return localName;
      }

      const match = context.resourcePath.match(/src(.*)/);

      if (match && match[1]) {
        const antdProPath = match[1].replace('.less', '');
        const arr = slash(antdProPath)
          .split('/')
          .map(a => a.replace(/([A-Z])/g, '-$1'))
          .map(a => a.toLowerCase());
        return `antd-pro${arr.join('-')}-${localName}`.replace(/--/g, '-');
      }

      return localName;
    },
  },
  manifest: {
    basePath: '/',
  },
  chainWebpack: webpackPlugin,

  /*
  proxy: {
    '/server/api/': {
      target: 'https://preview.pro.ant.design/',
      changeOrigin: true,
      pathRewrite: { '^/server': '' },
    },
  },
  */
  proxy: {
    '/api': {
      target: 'http://139.159.149.15:8080',
      // target: 'http://localhost:8080',
      changeOrigin: true,
      pathRewrite: {
        '^/api': '',
      },
    },
  },
};
