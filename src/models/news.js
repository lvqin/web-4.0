import { queryNews,addNews,deleteNews,
  queryDetailNews,recommendNews,newsIndex,
  queryColumns,queryNewsByColumnsId,newsBanner} from '@/services/news';


export default {
  namespace: 'newsManage',

  state: {
    data: {
      list: [],
      pagination: [],
      newsDetail:[],
      recommend:[],
      index:[],
      columnsNewsList:[],
      banner:[]
    },
  },

  effects: {
    *fetch({ payload }, { call, put }) {
      const response = yield call(queryNews, payload);
      const {content, pageable, totalElements, number } = response;
      const pagination = {
        total: totalElements,
        pageSize: pageable.pageSize,
        current: number + 1,
      };
      yield put({
        type: 'save',
        payload: {list: content, pagination},
      });
    },
    *add({ payload, callback }, { call }) {
      yield call(addNews, payload);
      if (callback) callback();
    },
    *delete({ payload, callback }, { call }) {
      yield call(deleteNews, payload);
      if (callback) callback();
    },
    *queryDetail({ payload, callback }, { call,put }) {
      const response = yield call(queryDetailNews, payload);
      const newsDetail = response;
      const recommend = yield call(recommendNews, payload);
      // console.log(recommend);
      yield put({
        type:'save',
        payload:{newsDetail,recommend}
      })
    },
    *newsIndex({ payload, callback }, { call,put }) {
      const index = yield call(newsIndex, payload);
      const recommend = yield call(recommendNews, payload);
      const banner = yield call(newsBanner, payload);
      // console.log(banner);
      yield put({
        type:'save',
        payload:{index,recommend,banner}
      })
    },
    *queryNewsByColumnsId({ payload, callback  }, { call, put }) {
      // console.log(payload);
      const recommend = yield call(recommendNews, payload);
      const columnsNewsList = yield call(queryNewsByColumnsId, payload);
      // console.log(columnsNewsList);
      yield put({
        type: 'save',
        payload:{columnsNewsList,recommend}
      });
      if (callback) callback();
    },
    *recommendNews({ payload, callback }, { call,put }) {
      const response = yield call(recommendNews, payload);
      const recommend = response;
      // console.log(response);
      yield put({type:'save',payload:{recommend}})
    },

  },
  reducers: {
    save(state, action) {
      // console.log(action);
      return {
        ...state,
        data: action.payload,
      };
    },
    // queryDetail(state, action) {
    //   console.log(state);
    //   return {
    //     ...state,
    //     data: action.payload,
    //   };
    // },
  },
  
};
