import { queryStstcData, queryOffenApp, queryNoticeData, queryAppUrls } from '@/services/personal';
import { recommendNews } from '@/services/news';
import { getTrendData, getMonths, isJSON, isArrayFn } from '../utils/utils';
import { setAuthority } from '@/utils/authority';
import { reloadAuthorized } from '@/utils/Authorized';
import jwt_decode from 'jwt-decode';

export default {
  namespace: 'personal',

  state: {
    data: {
      ststcData: {
        declareNum: 0,
        backNum: 0,
        passRate: 0,
      },
      trendData: [],
      offenApp: [],
      infoData: [],
      noticeData: [],
      appUrls: {},
    },
  },

  effects: {
    *fetch(_, { call, put }) {
      let ststcData, trendData, offenApp, infoData, noticeData, appUrls;

      const ststcRsp = yield call(queryStstcData);
      console.log('===============ststcRsp=====================');
      console.log(ststcRsp);
      console.log('====================================');
      if (ststcRsp.code == 200) {
        ststcData = ststcRsp.content;
        let month = getMonths();
        trendData = getTrendData(month, ststcData.trendList);
        console.log('===============trendData=====================');
        console.log(trendData);
        console.log('====================================');
      } else {
        ststcData = {
          declareNum: 0,
          backNum: 0,
          passRate: 0,
        };
        trendData = [];
      }

      const offenAppRsp = yield call(queryOffenApp);
      if (offenAppRsp.code == 200) {
        offenApp = offenAppRsp.content;
      } else {
        offenApp = [];
      }

      infoData = yield call(recommendNews);

      const noticeDataRsp = yield call(queryNoticeData);
      if (noticeDataRsp.code == 200) {
        noticeData = noticeDataRsp.content;
      } else {
        noticeData = [];
      }

      const appUrlsRsp = yield call(queryAppUrls);
      if (appUrlsRsp.code == 200) {
        appUrls = appUrlsRsp.content;
      } else {
        appUrls = {};
      }

      yield put({
        type: 'save',
        payload: {
          ststcData,
          trendData,
          offenApp,
          infoData,
          noticeData,
          appUrls,
        },
      });

      yield put({
        type: 'changeLoginStatus',
        payload: {
          access_token: sessionStorage.getItem('access_token'),
        },
      });
      reloadAuthorized();
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        data: action.payload,
      };
    },

    changeLoginStatus(state, { payload }) {
      try {
        const accessToken = jwt_decode(payload.access_token);
        const { authorities } = accessToken;
        setAuthority(authorities);
      } catch (err) {
        setAuthority([]);
      }
      return {
        ...state,
        status: payload.status,
        type: payload.type,
      };
    },
  },
};
