import { queryTitle,removeTitle,addTitle,updateTitle,queryWeb,removeWeb,addWeb,updateWeb} from '@/services/pageInfo';


export default {
  namespace: 'yellowPageManage',

  state: {
    data: {
      list: [],
      title:[],
      pagination: {},
  
    },
  },

  effects: {
    *queryTitle({ payload }, { call, put }) {
      const response = yield call(queryTitle, payload);
      const {content, pageable, totalElements, number } = response;
      const pagination = {
        total: totalElements,
        pageSize: pageable.pageSize,
        current: number + 1,
      };
      yield put({
        type: 'save',
        payload: {list: content, pagination},
      });
    },
    *addTitle({ payload, callback }, { call }) {
      yield call(addTitle, payload);
      if (callback) callback();
    },
    *deleteTitle({ payload, callback }, { call }) {
        yield call(removeTitle, payload);
        if (callback) callback();
      },
    *updateTitle({ payload, callback }, { call }) {
        yield call(updateTitle, payload);
        if (callback) callback();
      },

      *queryWeb({ payload }, { call, put }) {
        const response = yield call(queryWeb, payload);
        const {content, pageable, totalElements, number } = response;
        const pagination = {
          total: totalElements,
          pageSize: pageable.pageSize,
          current: number + 1,
        };
        //查询类别
        const params = {};
        const title = yield call(queryTitle, params);
        yield put({
          type: 'save',
          payload: {list: content, pagination,title},
        });
      },
      *addWeb({ payload, callback }, { call }) {
        // console.log(payload);
        yield call(addWeb, payload);
        if (callback) callback();
      },
      *deleteWeb({ payload, callback }, { call }) {
          yield call(removeWeb, payload);
          if (callback) callback();
        },
      *updateWeb({ payload, callback }, { call }) {
        console.log(payload);
          yield call(updateWeb, payload);
          if (callback) callback();
        },
    },
  reducers: {
    save(state, action) {
      return {
        ...state,
        data: action.payload,
      };
    },
  },
  
};
