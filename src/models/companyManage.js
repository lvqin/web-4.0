import { queryCompany, removeCompany, addCompany, updateCompany } from '@/services/company';

export default {
  namespace: 'companyManage',

  state: {
    data: {
      list: [],
      pagination: {},
    },
  },

  effects: {
    *fetch({ payload }, { call, put }) {
      console.log('aaa');
      const response = yield call(queryCompany, payload);
      console.log(response);
      const { content, pageable, totalElements, number } = response;
      const pagination = {
        total: totalElements,
        pageSize: pageable.pageSize,
        current: number + 1,
      };
      yield put({
        type: 'save',
        payload: { list: content, pagination },
      });
    },
    *add({ payload, callback }, { call, put }) {
      console.log('add');
      console.log(payload);
      yield call(addCompany, payload);
      if (callback) callback();
    },
    *remove({ payload, callback }, { call, put }) {
      yield call(removeCompany, payload);
      if (callback) callback();
    },
    *update({ payload, callback }, { call, put }) {
      yield call(updateCompany, payload);
      if (callback) callback();
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        data: action.payload,
      };
    },
  },
};
