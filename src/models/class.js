import { queryColumns,queryClass,updateClass} from '@/services/news';


export default {
  namespace: 'classManage',

  state: {
    data: {
      list: [],
      newsColumns:[],
      pagination: {},
    },
  },

  effects: {
    *queryColumns({ payload }, { call, put }) {
      const response = yield call(queryClass, payload);
      // console.log(response);
      const {content, pageable, totalElements, number } = response;
      const pagination = {
        total: totalElements,
        pageSize: pageable.pageSize,
        current: number + 1,
      };
      // 查询栏目
      const params = {};
      const newsColumns = yield call(queryColumns, params);
      // console.log(newsColumns);
      yield put({
        type: 'save',
        payload: {list: content, pagination,newsColumns},
      });
    },
    *update({ payload, callback }, { call }) {
      // console.log(payload);
      yield call(updateClass, payload);
      if (callback) callback();
    },

  },
  reducers: {
    save(state, action) {
      // console.log(action);
      return {
        ...state,
        data: action.payload,
      };
    },
    // queryDetail(state, action) {
    //   console.log(state);
    //   return {
    //     ...state,
    //     data: action.payload,
    //   };
    // },
  },
  
};
