import { queryUser, removeUser, addUser, updateUser } from '@/services/api';
import { queryRole } from '@/services/role';
export default {
  namespace: 'userManage',

  state: {
    data: {
      list: [],
      pagination: {},
      roles: [],
    },
  },

  effects: {
    *fetch({ payload }, { call, put }) {
      // 查询用户
      const response = yield call(queryUser, payload);
      const {content, pageable, totalElements, number} = response;
      const pagination = {
        total: totalElements,
        pageSize: pageable.pageSize,
        current: number + 1,
      }; 
      // 查询角色
      const params = {};
      const roleRsp = yield call(queryRole, params);
      yield put({
        type: 'save',
        payload: {list: content,
                  pagination,
                  roles: roleRsp.content,
                },
      });
    },
    *add({ payload, callback }, { call, put }) {
      yield call(addUser, payload);
      if (callback) callback();
    },
    *remove({ payload, callback }, { call, put }) {
      yield call(removeUser, payload);
      if (callback) callback();
    },
    *update({ payload, callback }, { call, put }) {
      yield call(updateUser, payload);
      if (callback) callback();
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        data: action.payload,
      };
    },
  },
};
