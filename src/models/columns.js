import { queryColumns,deleteColumns,addColumns} from '@/services/news';


export default {
  namespace: 'columnsManage',

  state: {
    data: {
      list: [],
      pagination: {},
    },
  },

  effects: {
    *queryColumns({ payload }, { call, put }) {
      const response = yield call(queryColumns, payload);
      // console.log(response);
      const {content, pageable, totalElements, number } = response;
      const pagination = {
        total: totalElements,
        pageSize: pageable.pageSize,
        current: number + 1,
      };
      yield put({
        type: 'save',
        payload: {list: content, pagination},
      });
    },
    *add({ payload, callback }, { call }) {
      yield call(addColumns, payload);
      if (callback) callback();
    },
    *delete({ payload, callback }, { call }) {
      yield call(deleteColumns, payload);
      if (callback) callback();
    },

  },
  reducers: {
    save(state, action) {
      // console.log(action);
      return {
        ...state,
        data: action.payload,
      };
    },
    // queryDetail(state, action) {
    //   console.log(state);
    //   return {
    //     ...state,
    //     data: action.payload,
    //   };
    // },
  },
  
};
