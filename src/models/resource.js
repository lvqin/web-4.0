import { queryResources, addResource, removeResource, updateResource } from '@/services/resource';
import { queryRole } from '@/services/role';

export default {
  namespace: 'resources',

  state: {
    data: {
      list: [],
      pagination: {},
    },
  },

  effects: {
    *fetch({ payload }, { call, put }) {
      const response = yield call(queryResources, payload);
      const { content, pageable, totalElements, number } = response;
      const pagination = {
        total: totalElements,
        pageSize: pageable.pageSize,
        current: number + 1,
      };

      const params = {};
      const roles = yield call(queryRole, params);

      yield put({
        type: 'save',
        payload: { list: content, pagination, roles: roles.content },
      });
    },
    *add({ payload, callback }, { call }) {
      yield call(addResource, payload);
      if (callback) callback();
    },
    *remove({ payload, callback }, { call }) {
      yield call(removeResource, payload);
      if (callback) callback();
    },
    *update({ payload, callback }, { call }) {
      yield call(updateResource, payload);
      if (callback) callback();
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        data: action.payload,
      };
    },
  },
};
