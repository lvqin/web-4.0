/* eslint-disable camelcase */
import { routerRedux } from 'dva/router';
import { stringify } from 'qs';
import { fakeAccountLogin, getFakeCaptcha } from '@/services/api';
import { setAuthority } from '@/utils/authority';
import { getPageQuery } from '@/utils/utils';
import { reloadAuthorized } from '@/utils/Authorized';
import jwt_decode from 'jwt-decode';

export default {
  namespace: 'login',

  state: {
    status: undefined,
  },

  effects: {
    *login({ payload }, { call, put }) {
      const response = yield call(fakeAccountLogin, payload);
      yield put({
        type: 'changeLoginStatus',
        payload: response,
      });
      // Login successfully
      if (response.access_token != null && response.access_token !== '') {
        reloadAuthorized();
        sessionStorage.setItem('access_token', response.access_token);
        const urlParams = new URL(window.location.href);
        const params = getPageQuery();
        let { redirect } = params;
        if (redirect) {
          const redirectUrlParams = new URL(redirect);
          if (redirectUrlParams.origin === urlParams.origin) {
            redirect = redirect.substr(urlParams.origin.length);
            if (redirect.match(/^\/.*#/)) {
              redirect = redirect.substr(redirect.indexOf('#') + 1);
            }
          } else {
            redirect = null;
          }
        }
        yield put(routerRedux.replace(redirect || '/'));
      }
    },

    *swLogin({ payload }, { call, put }) {
      if (payload.token != null && payload.token !== '') {
        yield put({
          type: 'changeLoginStatus',
          payload: {
            access_token: payload.token,
          },
        });

        reloadAuthorized();
        // 存储
        sessionStorage.setItem('access_token', payload.token);
        const urlParams = new URL(window.location.href);
        const params = getPageQuery();

        yield put(routerRedux.replace('/index'));
      }
    },

    *getCaptcha({ payload }, { call }) {
      yield call(getFakeCaptcha, payload);
    },

    *logout(_, { put }) {
      yield put({
        type: 'changeLoginStatus',
        payload: {
          status: false,
          authorities: 'guest',
        },
      });
      reloadAuthorized();
      sessionStorage.removeItem('access_token');
      const { redirect } = getPageQuery();
      // redirect
      if (window.location.pathname !== '/user/login' && !redirect) {
        yield put(
          routerRedux.replace({
            pathname: '/user/login',
            search: stringify({
              redirect: window.location.href,
            }),
          }),
        );
      }
    },
  },

  reducers: {
    changeLoginStatus(state, { payload }) {
      try {
        const accessToken = jwt_decode(payload.access_token);
        const { authorities } = accessToken;
        setAuthority(authorities);
      } catch (err) {
        setAuthority([]);
      }
      return {
        ...state,
        status: payload.status,
        type: payload.type,
      };
    },
  },
};
