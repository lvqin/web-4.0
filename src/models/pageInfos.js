import { pageInfosData } from '@/services/pageInfo';
export default {
  namespace: 'pageInfos',

  state: {
    data:{
    title:[],
    web0:[],
    web1:[],
    }
  },
  effects: {
    *fetch(_, { call, put }) {
        const response = yield call(pageInfosData);
       const {titleList}=response;
       const {websiteList0}=response;
       const {websiteList1}=response;
       console.log(websiteList0);
       console.log(websiteList1);
        yield put({
          type: 'save',
          payload: {title:titleList,web0:websiteList0,web1:websiteList1},
        });
      },
    },

reducers: {
    save(state,  action ) {
      return {
        ...state,
        data: action.payload,
      };
    },
    clear() {
      return {
       
      };
    },
  },
};
