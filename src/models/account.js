import { updatePassword, updateInformation, getSettings, updateSettings } from '@/services/account';

export default {
  namespace: 'account',

  state: {
    securitySettings: {},
  },

  effects: {
    *fetch(_, { callback, call, put }) {
      const response = yield call(getSettings);
      yield put({
        type: 'changeSecuritySettings',
        payload: response,
      });
      if (callback) callback();
    },
    *updatePassword({ payload, callback }, { call, put }) {
      const response = yield call(updatePassword, payload);
      yield put({
        type: 'changeSecuritySettings',
        payload: response,
      });
      if (callback) callback();
    },
    *updateInformation({ payload, callback }, { call }) {
      yield call(updateInformation, payload);
      if (callback) callback();
    },
    *updateSettings({ payload, callback }, { call, put }) {
      const response = yield call(updateSettings, payload);
      yield put({
        type: 'changeSecuritySettings',
        payload: response,
      });
      if (callback) callback();
    },
  },

  reducers: {
    changeSecuritySettings(state, action) {
      return {
        ...state,
        securitySettings: action.payload || {},
      };
    },
  },
};
