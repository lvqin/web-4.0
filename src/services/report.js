import { stringify } from 'qs';
import request from '@/utils/request';

export async function fakeChartData() {
  return request('/api/report_data');
}

export async function reportPickData(params) {
  return request(`/api/report_pick_data?${stringify(params)}`);
}

// export async function reportSortData(params) {
//   return request(`/api/report_sort_data?${stringify(params)}`);
// }

export async function reportSortData(params) {
  const token = sessionStorage.getItem('access_token');
  
  return request(`/api/SPRING-CLOUD-PROVIDER-MDC/portionData/query?${stringify(params)}`, {
    method: 'POST',
    data: {
     ...params,
    },
    headers: { Authorization: `Bearer ${token}` },
  });
}

export async function getAllData(params) {
  const token = sessionStorage.getItem('access_token');
  
  return request(`/api/SPRING-CLOUD-PROVIDER-MDC/getData?${stringify(params)}`, {
    method: 'POST',
    data: {
     ...params,
    },
    headers: { Authorization: `Bearer ${token}` },
  });
}