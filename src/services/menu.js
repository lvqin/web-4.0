import request from '@/utils/request';

export default async function queryMenu() {
  const token = sessionStorage.getItem('access_token');
  return request('/api/USER-SERVICE/menu/query', {
    method: 'GET',
    headers: { Authorization: `Bearer ${token}` },
  });
}
