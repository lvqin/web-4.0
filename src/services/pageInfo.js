import request from '@/utils/request';
import { stringify } from 'qs';

export async function pageInfosData() {
    const token =  sessionStorage.getItem('access_token');
    return request(`/api/SPRING-CLOUD-PROVIDER-MDC/yellowPage/query`, {
      method: 'POST',
     
      headers: { Authorization: `Bearer ${token}` },
    });
  }

  export async function queryTitle(params) {
    const pageable = { page: params.page, size: params.size };
  const token = sessionStorage.getItem('access_token');
  return request(`/api/SPRING-CLOUD-PROVIDER-MDC/title/query?${stringify(pageable)}`, {
    method: 'POST',
    data: {
      ...params,
    },
    headers: { Authorization: `Bearer ${token}` },
  });
}

  export async function removeTitle(params) {
    const token = sessionStorage.getItem('access_token');
    const id = params.id.join(','); // 批量删除资源，逗号分隔
    return request(`/api/SPRING-CLOUD-PROVIDER-MDC/title/${params.id}`, {
      method: 'DELETE',
      headers: { Authorization: `Bearer ${token}` },
    });
  }
  
  export async function addTitle(params) {
    const token = sessionStorage.getItem('access_token');
    console.debug(token);
    return request(`/api/SPRING-CLOUD-PROVIDER-MDC/title`, {
      method: 'POST',
      data: {
        ...params,
      },
      headers: { Authorization: `Bearer ${token}` },
    });
  }
  
  export async function updateTitle(params = {}) {
    console.log(params);
    const token = sessionStorage.getItem('access_token');
    return request(`/api/SPRING-CLOUD-PROVIDER-MDC/title/${params.body.id}`, {
      method: 'PUT',
      data: {
        ...params.body,
      },
      headers: { Authorization: `Bearer ${token}` },
    });
  }

  export async function queryWeb(params) {
    const pageable = { page: params.page, size: params.size };
  const token = sessionStorage.getItem('access_token');
  return request(`/api/SPRING-CLOUD-PROVIDER-MDC/web/query?${stringify(pageable)}`, {
    method: 'POST',
    data: {
      ...params,
    },
    headers: { Authorization: `Bearer ${token}` },
  });
}

  export async function removeWeb(params) {
    const token = sessionStorage.getItem('access_token');
    const id = params.id.join(','); // 批量删除资源，逗号分隔
    return request(`/api/SPRING-CLOUD-PROVIDER-MDC/web/${params.id}`, {
      method: 'DELETE',
      headers: { Authorization: `Bearer ${token}` },
    });
  }
  
  export async function addWeb(params) {
    const token = sessionStorage.getItem('access_token');
    console.debug("2222");
    console.debug(token);
    return request(`/api/SPRING-CLOUD-PROVIDER-MDC/web`, {
      method: 'POST',
      data: {
        ...params,
      },
      headers: { Authorization: `Bearer ${token}` },
    });
  }
  
  export async function updateWeb(params = {}) {
    const token = sessionStorage.getItem('access_token');
    return request(`/api/SPRING-CLOUD-PROVIDER-MDC/web/${params.body.id}`, {
      method: 'PUT',
      data: {
        ...params.body,
      },
      headers: { Authorization: `Bearer ${token}` },
    });
  }