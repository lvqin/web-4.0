import request from '@/utils/request';

export async function queryProvince() {
  const token = sessionStorage.getItem('access_token');
  return request('/api/USER-SERVICE/geographic/province', {
    method: 'GET',
    headers: { Authorization: `Bearer ${token}` },
  });
}

export async function queryCity(province) {
  const token = sessionStorage.getItem('access_token');
  return request(`/api/USER-SERVICE/geographic/city/${province}`, {
    method: 'GET',
    headers: { Authorization: `Bearer ${token}` },
  });
}
