import { stringify } from 'qs';
import request from '@/utils/request';

export async function queryRole(params) {
  const token = sessionStorage.getItem('access_token');
  const pageable = { page: params.page, size: params.size,sorter:params.sorter };
  return request(`/api/USER-SERVICE/role/query?${stringify(pageable)}`, {
    method: 'POST',
    data: {
      ...params,
    },
    headers: { Authorization: `Bearer ${token}` },
  });
}

export async function removeRole(params) {
  const token = sessionStorage.getItem('access_token');
  const id = params.id.join(','); // 批量删除资源，逗号分隔
  return request(`/api/USER-SERVICE/role/${params.id}`, {
    method: 'DELETE',
    headers: { Authorization: `Bearer ${token}` },
  });
}

export async function addRole(params) {
  const token = sessionStorage.getItem('access_token');
  console.debug(token);
  return request(`/api/USER-SERVICE/role`, {
    method: 'POST',
    data: {
      ...params,
    },
    headers: { Authorization: `Bearer ${token}` },
  });
}

export async function updateRole(params = {}) {
  const token = sessionStorage.getItem('access_token');
  return request(`/api/USER-SERVICE/role/${params.body.id}`, {
    method: 'PUT',
    data: {
      ...params.body,
    },
    headers: { Authorization: `Bearer ${token}` },
  });
}
