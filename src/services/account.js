import request from '@/utils/request';

export async function updatePassword(params = {}) {
  const token = sessionStorage.getItem('access_token');
  return request(`/api/USER-SERVICE/account/settings/security/password`, {
    method: 'PUT',
    data: {
      ...params.body,
    },
    headers: { Authorization: `Bearer ${token}` },
  });
} 

export async function updateInformation(params = {}) {
  const token = sessionStorage.getItem('access_token');
  return request(`/api/USER-SERVICE/account/settings/base`, {
    method: 'PUT',
    data: {
      ...params,
    },
    headers: { Authorization: `Bearer ${token}` },
  });
}

export async function getSettings() {
  const token = sessionStorage.getItem('access_token');
  return request(`/api/USER-SERVICE/account/settings/security`, {
    method: 'GET',
    headers: { Authorization: `Bearer ${token}` },
  });
}

export async function updateSettings(params = {}) {
  const token = sessionStorage.getItem('access_token');
  return request(`/api/USER-SERVICE/account/settings/security`, {
    method: 'PUT',
    data: {
      ...params,
    },
    headers: { Authorization: `Bearer ${token}` },
  });
}
