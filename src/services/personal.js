import { stringify } from 'qs';
import request from '@/utils/request';

export async function queryStstcData() {
  const token = sessionStorage.getItem('access_token');
  return request(`/api/SPRING-CLOUD-PROVIDER-MDC/swpersonal/queryStstcData?opName=孙燕&corpId=4419960RWK&ieFlag=E&startDate=2019-06-01 00:00:00&endDate=2019-06-30 23:59:59`, {
    method: 'GET',
    headers: { Authorization: `Bearer ${token}` },
  });
}

export async function queryOffenApp() {
  const token = sessionStorage.getItem('access_token');
  return request(`/api/SPRING-CLOUD-PROVIDER-MDC/swpersonal/queryOffenApp`, {
    method: 'GET',
    headers: { Authorization: `Bearer ${token}` },
  });
}

export async function queryNoticeData() {
  const token = sessionStorage.getItem('access_token');
  return request(`/api/SPRING-CLOUD-PROVIDER-MDC/swpersonal/queryNoticeData`, {
    method: 'GET',
    headers: { Authorization: `Bearer ${token}` },
  });
}

export async function queryAppUrls() {
  const token = sessionStorage.getItem('access_token');
  return request(`/api/SPRING-CLOUD-PROVIDER-MDC/swpersonal/queryAppUrls`, {
    method: 'GET',
    headers: { Authorization: `Bearer ${token}` },
  });
}

