import request from '@/utils/request';

export async function query() {
  return request('/api/users');
}

export async function queryCurrent() {
  // return request('/api/currentUser');
  const token = sessionStorage.getItem('access_token');
  if (null === token) {
    return {};
  }
  return request(`/api/USER-SERVICE/account`, {
    method: 'GET',
    headers: { Authorization: `Bearer ${token}` },
  });
}
