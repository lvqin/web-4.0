import { stringify } from 'qs';
import request from '@/utils/request';

export async function queryResources(params) {
  const token = sessionStorage.getItem('access_token');
  const pageable = { page: params.page, size: params.size, sorter: params.sorter };
  return request(`/api/USER-SERVICE/resource/query?${stringify(pageable)}`, {
    method: 'POST',
    data: { ...params },
    headers: { Authorization: `Bearer ${token}` },
  });
}

export async function addResource(params) {
  const token = sessionStorage.getItem('access_token');
  return request('/api/USER-SERVICE/resource', {
    method: 'POST',
    data: {
      ...params,
    },
    headers: { Authorization: `Bearer ${token}` },
  });
}

export async function removeResource(params) {
  const token = sessionStorage.getItem('access_token');
  const id = params.id.join(','); // 批量删除资源，逗号分隔
  return request(`/api/USER-SERVICE/resource/${id}`, {
    method: 'DELETE',
    headers: { Authorization: `Bearer ${token}` },
  });
}

export async function updateResource(params = {}) {
  const token = sessionStorage.getItem('access_token');
  return request(`/api/USER-SERVICE/resource/${params.body.id}`, {
    method: 'PUT',
    data: {
      ...params.body,
    },
    headers: { Authorization: `Bearer ${token}` },
  });
}
