import { stringify } from 'qs';
import request from '@/utils/request';

export async function queryProjectNotice() {
  return request('/api/project/notice');
}

export async function queryActivities() {
  return request('/api/activities');
}

export async function queryRule(params) {
  return request(`/api/rule?${stringify(params)}`);
}

export async function removeRule(params) {
  return request('/api/rule', {
    method: 'POST',
    data: {
      ...params,
      method: 'delete',
    },
  });
}

export async function addRule(params) {
  return request('/api/rule', {
    method: 'POST',
    data: {
      ...params,
      method: 'post',
    },
  });
}

export async function updateRule(params = {}) {
  return request(`/api/rule?${stringify(params.query)}`, {
    method: 'POST',
    data: {
      ...params.body,
      method: 'update',
    },
  });
}

export async function queryUser(params) {
  const token = sessionStorage.getItem('access_token');
  const pageable = { page: params.page, size: params.size, sorter: params.sorter };
  return request(`/api/USER-SERVICE/user/query?${stringify(pageable)}`, {
    method: 'POST',
    data: { ...params },
    headers: { Authorization: `Bearer ${token}` },
  });
}

export async function removeUser(params) {
  const token = sessionStorage.getItem('access_token');
  return request(`/api/USER-SERVICE/user/${params.id}`, {
    method: 'DELETE',
    headers: { Authorization: `Bearer ${token}` },
  });
}

export async function addUser(params) {
  const token = sessionStorage.getItem('access_token');
  return request('/api/USER-SERVICE/user', {
    method: 'POST',
    data: params,
    headers: { Authorization: `Bearer ${token}` },
  });
}

export async function updateUser(params = {}) {
  const token = sessionStorage.getItem('access_token');
  return request(`/api/USER-SERVICE/user/${params.body.id}`, {
    method: 'PUT',
    data: {
      ...params.body,
    },
    headers: { Authorization: `Bearer ${token}` },
  });
}

export async function fakeSubmitForm(params) {
  return request('/api/forms', {
    method: 'POST',
    data: params,
  });
}

export async function fakeChartData() {
  const response = request('/api/fake_chart_data');
  const token = sessionStorage.getItem('access_token');

  // response.salesData[0].x = "w";

  return response;
}

export async function fakeSalesData() {
  const token = sessionStorage.getItem('access_token');
  const pageable = {};
  console.log(token);
  return request(`/api/SPRING-CLOUD-PROVIDER-MDC/salesData/query?${stringify(pageable)}`, {
    method: 'POST',
    data: {},
    headers: { Authorization: `Bearer ${token}` },
  });
}
export async function queryTags() {
  return request('/api/tags');
}

export async function queryBasicProfile(id) {
  return request(`/api/profile/basic?id=${id}`);
}

export async function queryAdvancedProfile() {
  return request('/api/profile/advanced');
}

export async function queryFakeList(params) {
  return request(`/api/fake_list?${stringify(params)}`);
}

export async function removeFakeList(params) {
  const { count = 5, ...restParams } = params;
  return request(`/api/fake_list?count=${count}`, {
    method: 'POST',
    data: {
      ...restParams,
      method: 'delete',
    },
  });
}

export async function addFakeList(params) {
  const { count = 5, ...restParams } = params;
  return request(`/api/fake_list?count=${count}`, {
    method: 'POST',
    data: {
      ...restParams,
      method: 'post',
    },
  });
}

export async function updateFakeList(params) {
  const { count = 5, ...restParams } = params;
  return request(`/api/fake_list?count=${count}`, {
    method: 'POST',
    data: {
      ...restParams,
      method: 'update',
    },
  });
}

export async function fakeAccountLogin(params) {
  params.grant_type = 'password';
  params.scope = 'all';
  return request('/api/oauth/token', {
    method: 'POST',
    data: params,
    headers: { Authorization: 'Basic dGVzdF9jbGllbnQ6dGVzdF9zZWNyZXQ=' },
    requestType: 'form',
  });
}

export async function fakeRegister(params) {
  return request('/api/register', {
    method: 'POST',
    data: params,
  });
}

export async function queryNotices(params = {}) {
  const token = sessionStorage.getItem('access_token');
  return request(`/api/USER-SERVICE/notices?${stringify(params)}`, {
    method: 'GET',
    headers: { Authorization: `Bearer ${token}` },
  });
}

export async function getFakeCaptcha(mobile) {
  return request(`/api/captcha?mobile=${mobile}`);
}
