import request from '@/utils/request';
import { stringify } from 'qs';

export async function queryNews(params) {
  const token = sessionStorage.getItem('access_token');
  const pageable = { page: params.page, size: params.size };
  // console.log(pageable);
  return request(`/api/SPRING-CLOUD-PROVIDER-MDC/news/findAll?${stringify(pageable)}`,{
    method: 'POST',
    data: { ...params },
    headers: { Authorization: `Bearer ${token}` },
  });
}

export async function addNews(params) {
  // console.log(params);
  const token = sessionStorage.getItem('access_token');
  return request('/api/SPRING-CLOUD-PROVIDER-MDC/news/update', {
    method: 'POST',
    data: {
      ...params,
    },
    headers: { Authorization: `Bearer ${token}` },
  });
}

export async function deleteNews(params) {
  // console.log(params);
  const token = sessionStorage.getItem('access_token');
  const id = params.id.join(','); 
  return request(`/api/SPRING-CLOUD-PROVIDER-MDC/news/${id}`, {
    method: 'DELETE',
    headers: { Authorization: `Bearer ${token}` },
  });
}
export async function queryDetailNews(params) {
  // console.log(params);
  const token = sessionStorage.getItem('access_token');
  return request(`/api/SPRING-CLOUD-PROVIDER-MDC/news/findById`, {
    method: 'POST',
    data: {
      ...params,
    },
    headers: { Authorization: `Bearer ${token}` },
  });
}
export async function queryNewsByColumnsId(params) {
  // console.log(params);
  const token = sessionStorage.getItem('access_token');
  return request(`/api/SPRING-CLOUD-PROVIDER-MDC/news/columnsNewsList?${stringify(params)}`, {
    method: 'POST',
    // data: {
    //   ...params,
    // },
    headers: { Authorization: `Bearer ${token}` },
  });
}

export async function recommendNews() {
  // console.log(params);
  const token = sessionStorage.getItem('access_token');
  return request(`/api/SPRING-CLOUD-PROVIDER-MDC/news/toSingleWindow`, {
    method: 'POST',
    // data: {
    //   ...params,
    // },
    headers: { Authorization: `Bearer ${token}` },
  });
}
export async function newsIndex() {
  // console.log(params);
  const token = sessionStorage.getItem('access_token');
  return request(`/api/SPRING-CLOUD-PROVIDER-MDC/news/index`, {
    method: 'POST',
    // data: {
    //   ...params,
    // },
    headers: { Authorization: `Bearer ${token}` },
  });
}
export async function newsBanner() {
  // console.log(params);
  const token = sessionStorage.getItem('access_token');
  return request(`/api/SPRING-CLOUD-PROVIDER-MDC/news/newsBanner`, {
    method: 'POST',
    // data: {
    //   ...params,
    // },
    headers: { Authorization: `Bearer ${token}` },
  });
}
export async function queryColumns(params) {
  const pageable = { page: params.page, size: params.size };
  // console.log(params);
  const token = sessionStorage.getItem('access_token');
  return request(`/api/SPRING-CLOUD-PROVIDER-MDC/columns/findAllColumns?${stringify(pageable)}`, {
    method: 'POST',
    data: {
      ...params,
    },
    headers: { Authorization: `Bearer ${token}` },
  });
}
export async function deleteColumns(params) {
  // console.log(params);
  const token = sessionStorage.getItem('access_token');
  const id = params.id.join(','); 
  return request(`/api/SPRING-CLOUD-PROVIDER-MDC/columns/${id}`, {
    method: 'DELETE',
    headers: { Authorization: `Bearer ${token}` },
  });
}
export async function addColumns(params) {
  // console.log(params);
  const token = sessionStorage.getItem('access_token');
  return request('/api/SPRING-CLOUD-PROVIDER-MDC/columns/update', {
    method: 'POST',
    data: {
      ...params,
    },
    headers: { Authorization: `Bearer ${token}` },
  });
}

export async function queryClass(params) {
  const pageable = { page: params.page, size: params.size };
  // console.log(params);
  const token = sessionStorage.getItem('access_token');
  return request(`/api/SPRING-CLOUD-PROVIDER-MDC/columns/findAllClass?${stringify(pageable)}`, {
    method: 'POST',
    data: {
      ...params,
    },
    headers: { Authorization: `Bearer ${token}` },
  });
  
}
export async function updateClass(params) {
  // console.log(params);
  const token = sessionStorage.getItem('access_token');
  return request('/api/SPRING-CLOUD-PROVIDER-MDC/class/update', {
    method: 'POST',
    data: {
      ...params,
    },
    headers: { Authorization: `Bearer ${token}` },
  });
}

