import component from './zh-CN/component';
import globalHeader from './zh-CN/globalHeader';
import menu from './zh-CN/menu';
import pwa from './zh-CN/pwa';
import form from './zh-CN/form';
import login from './zh-CN/login';
import analysis from './zh-CN/analysis';
import settingDrawer from './zh-CN/settingDrawer';
import settings from './zh-CN/settings';
export default {
  'navBar.lang': '语言',
  'layout.user.link.help': '帮助',
  'layout.user.link.privacy': '隐私',
  'layout.user.link.terms': '条款',
  'app.preview.down.block': '下载此页面到本地项目',
  ...globalHeader,
  ...menu,
  ...settingDrawer,
  ...settings,
  ...pwa,
  ...component,
  ...form,
  ...login,
  ...analysis,
};
