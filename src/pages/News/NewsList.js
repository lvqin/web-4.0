import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import { routerRedux } from 'dva/router';
import router from 'umi/router';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Steps,
  Radio,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';

import styles from './News.less';

const FormItem = Form.Item;
const { Step } = Steps;
const { TextArea } = Input;
const { Option } = Select;
const RadioGroup = Radio.Group;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible} = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };

  // const children = [];
  // const roles = [];
  // for (let i = 0; i < roles.length; i++) {
  //   children.push(<Option value={roles[i].id}>{roles[i].name}</Option>);
  // }
  return (
    <Modal
      destroyOnClose
      title="发表文章"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="标题">
        {form.getFieldDecorator('newsTitle', {
          rules: [{ required: true, message: '请输入至少五个字的标题！', min: 5 }],
        })(<Input autoComplete="off" placeholder="请输入" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="作者">
        {form.getFieldDecorator('newsWriter', {
          rules: [{ required: true, message: '请输入至少两个字的作者名称！', min: 2 }],
        })(<Input autoComplete="off" placeholder="请输入" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="新闻内容">
        {form.getFieldDecorator('newsContent', {
          rules: [{ required: true, message: '请输入至少两个字的新闻内容！', min: 2 }],
        })(<Input.TextArea autoComplete="off" placeholder="请输入" />)}
      </FormItem>
    </Modal>
  );
});

/* eslint react/no-multi-comp:0 */
@connect(({ newsManage, loading }) => ({
  data: newsManage.data,
  loading: loading.models.newsManage,
}))

@Form.create()
class NewsList extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    stepFormValues: {},
    page: 0,
    size: 10,
  };

  columns = [
    {
      title: '新闻标题',
      dataIndex: 'newsTitle',
      render: (text, record)=> {
        // var textSubstring = text.substring(0,20)+'...';
        return <a onClick={() => this.queryDetil(record)}>
          {text.length>27 ? text.substring(0,27)+'...' : text}
        </a>;
      }
    },
    // {
    //     title: '作者',
    //     dataIndex: 'newsWriter',
    //     render: (text, record)=> {
    //       return <Fragment>
    //         { text.length>8 ? text.substring(0,8)+'...' : text}
    //       </Fragment>
    //     }
    // },
    {
      title: '来源',
      dataIndex: 'newsSource',
      render: (text, record)=> {
        return <Fragment>
          { text.length>10 ? text.substring(0,10)+'...' : text}
        </Fragment>
      }
    },
    {
      title: '新闻栏目',
      dataIndex: 'newsClass',
    },
    // {
    //     title: '新闻内容',
    //     dataIndex: 'newsContont',
    // },
    {
      title: '时间',
      dataIndex: 'newsTime',
    },
    // {
    //   title: '操作',
    //   render: (text, record) => (
    //     <Fragment>
    //       <a onClick={() => this.queryDetil(record)}>查看详情</a>
    //     </Fragment>
    //   ),
    // },
  ];

  componentDidMount() {
    // console.log("componentDidMount");
    const { dispatch } = this.props;
    dispatch({
      type: 'newsManage/fetch',
      payload: {},
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;
    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});
    const params = {
      page: pagination.current - 1,
      size: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    this.setState({
      page: pagination.current - 1,
      size: pagination.pageSize,
    });
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }
    dispatch({
      type: 'newsManage/fetch',
      payload: params,
    });
  };

  handleFormReset = () => {//重置
    const { form, dispatch } = this.props;
    form.resetFields();
    this.setState({
      formValues: {},
    });
    dispatch({
      type: 'newsManage/fetch',
      payload: {},
    });
  };
  
  toggleForm = () => {//展开
    const { expandForm } = this.state;
    this.setState({
      expandForm: !expandForm,
    });
  };

  queryDetil = (record) =>{//查看详情
    const{newsId} = record;
    const url = '/index/news/'+newsId;
    const h=window.open('about:blank');
    h.location.href=url;
    // this.props.dispatch(routerRedux.push({ 
    //   pathname: 'news-detail',
    //   // pathname: '/news/detail/',
    //   params: record
    // }))
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleSearch = e => {
    e.preventDefault();
    const { dispatch, form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const values = {
        ...fieldsValue,
        updatedAt: fieldsValue.updatedAt && fieldsValue.updatedAt.valueOf(),
      };
      this.setState({
        formValues: values,
      });
      dispatch({
        type: 'newsManage/fetch',
        payload: values,
      });
    });
  };

  handleModalVisible = flag => {//新建
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleAdd = fields => {//新增
    const { dispatch } = this.props;
    const { page, size } = this.state;
    const params = { page, size };
    dispatch({
      type: 'newsManage/add',
      payload: {
        ...fields
      },
      callback: () => {
        // 刷新列表
        dispatch({
          type: 'newsManage/fetch',
          payload: params,
        });
      },
    });
    message.success('添加成功');
    this.handleModalVisible();
  };

  handleDelete = e => {//删除
    // console.log(e);
    const { dispatch } = this.props;
    const { selectedRows, page, size } = this.state;
    const params = { page, size };
    if (selectedRows.length === 0) return;
    switch (e.key) {
      default:
        dispatch({
          type: 'newsManage/delete',
          payload: {
            id: selectedRows.map(row => row.newsId),
          },
          callback: () => {
            this.setState({
              selectedRows: [],
            });
            // 刷新列表
            dispatch({
              type: 'newsManage/fetch',
              payload: params,
            });
          },
        });
        break;
      // default:
      //   break;
    }
  };

  renderSimpleForm() {
    const {
      form: { getFieldDecorator } ,
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="新闻标题">
              {getFieldDecorator('newsTitle')(<Input  autoComplete="off" placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="新闻作者">
              {getFieldDecorator('newsWriter')(<Input  autoComplete="off" placeholder="请输入" />)}
            </FormItem>
          </Col>
          {/* <Col md={8} sm={24}>
            <FormItem label="新闻栏目">
              {getFieldDecorator('newsClass')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col> */}
          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              <a style={{ marginLeft: 8 }} onClick={this.toggleForm}>
                展开 <Icon type="down" />
              </a>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  renderAdvancedForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="新闻标题">
              {getFieldDecorator('newsTitle')(<Input autoComplete="off" placeholder="请输入" />)}
            </FormItem>
          </Col>
          {/* <Col md={8} sm={24}>
            <FormItem label="新闻栏目">
              {getFieldDecorator('newsClass')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col> */}
          <Col md={8} sm={24}>
            <FormItem label="新闻作者">
              {getFieldDecorator('newsWriter')(<Input  autoComplete="off" placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="新闻来源">
              {getFieldDecorator('newsSource')(<Input autoComplete="off" placeholder="请输入" />)}
            </FormItem>
          </Col>
          {/* <Col md={8} sm={24}>
            <FormItem label="状态">
              {getFieldDecorator('newsState')(
                // <Input placeholder="请输入" />
                <Select placeholder="请选择" style={{ width: '100%' }}>
                  <Option value="0">网页抓取</Option>
                  <Option value="1">非抓取</Option>
                  <Option value="10">暂存</Option>
                  <Option value="11">发表</Option>
                  <Option value="3">删除</Option>
                  <Option value="4">未知</Option>
                </Select>
              )}
            </FormItem>
          </Col> */}
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              <a style={{ marginLeft: 8 }} onClick={this.toggleForm}>
                收起 <Icon type="up" />
              </a>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  renderForm() {
    const { expandForm } = this.state;
    return expandForm ? this.renderAdvancedForm() : this.renderSimpleForm();
  }

  render() {
    const { data, loading } = this.props;
    const { selectedRows, modalVisible, updateModalVisible, stepFormValues } = this.state;
    const menu = (
      <Menu onClick={this.handleMenuClick} selectedKeys={[]}>
        <Menu.Item key="remove">删除</Menu.Item>
        <Menu.Item key="approval">批量审批</Menu.Item>
      </Menu>
    );
    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
      listRoles: this.listRoles,
    };
    return (
      <PageHeaderWrapper title="新闻管理">
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderForm()}</div>
            <div className={styles.tableListOperator}>
              <Button icon="plus" type="primary" onClick={() => this.handleModalVisible(true)}>
                  新建
              </Button>
              {selectedRows.length > 0 && (
                <span>
                  <Button onClick={() =>this.handleDelete(selectedRows)}>删除</Button>
                  {/* <Dropdown overlay={menu}>
                     <Button>
                      更多操作 <Icon type="down" />
                    </Button> 
                  </Dropdown> */}
                </span>
              )}
              </div>
              <StandardTable
                rowKey="id"
                selectedRows={selectedRows}
                loading={loading}
                data={data}
                columns={this.columns}
                onSelectRow={this.handleSelectRows}
                onChange={this.handleStandardTableChange}
              />
          </div>
        </Card>
        <CreateForm {...parentMethods} modalVisible={modalVisible} />
      </PageHeaderWrapper>
    );
  }
}

export default NewsList;
