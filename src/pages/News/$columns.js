import React, { PureComponent, Fragment , Component } from 'react';
import { connect } from 'dva';
// import moment from 'moment';
// import router from 'umi/router';
import Link from 'umi/link';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Steps,
  Radio,
  Carousel,
  List, 
  Avatar, 
  Button, 
  Skeleton,
  Pagination ,
} from 'antd';

import styles from './$columns.less';

var colorList = [
  '#235c98',
  '#6189b4',
  '#9fb7d0',
]

@connect(({ newsManage, loading }) => ({
  data: newsManage.data,
  loading: loading.models.newsManage,
  columnsNewsList : newsManage.data.columnsNewsList,
  recommend : newsManage.data.recommend,
}))
class NewsListShow extends PureComponent {
  constructor(props) {
    super(props);
  }

  state = {
    initLoading: true,
  }

  componentDidMount () {
    // console.log(params);
    const { dispatch } = this.props;
    const {params} = this.props.match;
    // console.log(params);
    dispatch({
      type: 'newsManage/queryNewsByColumnsId',
      payload: {
          id:params.columnsId,
          size:8,
          page:0
      },
      callback: () => {
        // console.log("callback");
        this.setState({
          initLoading: false,
        });
      },
    });
  }

  onChange= pageNumber => {
    let anchorElement = document.getElementById("top");// 找到锚点
    // 如果对应id的锚点存在，就跳转到锚点
    if(anchorElement) { anchorElement.scrollIntoView({block: 'start', behavior: 'instant'}); }
    // if(anchorElement) { anchorElement.scrollIntoView({block: 'start', behavior: 'smooth'}); }
    this.setState({
      initLoading: true,
    });
    const { dispatch } = this.props;
    const {params} = this.props.match;
    console.log('Page: ', (pageNumber-1));
    dispatch({
      type: 'newsManage/queryNewsByColumnsId',
      payload: {
          id:params.columnsId,
          size:8,
          page:(pageNumber-1)
      },
      callback: () => {
        // console.log("callback");
        this.setState({
          initLoading: false,
        });
      },
    });
  }

  render() {
    const {initLoading} = this.state;
    const{recommend,columnsNewsList} = this.props;
    const {columnsId,columnsName,newsList,columnsCount} =columnsNewsList;
    // console.log(columnsNewsList);
    return (
      <Fragment>
        <div className = {styles.bodyDiv}>
          <div className = {styles.conn}>
            {/* top */}
            <div id ="top" className = {styles.top}>
              {/* <Link to="/index"><b><span>首页</span></b></Link> */}
              <span><b><span><a href="/index">首页</a></span></b></span>
              <span> > </span>
              {/* <Link to="/index/news"><span>信息服务</span></Link> */}
              <span><a href="/index/news">信息服务</a></span>
              <span> > </span>
              {/* <Link to={`/index/columns/${columnsId}`}><span>{columnsName}</span></Link> */}
              <span><a href={`/index/columns/${columnsId}`}>{columnsName}</a></span>
            </div>
            {/* top */}
            <div className = {styles.info}>
              {/* list */}
              <div className = {styles.list}>
                <List
                loading={initLoading}
                itemLayout="vertical"
                size="large"
                // pagination={{
                //   onChange: page => {
                //     console.log(page);
                //   },
                //   pageSize: 8,
                // }}
                dataSource={newsList}
                renderItem={item => (
                  <List.Item
                    key={item.newsId}
                  >
                    <List.Item.Meta
                    />
                    <Link to={`/index/news/${item.newsId}`} target="_blank">
                      <div className = {styles.list_con}>
                        <div className = {styles.list_con_l}>
                            <p><b><a>{item.newsTitle.substring(0, 32)}</a></b></p>
                            <p>
                              {item.newsContent
                                    .replace(/<[^>]+>/g,"")
                                    .replace(/&nbsp;/ig, "")
                                    .replace(/<[^>]+>/g, '')
                                    .replace(/&nbsp;/gi, '')
                                    .replace(/\s+/g, '')
                                    .substr(0, 150)
                                    .concat('...')}
                            </p>
                        </div>
                        <div className = {styles.list_con_r}>
                          <p>
                            {/* 发表于<br/>  */}
                            {item.newsTime.substr(0, 10)}
                          </p>
                        </div>
                      </div>
                    </Link>
                    {/* {item.content} */}
                  </List.Item>
                )}
              />
              <div>
                <Pagination showQuickJumper defaultCurrent={1} total={parseInt(columnsCount)} onChange={this.onChange} />
                {/* <br />
                <Pagination showQuickJumper defaultCurrent={2} total={500} onChange={this.onChange} disabled /> */}
              </div>
              </div>
              {/* list */}
              {/* sidebar */}
              <div className = {styles.sidebar}>
                <div className = {styles.sidebar_title}>
                  <b><span>每日精选</span></b>
                </div>
                <div className = {styles.sidebar_list}>
                  {JSON.stringify(recommend) !== "{}" ?
                      recommend.map((item,i)=> (
                      <div key={item.newsId} className = {styles.sidebar_con}>
                        <div className={styles.iconDiv} style={{backgroundColor:colorList[i]}}>{i+1}</div>
                        <Link to={'/index/news/'+item.newsId} target="_blank">
                        <div className={styles.textDiv}>
                          <span>&nbsp;&nbsp; <a>{item.newsTitle.substring(0, 13) + '...'}</a> </span>
                        </div>
                          </Link>
                      </div>
                    )):''
                  }
                </div>
              </div>
              {/* sidebar */}
            </div>
          </div>
        </div>
      </Fragment>
      );
  }
}  
export default NewsListShow;
