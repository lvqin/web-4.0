import React, { PureComponent, Fragment, Component } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import router from 'umi/router';
import Link from 'umi/link';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Steps,
  Radio,
  Carousel,
  Tag,
  List,
  Tabs,
  Typography,
} from 'antd';
// import ArticleListContent from '@/components/ArticleListContent';

import styles from './NewsListShow.less';

const size = 'Large';
var colorList = [
  '#235c98',
  '#6189b4',
  '#9fb7d0',
]
const { TabPane } = Tabs;

var tabsKey = '2019003';

@connect(({ newsManage, loading }) => ({
  data: newsManage.data,
  loading: loading.models.newsManage,
  recommend: newsManage.data.recommend,
  index: newsManage.data.index,
  banner: newsManage.data.banner,
}))

class NewsListShow extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      activeKey: {},
      backgroundSize: '100%',
    };
  }

  componentWillMount() {
    const { dispatch } = this.props;
    // console.log(params);
    dispatch({
      type: 'newsManage/newsIndex',
      // payload: {
      //   ...params
      // },
    });
  }

  moreBut = () => {
    const url = '/index/columns/' + tabsKey;
    const h = window.open('about:blank');
    h.location.href = url;
  }

  onTabChange = tabType => {
    tabsKey = tabType;
    // console.log(tabsKey);
  };

  imgLoad = (url) => {
    var img = new Image();
    img.src = url;
    let min = 0;
    // console.log(img.height + ";" + img.width);
    let heightPercent = img.height / 330
    let widthPercnet = img.width / 880
    widthPercnet > heightPercent ? min
      = heightPercent : min = widthPercnet;
    let backgroundSize = (1.5/ min) * 100 + '%';
    // console.log(heightPercent+"=="+widthPercnet);
    // console.log(backgroundSize);
    this.setState({ backgroundSize })
  };
  render() {
    const { index, recommend, banner } = this.props;
    const { backgroundSize } = this.state;
    // console.log(banner);
    // const bannerDivStyle = {
    //   display: 'inline-block',
    //   position: 'relative',
    //   height: '330px',
    //   width:'100%',
    //   background: `url('${bannerImg}')  no-repeat center`,
    //   // backgroundSize:'50%',
    // }

    return (
      <Fragment>
        <div className={styles.bodyDiv}>
          <div className={styles.conn}>
            {/* top */}
            <div className={styles.top}>
              <Link to="/index"><b><span>首页</span></b></Link>
              <span> > </span>
              <Link to="/index/news"><span>信息服务</span></Link>
            </div>
            {/* top */}
            <div className={styles.info}>
              {/* list */}
              <div className={styles.list}>
                <div className={styles.list_banner}>
                  <Carousel autoplay>
                    {JSON.stringify(banner) !== "{}" ?
                      banner.map((item, i) => (
                        <Link to={'/index/news/' + item.newsId} target="_blank" key={item.newsId}>
                          {this.imgLoad(item.newsContent)}
                          <div style={{ height: '330px', background: 'no-repeat center', backgroundImage: 'url(' + item.newsContent + ')', backgroundSize:  backgroundSize  }}>
                            <div className={styles.bannerTitle} >
                              <p><span>{item.newsTitle.substring(0, 32)}</span></p>
                            </div>
                          </div>
                        </Link>
                      )) : ''
                    }
                    {/* <Link to='/'  target="_blank">
                      <div  style={bannerDivStyle}>
                        <div className={styles.bannerTitle}>
                          <p><span>美报告说保护主义推升2020年底前美经济衰退几率...</span></p>
                        </div>
                      </div>
                    </Link> */}
                  </Carousel>
                </div>
                <Tabs
                  className={styles.list}
                  size="Large"
                  onChange={this.onTabChange}
                  tabBarExtraContent={<Button onClick={() => this.moreBut()} >更多></Button>}>
                  <TabPane tab="外贸要闻" key="2019003">
                    <List
                      size="large"
                      // header={<div>Header</div>}
                      // footer={<div>Footer</div>}
                      bordered
                      dataSource={index.newsLists}
                      renderItem={item =>
                        <List.Item>
                          <Link to={item.newsWebsite} target="_blank">
                            <div className={styles.list_con}>
                              <div className={styles.list_con_l}>
                                <p><a><b>
                                  {item.newsTitle.substring(0, 32)}
                                </b></a></p>
                                <p>
                                  {item.newsContent
                                    .replace(/<[^>]+>/g, "")
                                    .replace(/&nbsp;/ig, "")
                                    .replace(/<[^>]+>/g, '')
                                    .replace(/&nbsp;/gi, '')
                                    .replace(/\s+/g, '')
                                    .substr(0, 120)
                                    .concat('...')}
                                </p>
                              </div>
                              <div className={styles.list_con_r}>
                                <p>
                                  {/* 发表于<br/> */}
                                  {item.newsTime.substr(0, 10)}
                                </p>
                              </div>
                            </div>
                          </Link>
                        </List.Item>
                      }
                    />
                  </TabPane>
                  <TabPane tab="政策发布" key="2019002">
                    <List
                      size="large"
                      // header={<div>Header</div>}
                      // footer={<div>Footer</div>}
                      bordered
                      dataSource={index.policyLists}
                      renderItem={item =>
                        <List.Item>
                          <Link to={item.newsWebsite} target="_blank">
                            <div className={styles.list_con}>
                              <div className={styles.list_con_l}>
                                <p><a><b>
                                  {item.newsTitle.substring(0, 32)}
                                </b></a></p>
                                <p>
                                  {item.newsContent
                                    .replace(/<[^>]+>/g, "")
                                    .replace(/&nbsp;/ig, "")
                                    .replace(/<[^>]+>/g, '')
                                    .replace(/&nbsp;/gi, '')
                                    .replace(/\s+/g, '')
                                    .substr(0, 120)
                                    .concat('...')}
                                </p>
                              </div>
                              <div className={styles.list_con_r}>
                                <p>
                                  {/* 发表于<br/> */}
                                  {item.newsTime.substr(0, 10)}
                                </p>
                              </div>
                            </div>
                          </Link>
                        </List.Item>}
                    />
                  </TabPane>
                  <TabPane tab="风险预警" key="2019001">
                    <List
                      size="large"
                      // header={<div>Header</div>}
                      // footer={<div>Footer</div>}
                      bordered
                      dataSource={index.warnLists}
                      renderItem={item =>
                        <List.Item>
                          <Link to={item.newsWebsite} target="_blank">
                            <div className={styles.list_con}>
                              <div className={styles.list_con_l}>
                                <p><a><b>
                                {item.newsTitle.substring(0, 32)}
                                </b></a></p>
                                <p>
                                  {item.newsContent
                                    .replace(/<[^>]+>/g, "")
                                    .replace(/&nbsp;/ig, "")
                                    .replace(/<[^>]+>/g, '')
                                    .replace(/&nbsp;/gi, '')
                                    .replace(/\s+/g, '')
                                    .substr(0, 120)
                                    .concat('...')}
                                </p>
                              </div>
                              <div className={styles.list_con_r}>
                                <p>{item.newsTime.substr(0, 10)}</p>
                              </div>
                            </div>
                          </Link>
                        </List.Item>}
                    />
                  </TabPane>
                </Tabs>
              </div>
              {/* list */}
              {/* sidebar */}
              <div className={styles.sidebar}>
                <div className={styles.sidebar_title}>
                  <b><span>每日精选</span></b>
                </div>
                <div className={styles.sidebar_list}>
                  {JSON.stringify(recommend) !== "{}" ?
                    recommend.map((item, i) => (
                      <div key={item.newsId} className={styles.sidebar_con}>
                        <div className={styles.iconDiv} style={{backgroundColor:colorList[i]}}>{i + 1}</div>
                        <Link to={'/index/news/' + item.newsId} target="_blank">
                          <div className={styles.textDiv}>
                            <span>&nbsp;&nbsp; <a>{item.newsTitle.substring(0, 13) + '...'}</a> </span>
                          </div>
                        </Link>
                      </div>
                    )) : ''
                  }
                </div>
              </div>
              {/* sidebar */}
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}
export default NewsListShow;