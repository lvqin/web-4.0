import React, { PureComponent, Fragment , Component } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import router from 'umi/router';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Steps,
  Radio,
  Carousel,
  Tag,
  List,
  Tabs,
  Typography,
} from 'antd';
// import StandardTable from '@/components/StandardTable';
// import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import { Link } from 'umi';

import styles from './NewsDetail.less';

var colorList = [
  '#235c98',
  '#6189b4',
  '#9fb7d0',
]

/* eslint react/no-multi-comp:0 */
@connect(({ newsManage, loading }) => ({
    data: newsManage.data,
    loading: loading.models.newsManage,
    newsDetail : newsManage.data.newsDetail,
    recommend : newsManage.data.recommend,
  }))
  
  class NewsDetailById extends PureComponent {
    constructor(props) {
      super(props);
    }

    componentDidMount () {
      const { dispatch } = this.props;
      const {params} = this.props.match;
      // console.log(params);
      dispatch({
        type: 'newsManage/queryDetail',
        payload: {
          ...params
        },
      });
    }

    render() {
      const{newsDetail,recommend} = this.props;
      const {newsSource,newsTime,newsContent,newsTitle,newsWriter,newsClass,newsWebsite} = newsDetail;
    //  console.log(newsDetail);
        return (
            <Fragment>
              <div className = {styles.bodyDiv}>
                <div className = {styles.conn}>
                  {/* top */}
                  <div className = {styles.top}>
                    {/* <Link to="/index"><b><span>首页</span></b></Link> */}
                    <span><b><span><a href="/index">首页</a></span></b></span>
                    <span> > </span>
                    {/* <Link to="/index/news"><span>信息服务</span></Link> */}
                    <span><a href="/index/news">信息服务</a></span>
                    <span> > </span>
                    {/* <Link to={`${newsWebsite}`}><span>{newsClass}</span></Link> */}
                    <span><a href={`${newsWebsite}`}> {newsClass} </a></span>
                  </div>
                  {/* top */}
                  <div className = {styles.info}>
                    {/* list */}
                    <div className = {styles.list}>
                      <div className={styles.article}>
                        <div className={styles.title}> 
                          {/* 标题 */}
                          <p>{newsTitle}</p>
                        </div>
                        <div className={styles.info}> 
                          {/* 来源时间等信息等信息 */}
                          <span>文章来源：{ newsSource} </span>
                          &nbsp;&nbsp;
                          <span> {newsTime} </span>
                          &nbsp;&nbsp; 
                          <span>{newsWriter} </span> 
                        </div>
                        <div className={styles.content} dangerouslySetInnerHTML={{__html: newsContent}}> 
                        {/* 内容 */}
                        {/* <div className={styles.content} >
                          {newsContent} */}
                        </div>
                      </div>
                    </div>
                    {/* list */}
                    {/* sidebar */}
                    <div className = {styles.sidebar}>
                      <div className = {styles.sidebar_title}>
                        <b><span>每日精选</span></b>
                      </div>
                      <div className = {styles.sidebar_list}>
                        {JSON.stringify(recommend) !== "{}" ?
                            recommend.map((item,i)=> (
                            <div key={item.newsId} className = {styles.sidebar_con}>
                              <div className={styles.iconDiv} style={{backgroundColor:colorList[i]}}>{i+1}</div>
                              <Link to={'/index/news/'+item.newsId} target="_blank">
                              <div className={styles.textDiv}>
                                <span>&nbsp;&nbsp; <a>{item.newsTitle.substring(0, 13) + '...'}</a> </span>
                              </div>
                                </Link>
                            </div>
                          )):''
                        }
                      </div>
                    </div>
                    {/* sidebar */}
                  </div>
                </div>
              </div>
            </Fragment>
          );
    }
  }  
export default NewsDetailById;