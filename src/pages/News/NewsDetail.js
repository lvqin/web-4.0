import React, { PureComponent, Fragment , Component } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import router from 'umi/router';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Steps,
  Radio,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import styles from './News.less';
import { Link } from 'umi';

var colorList = [
  '#235c98',
  '#6189b4',
  '#9fb7d0',
]

const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

/* eslint react/no-multi-comp:0 */
@connect(({ newsManage, loading }) => ({
    data: newsManage.data,
    loading: loading.models.newsManage,
  }))
  
  class NewsDetail extends PureComponent {
    constructor(props) {
      super(props);
    }
    

    // componentDidMount() {
    //   const {params} = this.props.location;
    //   // console.log(params);
    //   const { dispatch } = this.props;
    //   dispatch({
    //     type: 'newsManage/queryDetail',
    //     payload: {
    //       ...params
    //     },
    //     callback: () => {
    //       const {newsSource,newsTime,newsContent,newsTitle,newsWriter} = this.props.location.params;
    //       console.log(prams);
    //       刷新列表
    //       dispatch({
    //         type: 'newsManage/fetch',
    //         payload: params,
    //       });
    //     },
    //   });
    // }

    render() {
        const {newsSource,newsTime,newsContent,newsTitle,newsWriter} = this.props.location.params;
        return (
            <Fragment>
              <p><Link to='news-list'>返回列表页</Link></p>
              <div className={styles.article}>
                <div className={styles.title}> 
                  {/* 标题 */}
                  <h1>
                    {newsTitle}
                  </h1>
                </div>
                <div className={styles.info}> 
                  {/* 来源时间等信息等信息 */}
                  <span>文章来源：{newsSource} </span>&nbsp;&nbsp;
                  <span> {newsTime} </span>&nbsp;&nbsp;
                  <span>{newsWriter} </span>
                </div>
                <div className={styles.content} dangerouslySetInnerHTML={{__html: newsContent}}>  
                  {/* 内容 */}
                  {/* {newsContont} */}
                </div>
              </div>
            </Fragment>
          );
    }
  }  
export default NewsDetail;