import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import { routerRedux } from 'dva/router';
import router from 'umi/router';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Steps,
  Radio,
  notification,
  Popconfirm,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';

import styles from './News.less';

const FormItem = Form.Item;
const { Step } = Steps;
const { TextArea } = Input;
const { Option } = Select;
const RadioGroup = Radio.Group;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible} = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };
  return (
    <Modal
      destroyOnClose
      title="新增栏目"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="栏目编号">
        {form.getFieldDecorator('columnsId', {
          rules: [{ required: true, message: '请输入至少七个数字的栏目编号！', min: 7}],
        })(<Input autoComplete="off" placeholder="请输入" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="栏目名称">
        {form.getFieldDecorator('columnsName', {
          rules: [{ required: true, message: '请输入至少两个字的栏目名称！', min: 2 }],
        })(<Input autoComplete="off" placeholder="请输入" />)}
      </FormItem>
    </Modal>
  );
});

@Form.create()
class UpdateForm extends PureComponent {
  static defaultProps = {
    handleUpdate: () => {},
    handleUpdateModalVisible: () => {},
    values: {},
  };
  constructor(props) {
    super(props);
    this.state = {
      formVals: {
        name: props.values.name,
      },
    };
    this.formLayout = {
      labelCol: { span: 7 },
      wrapperCol: { span: 13 },
    };
  }
  render() {
    const { updateModalVisible, handleUpdateModalVisible, values, form, handleUpdate} = this.props;
    const { currentStep, formVals } = this.state;
    const OkUpdateHandle = () => {
      form.validateFields((err, fieldsValue) => {
        if (err) return;
        form.resetFields();
        handleUpdate(fieldsValue);
      });
    };
    return (
      <Modal
        destroyOnClose
        title="修改分类"
        visible={updateModalVisible}
        onOk={OkUpdateHandle}
        onCancel={() => handleUpdateModalVisible()}
      > 
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="栏目编号">
          {form.getFieldDecorator('columnsId', {
            rules: [{ required: true, message: '请输入至少七个数字的栏目编号！', min: 7 }],
            initialValue: values.columnsId,
          })(<Input placeholder="请输入"/>)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="栏目名称">
          {form.getFieldDecorator('columnsName', {
            rules: [{ required: true, message: '请输入至少两个字符的栏目名称！', min: 2 }],
            initialValue: values.columnsName,
          })(<Input placeholder="请输入"/>)}
        </FormItem>
      </Modal>
    );
  }
}

/* eslint react/no-multi-comp:0 */
@connect(({ columnsManage, loading }) => ({
  data: columnsManage.data,
  loading: loading.models.columnsManage,
}))

@Form.create()
class ColumnsList extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    stepFormValues: {},
    page: 0,
    size: 10,
  };
  columns = [
    {
      title: '栏目编号',
      dataIndex: 'columnsId',
    },
    {
      title: '栏目名称',
      dataIndex: 'columnsName',
    },
    {
      title: '操作',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.handleUpdateModalVisible(true,record)}>修改</a>
        </Fragment>
      ),
    },
  ];

  componentDidMount() {
    // console.log("componentDidMount");
    const { dispatch } = this.props;
    dispatch({
      type: 'columnsManage/queryColumns',
      payload: {},
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});
    const params = {
      page: pagination.current - 1,
      size: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    this.setState({
      page: pagination.current - 1,
      size: pagination.pageSize,
    });
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }
    dispatch({
      type: 'columnsManage/queryColumns',
      payload: params,
    });
  };

  handleFormReset = () => {//重置
    const { form, dispatch } = this.props;
    form.resetFields();
    this.setState({
      formValues: {},
    });
    dispatch({
      type: 'columnsManage/queryColumns',
      payload: {},
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleModalVisible = flag => {//新建
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag,record) => {//修改
    this.setState({
      updateModalVisible: !!flag,
      record: record,
    });
  };

  handleAdd = fields => {//新增
    const { dispatch } = this.props;
    const { page, size } = this.state;
    const params = { page, size };
    dispatch({
      type: 'columnsManage/add',
      payload: {
        ...fields
      },
      callback: () => {
        message.success('添加成功');
        // 刷新列表
        dispatch({
          type: 'columnsManage/queryColumns',
          payload: params,
        });
      },
    });
    this.handleModalVisible();
  };

  handleUpdate = fields => {//修改
    const { dispatch } = this.props;
    const { record, page, size } = this.state;
    const params = {
      page,
      size,
    };
    dispatch({
      type: 'columnsManage/add',
      payload: {
        ...fields
      },
      callback: ()=>{
        message.success('修改成功');
        dispatch({
          type: 'columnsManage/queryColumns',
          payload: params,
        });
      },
    });
    this.handleUpdateModalVisible();
  };

  handleDelete = e => {//删除
    // console.log(e);
    const { dispatch } = this.props;
    const { selectedRows, page, size } = this.state;
    const params = { page, size };
    if (selectedRows.length === 0) return;
    switch (e.key) {
      default:
        dispatch({
          type: 'columnsManage/delete',
          payload: {
            id: selectedRows.map(row => row.columnsId),
          },
          callback: () => {
            this.setState({
              selectedRows: [],
            });
            // 刷新列表
            dispatch({
              type: 'columnsManage/queryColumns',
              payload: params,
            });
          },
        });
      break;
      // default:
      //   break;
    }
  };

  renderForm() {
    const { expandForm } = this.state;
  }

  render() {
    const { data, loading } = this.props;
    // console.log(data);
    const { selectedRows, modalVisible, updateModalVisible, stepFormValues } = this.state;
    const menu = (
      <Menu onClick={this.handleMenuClick} selectedKeys={[]}>
        <Menu.Item key="remove">
          删除
        </Menu.Item>
        {/* <Menu.Item key="approval">批量审批</Menu.Item> */}
      </Menu>
    );
    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };
    return (
      <PageHeaderWrapper title="栏目管理">
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderForm()}</div>
            <div className={styles.tableListOperator}>
              <Button icon="plus" type="primary" onClick={() => this.handleModalVisible(true)}>
                  新建
              </Button>
              {selectedRows.length > 0 && (
                <span>
                  <Popconfirm
                    title="是否删除此分类！"
                    onConfirm={() =>this.handleDelete(selectedRows)}
                    okText="是"
                    cancelText="否"
                  >
                  <Button >删除</Button>
                  </Popconfirm>
                  {/* <Dropdown overlay={menu}>
                     <Button>
                      更多操作 <Icon type="down" />
                    </Button> 
                  </Dropdown> */}
                </span>
              )}
              </div>
              <StandardTable
                rowKey="id"
                selectedRows={selectedRows}
                loading={loading}
                data={data}
                columns={this.columns}
                onSelectRow={this.handleSelectRows}
                onChange={this.handleStandardTableChange}
              />
          </div>
        </Card>
        <CreateForm {...parentMethods} modalVisible={modalVisible} />
        <UpdateForm {...updateMethods} updateModalVisible={updateModalVisible} values={this.state.record} />
      </PageHeaderWrapper>
    );
  }
}

export default ColumnsList;
