import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import { routerRedux } from 'dva/router';
import router from 'umi/router';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Steps,
  Radio,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';

import styles from './News.less';

const FormItem = Form.Item;
const { Step } = Steps;
const { TextArea } = Input;
const { Option } = Select;
const RadioGroup = Radio.Group;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

// const CreateForm = Form.create()(props => {
//   const { modalVisible, form, handleAdd, handleModalVisible} = props;
//   const okHandle = () => {
//     form.validateFields((err, fieldsValue) => {
//       if (err) return;
//       form.resetFields();
//       handleAdd(fieldsValue);
//     });
//   };

//   return (
//     <Modal
//       destroyOnClose
//       title="新增栏目"
//       visible={modalVisible}
//       onOk={okHandle}
//       onCancel={() => handleModalVisible()}
//     >
//       <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="栏目编号">
//         {form.getFieldDecorator('newsTitle', {
//           rules: [{ required: true, message: '请输入至少八个数字的栏目编号！', min: 8}],
//         })(<Input autoComplete="off" placeholder="请输入" />)}
//       </FormItem>
//       <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="栏目名称">
//         {form.getFieldDecorator('newsWriter', {
//           rules: [{ required: true, message: '请输入至少两个字的作者名称！', min: 2 }],
//         })(<Input autoComplete="off" placeholder="请输入" />)}
//       </FormItem>
//     </Modal>
//   );
// });

@Form.create()
class UpdateForm extends PureComponent {
  static defaultProps = {
    handleUpdate: () => {},
    handleUpdateModalVisible: () => {},
    getAllColumns: () => {},
    values: {},
  };
  constructor(props) {
    super(props);
    this.state = {
      formVals: {
        name: props.values.name,
      },
    };
    this.formLayout = {
      labelCol: { span: 7 },
      wrapperCol: { span: 13 },
    };
  }
  render() {
    const { updateModalVisible, handleUpdateModalVisible, values, 
      form, handleUpdate,getAllColumns} = this.props;
    const { currentStep, formVals } = this.state;
    // 全部栏目 
    const children = [];
    const columns = getAllColumns().content;
    if(columns){
      for (let i = 0; i < columns.length; i++) {
        children.push(<Option value={columns[i].columnsId} key={i}>{columns[i].columnsName}</Option>);
      }
    }
    // console.log(columns);
    // 已选角色
    var checkedColumns ;
    if(values.classCode){
      checkedColumns = values.classCode;
    }
    const OkUpdateHandle = () => {
      form.validateFields((err, fieldsValue) => {
        if (err) return;
        form.resetFields();
        handleUpdate(fieldsValue);
      });
    };
    return (
      <Modal
        destroyOnClose
        title="修改分类"
        visible={updateModalVisible}
        onOk={OkUpdateHandle}
        onCancel={() => handleUpdateModalVisible()}
      > 
        <FormItem label="栏目" labelCol={{ span: 5 }} wrapperCol={{ span: 15 }}>
            {form.getFieldDecorator('classCode', {
              columns: [
                { required: true, message: '请选择所属栏目！', type: 'array' },
              ],
              initialValue: checkedColumns,
            })(
              <Select defaultValue="lucy"
                      style={{ width: '100%' }}
                      placeholder="请选择">
                {children}
              </Select>
            )}
        </FormItem>
      </Modal>
    );
  }
}

/* eslint react/no-multi-comp:0 */
@connect(({ classManage, loading }) => ({
  data: classManage.data,
  loading: loading.models.classManage,
}))

@Form.create()
class ClassList extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    stepFormValues: {},
    page: 0,
    size: 10,
  };
  columns = [
    {
      title: '分类名称',
      dataIndex: 'className',
      render: (text, record)=> {
        // var textSubstring = text.substring(0,20)+'...';
        return <a onClick={() => this.queryDetil(record)}>
          {text}
        </a>;
      }
    },
    {
      title: '所属栏目编号',
      dataIndex: 'classCode',
    },
    // {
    //   title: '所属栏目编号',
    //   dataIndex: 'classCode',
    // },
    {
      title: '新闻数量',
      dataIndex: 'newsNum',
    },
    {
      title: '操作',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.handleUpdateModalVisible(true,record)}>修改</a>
        </Fragment>
      ),
    },
  ];
  
  componentDidMount() {
    // console.log("componentDidMount");
    const { dispatch } = this.props;
    dispatch({
      type: 'classManage/queryColumns',
      payload: {},
    });
  }

  queryDetil = (record) =>{//查看详情
    console.log(record);
    const {classWebsite} = record;
    const h=window.open('about:blank');
    h.location.href=classWebsite;
  };

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});
    const params = {
      page: pagination.current - 1,
      size: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    this.setState({
      page: pagination.current - 1,
      size: pagination.pageSize,
    });
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }
    dispatch({
      type: 'classManage/queryColumns',
      payload: params,
    });
  };

  handleFormReset = () => {//重置
    const { form, dispatch } = this.props;
    form.resetFields();
    this.setState({
      formValues: {},
    });
    dispatch({
      type: 'classManage/queryColumns',
      payload: {},
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleModalVisible = flag => {//新建
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleAdd = fields => {//新增
    const { dispatch } = this.props;
    const { page, size } = this.state;
    const params = { page, size };
    dispatch({
      type: 'classManage/add',
      payload: {
        ...fields
      },
      callback: () => {
        // 刷新列表
        dispatch({
          type: 'classManage/queryColumns',
          payload: params,
        });
      },
    });
    message.success('添加成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record, page, size } = this.state;
    const params = {
      page,
      size,
    };
    // console.log(record.classCode);
    // console.log(fields.classCode);
    if(record.classCode==fields.classCode){
      message.error('未修改');
      // console.log('未修改');
      return;
    }
    dispatch({
      type: 'classManage/update',
      payload: {
          classId: record.classId,
          classCode: fields.classCode,
      },
      callback: ()=>{
        message.success('修改成功');
        dispatch({
          type: 'classManage/queryColumns',
          payload: params,
        });
      },
    });
    this.handleUpdateModalVisible();
  };

  getAllColumns = () => {
    const {data} = this.props;
    // console.log(data);
    return data.newsColumns;
  };

  handleUpdateModalVisible = (flag,record) => {//修改
    // console.log(record);
    this.setState({
      updateModalVisible: !!flag,
      record: record,
    });
  };

  renderForm() {
    const { expandForm } = this.state;
  }

  render() {
    const { data, loading } = this.props;
    // console.log(data);
    const { selectedRows, modalVisible, updateModalVisible, stepFormValues } = this.state;
    const menu = (
      <Menu onClick={this.handleMenuClick} selectedKeys={[]}>
        <Menu.Item key="remove">删除</Menu.Item>
        <Menu.Item key="approval">批量审批</Menu.Item>
      </Menu>
    );
    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
      // listRoles: this.listRoles,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
      getAllColumns: this.getAllColumns,
    };
    return (
      <PageHeaderWrapper title="栏目管理">
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderForm()}</div>
              <StandardTable
                rowKey="id"
                selectedRows={selectedRows}
                loading={loading}
                data={data}
                columns={this.columns}
                onSelectRow={this.handleSelectRows}
                onChange={this.handleStandardTableChange}
              />
          </div>
        </Card>
        <UpdateForm {...updateMethods} updateModalVisible={updateModalVisible} values={this.state.record} />
      </PageHeaderWrapper>
    );
  }
}

export default ClassList;
