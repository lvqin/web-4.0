import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import { routerRedux } from 'dva/router';
import router from 'umi/router';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Steps,
  Radio,
  notification,
  Popconfirm,
  Upload,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';

import styles from './News.less';
import { file, tsConstructorType } from '@babel/types';

const FormItem = Form.Item;
const { TextArea } = Input;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

//对img进行base64加密
function getBase64(img, callback) {
  console.log('base64');
  console.log(img);
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  //显示图片
  reader.readAsDataURL(img);
}

//上传图标前进行判断文件格式，文件大小
function beforeUpload(file) {
  // const isJPEG = file.type === 'image/jpeg';
  // const isJPG = file.type === 'image/jpg';
  // const isPNG = file.type === 'image/png';
  // const isWEBP = file.type === 'image/webp';
  // const isICO = file.type == 'image/ico';
  const isPIC = file.type === 'image/*';

  // if (!(isJPG || isJPEG || isPNG || isWEBP)) {
  if (!isPIC) {
    message.error('只能上传图片类型!');
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('图标大小不能超过2M!');
  }
  return isPIC && isLt2M;
}

// const CreateForm = Form.create()(props => {
@Form.create()
class CreateForm extends PureComponent {
  // const { modalVisible, form, handleAdd, handleModalVisible,getAllTitle } = props;

  static defaultProps = {
    handleAdd: () => {},
    handleModalVisible: () => {},
    getAllTitle: () => {},

    values: {},
  };

  //改改改
  state = {
    loading: false,
  };

  handleChange = info => {
    if (info.file.status === 'uploading') {
      // console.log(info);
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, imageUrl =>
        // console.log(imageUrl);
        this.setState({
          imageUrl,
          loading: false,
        }),
      );
    }
  };

  constructor(props) {
    super(props);

    this.formLayout = {
      labelCol: { span: 7 },
      wrapperCol: { span: 13 },
    };
  }

  render() {
    const { modalVisible, handleModalVisible, values, form, handleAdd, getAllTitle } = this.props;

    //文件上传按钮
    const uploadButton = (
      <div>
        <Icon type={this.state.loading ? 'loading' : 'plus'} />
        <div className="ant-upload-text">Upload</div>
      </div>
    );
    const { imageUrl } = this.state;

    // 全部类别
    const children = [];
    // const columns = getAllTitle().content;
    var columns = '';
    if (getAllTitle() != null) columns = getAllTitle().content;

    if (columns && columns != '' && columns != null) {
      for (let i = 0; i < columns.length; i++) {
        children.push(
          <Option value={columns[i].id} key={i}>
            {columns[i].titleName}
          </Option>,
        );
      }
    }
    //选择类别
    var checkedColumns;
    if (values.titleId) {
      checkedColumns = values.titleId;
    }
    //所有范围
    const allscope = [];
    allscope.push(
      <Option value={0} key="0">
        国内
      </Option>,
    );
    allscope.push(
      <Option value={1} key="1">
        国外
      </Option>,
    );

    const okHandle = () => {
      form.validateFields((err, fieldsValue) => {
        if (err) return;
        form.resetFields();
        this.setState({
          imageUrl: null,
        });
        handleAdd(fieldsValue);
      });
    };

    const token = sessionStorage.getItem('access_token');

    const prop = {
      listType: 'picture-card',
      className: 'avatar-uploader',
      showUploadList: false,
      action: '/api/SPRING-CLOUD-PROVIDER-MDC/upload',
      accept: '.jpg,.png,.webp,.jpeg,.ico,.bmp',
      name: 'image',
      headers: { Authorization: `Bearer ${token}` },
      beforeUpload: beforeUpload,
      onChange: this.handleChange,
    };

    return (
      <Modal
        destroyOnClose
        title="新建网站"
        visible={modalVisible}
        onOk={okHandle}
        onCancel={() => {
          this.setState({
            imageUrl: null,
          });
          return handleModalVisible();
        }}
      >
        <FormItem label="网站名称" labelCol={{ span: 5 }} wrapperCol={{ span: 15 }}>
          {form.getFieldDecorator('web', {
            rules: [
              { required: true, message: '请输入至少2个字符的网站名称！', min: 2 },
              { required: true, message: '请输入不多于50个字符的网站名称！', max: 50 },
            ],
          })(<Input placeholder="请输入" />)}
        </FormItem>
        <FormItem label="网站地址" labelCol={{ span: 5 }} wrapperCol={{ span: 15 }}>
          {form.getFieldDecorator('url', {
            rules: [
              { required: true, message: '请输入至少5个字符的网站地址！', min: 5 },
              { required: true, message: '请输入不多于100个字符的网站地址！', max: 100 },
            ],
          })(<Input placeholder="请输入" />)}
        </FormItem>
        <FormItem label="类别名称" labelCol={{ span: 5 }} wrapperCol={{ span: 15 }}>
          {form.getFieldDecorator('titleId', {
            rules: [{ required: true, message: '请选择所属类别！' }],
          })(
            <Select style={{ width: '100%' }} placeholder="请选择">
              {children}
            </Select>,
          )}
        </FormItem>
        <FormItem label="所属地区" labelCol={{ span: 5 }} wrapperCol={{ span: 15 }}>
          {form.getFieldDecorator('scope', {
            rules: [{ required: true, message: '请选择所属地区！' }],
          })(
            <Select style={{ width: '100%' }} placeholder="请选择">
              {allscope}
            </Select>,
          )}
        </FormItem>
        <FormItem label="排序标志" labelCol={{ span: 5 }} wrapperCol={{ span: 15 }}>
          {form.getFieldDecorator('sort', {
            rules: [{ required: true, message: '请输入不多于10个字符的排序标志！', max: 10 }],
          })(<Input placeholder="请输入" />)}
        </FormItem>
        <FormItem label="网站图标" labelCol={{ span: 5 }} wrapperCol={{ span: 15 }}>
          {/* {form.getFieldDecorator('imgUrl', { */}
          {form.getFieldDecorator('picUrl', {
            rules: [{ required: false }],
          })(
            <Upload {...prop}>
              {imageUrl || imageUrl != null ? (
                <img src={imageUrl} width="64px" height="64px" />
              ) : (
                uploadButton
              )}
            </Upload>,
          )}
        </FormItem>
      </Modal>
    );
  }
}

@Form.create()
class UpdateForm extends PureComponent {
  //绑定
  componentDidMount() {
    this.props.onRef(this);
  }

  static defaultProps = {
    handleUpdate: () => {},
    handleUpdateModalVisible: () => {},
    getAllTitle: () => {},
    values: {},
  };

  //添加imageUrl
  state = {
    loading: false,
    imageUrl: null,
  };

  handleChange = info => {
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, imageUrl =>
        this.setState({
          imageUrl,
          loading: false,
        }),
      );
    }
  };

  //父组件调用的方法，初始化imageUrl
  init = record => {
    if (record.picUrl && record.picUrl != null && record.picUrl != '') {
      this.state.imageUrl = require(`./pic/${record.picUrl}`);
    }
  };

  constructor(props) {
    super(props);

    this.formLayout = {
      labelCol: { span: 7 },
      wrapperCol: { span: 13 },
    };
  }

  render() {
    const {
      updateModalVisible,
      handleUpdateModalVisible,
      values,
      form,
      handleUpdate,
      getAllTitle,
    } = this.props;

    //上传图片按钮
    const uploadButton = (
      <div>
        <Icon type={this.state.loading ? 'loading' : 'plus'} />
        <div className="ant-upload-text">Upload</div>
      </div>
    );

    const { imageUrl } = this.state;

    //定义图片的相对路径
    var url = '';
    if (values.picUrl && values.picUrl != null && values.picUrl != '') {
      url = require(`./pic/${values.picUrl}`);
    }

    // 全部类别
    const children = [];
    // const columns = getAllTitle().content;
    var columns = '';
    if (getAllTitle() != null) columns = getAllTitle().content;

    if (columns && columns != '' && columns != null) {
      for (let i = 0; i < columns.length; i++) {
        children.push(
          <Option value={columns[i].id} key={i}>
            {columns[i].titleName}
          </Option>,
        );
      }
    }
    //选择类别
    var checkedColumns;
    if (values.titleId) {
      checkedColumns = values.titleId;
    }
    //所有范围
    const allscope = [];
    allscope.push(
      <Option value={0} key="0">
        国内
      </Option>,
    );
    allscope.push(
      <Option value={1} key="1">
        国外
      </Option>,
    );

    const OkUpdateHandle = () => {
      form.validateFields((err, fieldsValue) => {
        if (err) return;
        form.resetFields();
        this.setState({
          imageUrl: null,
        });
        handleUpdate(fieldsValue);
      });
    };

    const token = sessionStorage.getItem('access_token');

    const prop = {
      listType: 'picture-card',
      className: 'avatar-uploader',
      showUploadList: false,
      action: '/api/SPRING-CLOUD-PROVIDER-MDC/upload',
      // action:"https://www.mocky.io/v2/5cc8019d300000980a055e76",
      accept: '.jpg,.png,.webp,.jpeg,.bmp',
      name: 'image',
      headers: { Authorization: `Bearer ${token}` },
      beforeUpload: beforeUpload,
      onChange: this.handleChange,
    };

    return (
      <Modal
        destroyOnClose
        title="修改网站"
        visible={updateModalVisible}
        onOk={OkUpdateHandle}
        onCancel={() => {
          this.setState({
            imageUrl: null,
          });

          //尝试
          handleUpdateModalVisible();
        }}
      >
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="网站编号">
          {form.getFieldDecorator('id', {
            rules: [{ required: true }],
            initialValue: values.id,
          })(<Input disabled />)}
        </FormItem>
        <FormItem label="网站名称" labelCol={{ span: 5 }} wrapperCol={{ span: 15 }}>
          {form.getFieldDecorator('web', {
            rules: [
              { required: true, message: '请输入至少2个字符的网站名称！', min: 2 },
              { required: true, message: '请输入不多于50个字符的网站名称！', max: 50 },
            ],
            initialValue: values.web,
          })(<Input placeholder="请输入" />)}
        </FormItem>
        <FormItem label="网站地址" labelCol={{ span: 5 }} wrapperCol={{ span: 15 }}>
          {form.getFieldDecorator('url', {
            rules: [
              { required: true, message: '请输入至少5个字符的网站地址！', min: 5 },
              { required: true, message: '请输入不多于100个字符的网站地址！', max: 100 },
            ],
            initialValue: values.url,
          })(<Input placeholder="请输入" />)}
        </FormItem>
        <FormItem label="类别名称" labelCol={{ span: 5 }} wrapperCol={{ span: 15 }}>
          {form.getFieldDecorator('titleId', {
            rules: [{ required: true, message: '请选择所属类别！' }],
            initialValue: values.titleId,
          })(
            <Select style={{ width: '100%' }} placeholder="请选择">
              {children}
            </Select>,
          )}
        </FormItem>
        <FormItem label="所属地区" labelCol={{ span: 5 }} wrapperCol={{ span: 15 }}>
          {form.getFieldDecorator('scope', {
            rules: [{ required: true, message: '请选择所属地区！' }],
            initialValue: values.scope,
          })(
            <Select style={{ width: '100%' }} placeholder="请选择">
              {allscope}
            </Select>,
          )}
        </FormItem>
        <FormItem label="排序标志" labelCol={{ span: 5 }} wrapperCol={{ span: 15 }}>
          {form.getFieldDecorator('sort', {
            rules: [{ required: true, message: '请输入不多于10个字符的类别编码！', max: 10 }],
            initialValue: values.sort,
          })(<Input placeholder="请输入" />)}
        </FormItem>
        <FormItem label="图标地址" labelCol={{ span: 5 }} wrapperCol={{ span: 15 }}>
          {form.getFieldDecorator('picUrl', {
            rules: [{ required: false }],
          })(
            <Upload {...prop}>
              {imageUrl && imageUrl != null && imageUrl != '' ? (
                <img src={imageUrl} width="64px" height="64px" />
              ) : (
                uploadButton
              )}
            </Upload>,
          )}
        </FormItem>
      </Modal>
    );
  }
}

/* eslint react/no-multi-comp:0 */
@connect(({ yellowPageManage, loading }) => ({
  data: yellowPageManage.data,
  loading: loading.models.yellowPageManage,
}))
@Form.create()
class WebList extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    selectedRows: [],
    formValues: {},
    page: 0,
    size: 10,
  };

  columns = [
    {
      title: '网站名称',
      dataIndex: 'web',
    },
    {
      title: '类别名称',
      dataIndex: 'titleName',
    },
    {
      title: '操作',
      render: record => (
        <Fragment>
          <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>
        </Fragment>
      ),
    },
  ];

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'yellowPageManage/queryWeb',
      payload: {},
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      page: pagination.current - 1,
      size: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    this.setState({
      page: pagination.current - 1,
      size: pagination.pageSize,
    });

    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'yellowPageManage/queryWeb',
      payload: params,
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    this.setState({
      formValues: {},
    });
    dispatch({
      type: 'yellowPageManage/queryWeb',
      payload: {},
    });
  };

  handleMenuClick = e => {
    const { dispatch } = this.props;
    const { selectedRows, page, size } = this.state;
    const params = {
      page,
      size,
    };
    if (selectedRows.length === 0) return;
    switch (e.key) {
      default:
        dispatch({
          type: 'yellowPageManage/deleteWeb',
          payload: {
            id: selectedRows.map(row => row.id),
          },
          callback: () => {
            this.setState({
              selectedRows: [],
            });

            // 刷新列表
            dispatch({
              type: 'yellowPageManage/queryWeb',
              payload: params,
            });
          },
        });
        break;
    }
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleSearch = e => {
    e.preventDefault();
    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const values = {
        ...fieldsValue,
        updatedAt: fieldsValue.updatedAt && fieldsValue.updatedAt.valueOf(),
      };

      this.setState({
        formValues: values,
      });

      dispatch({
        type: 'yellowPageManage/queryWeb',
        payload: values,
      });
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  getAllTitle = () => {
    const { data } = this.props;
    return data.title;
  };

  handleUpdateModalVisible = (flag, record) => {
    this.setState({
      updateModalVisible: !!flag,
      record,
    });

    //调用子组件的方法
    if (this.state.updateModalVisible == false) {
      this.child.init(record);
    }
  };

  //绑定子组件
  onRef = ref => {
    this.child = ref;
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    const { page, size } = this.state;
    const params = { page, size };
    console.log(fields);
    dispatch({
      type: 'yellowPageManage/addWeb',
      payload: {
        ...fields,
      },
      callback: () => {
        dispatch({
          type: 'yellowPageManage/queryWeb',
          payload: params,
        });
      },
    });

    message.success('添加成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record, page, size } = this.state;

    const params = {
      page,
      size,
    };

    dispatch({
      type: 'yellowPageManage/updateWeb',
      payload: {
        body: {
          id: record.id,
          web: fields.web,
          url: fields.url,
          titleId: fields.titleId,
          scope: fields.scope,
          picUrl: fields.picUrl,
          sort: fields.sort,
        },
      },
      callback: () => {
        dispatch({
          type: 'yellowPageManage/queryWeb',
          payload: params,
        });
      },
    });

    message.success('修改成功');
    this.handleUpdateModalVisible();
  };

  renderForm() {
    const {
      form: { getFieldDecorator },
      getAllTitle,
    } = this.props;
    const children = [];
    var columns = '';
    console.log('columns');
    if (this.getAllTitle() != null) columns = this.getAllTitle().content;
    if (columns && columns != '' && columns != null) {
      for (let i = 0; i < columns.length; i++) {
        children.push(
          <Option value={columns[i].id} key={i}>
            {columns[i].titleName}
          </Option>,
        );
      }
    }

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="网站名称">
              {getFieldDecorator('web')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="类别名称">
              {getFieldDecorator('titleId')(
                <Select style={{ width: '100%' }} placeholder="请选择">
                  {children}
                </Select>,
              )}
            </FormItem>
          </Col>

          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  render() {
    const { data, loading } = this.props;
    const { selectedRows, modalVisible, updateModalVisible, record } = this.state;
    const menu = (
      <Menu onClick={this.handleMenuClick} selectedKeys={[]}>
        <Menu.Item key="remove">删除</Menu.Item>
      </Menu>
    );
    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
      getAllTitle: this.getAllTitle,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
      getAllTitle: this.getAllTitle,
    };

    return (
      <PageHeaderWrapper title="网站管理">
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderForm()}</div>
            <div className={styles.tableListOperator}>
              <Button icon="plus" type="primary" onClick={() => this.handleModalVisible(true)}>
                新建
              </Button>
              {selectedRows.length > 0 && (
                <span>
                  <Popconfirm
                    title="是否删除此网站！"
                    onConfirm={() => this.handleMenuClick(selectedRows)}
                    okText="是"
                    cancelText="否"
                  >
                    <Button>删除</Button>
                  </Popconfirm>
                </span>
              )}
            </div>
            <StandardTable
              rowKey="id"
              selectedRows={selectedRows}
              loading={loading}
              data={data}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
            />
          </div>
        </Card>
        <CreateForm {...parentMethods} modalVisible={modalVisible} />
        {/* 调用自身方法，绑定子组件 */}
        <UpdateForm
          {...updateMethods}
          updateModalVisible={updateModalVisible}
          values={record}
          onRef={this.onRef}
        />
        {/* <UpdateForm {...updateMethods} updateModalVisible={updateModalVisible} values={record} /> */}
      </PageHeaderWrapper>
    );
  }
}

export default WebList;
