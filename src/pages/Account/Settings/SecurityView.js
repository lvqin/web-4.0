import React, { Component, Fragment, PureComponent } from 'react';
import { connect } from 'dva';
import { formatMessage, FormattedMessage } from 'umi-plugin-react/locale';
import { List, Form, Input, Modal, message } from 'antd';
// import { getTimeDistance } from '@/utils/utils';

const FormItem = Form.Item;

const passwordStrengthText = {
  strong: (
    <font className="strong">
      <FormattedMessage id="app.settings.security.strong" defaultMessage="Strong" />
    </font>
  ),
  medium: (
    <font className="medium">
      <FormattedMessage id="app.settings.security.medium" defaultMessage="Medium" />
    </font>
  ),
  weak: (
    <font className="weak">
      <FormattedMessage id="app.settings.security.weak" defaultMessage="Weak" />
    </font>
  ),
};

// 隐藏部分手机号码
const Phone = ({ text }) => (text ? text.replace(/(\d{3})(\d{4})(\d{4})/, '$1****$3') : '');
// 隐藏部分电子邮箱
const Email = ({ text }) =>
  text ? text.replace(/(\w?)(\w+)(\w)(@\w+\.[a-z]+(\.[a-z]+)?)/, '$1****$3$4') : '';

@Form.create()
class UpdatePasswordForm extends PureComponent {
  state = { confirmDirty: false };

  static defaultProps = {
    handleUpdate: () => {},
    handleUpdateModalVisible: () => {},
  };

  constructor(props) {
    super(props);

    this.formLayout = {
      labelCol: { span: 7 },
      wrapperCol: { span: 13 },
    };
  }

  handleConfirmBlur = e => {
    const { value } = e.target;
    const { confirmDirty } = this.state;
    this.setState({ confirmDirty: confirmDirty || !!value });
  };

  compareToFirstPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && value !== form.getFieldValue('newPassword')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    const { form } = this.props;
    const { confirmDirty } = this.state;
    if (value && confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  };

  render() {
    const { updateModalVisible, handleUpdateModalVisible, form, handleUpdate } = this.props;
    const OkUpdateHandle = () => {
      form.validateFields((err, fieldsValue) => {
        if (err) return;
        form.resetFields();
        handleUpdate(fieldsValue);
      });
    };
    return (
      <Modal
        destroyOnClose
        title="修改密码"
        visible={updateModalVisible}
        onOk={OkUpdateHandle}
        onCancel={() => handleUpdateModalVisible()}
      >
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="当前密码">
          {form.getFieldDecorator('password', {
            rules: [{ required: true, message: '请输入至多50个字符的账号密码！', max: 50 }],
          })(<Input.Password placeholder="请输入当前密码" />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="新密码">
          {form.getFieldDecorator('newPassword', {
            rules: [
              { required: true, message: '请输入至少6个字符的新密码！', min: 6 },
              { required: true, message: '请输入不多于50个字符的新密码！', max: 50 },
              {
                validator: this.validateToNextPassword,
              },
            ],
          })(<Input.Password placeholder="请输入新密码" />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="确认密码">
          {form.getFieldDecorator('confirm', {
            rules: [{ required: true, validator: this.compareToFirstPassword }],
          })(<Input.Password placeholder="请再次输入新密码" onBlur={this.handleConfirmBlur} />)}
        </FormItem>
      </Modal>
    );
  }
}

// eslint-disable-next-line react/no-multi-comp
@Form.create()
class UpdatePhoneForm extends PureComponent {
  state = { confirmDirty: false };

  static defaultProps = {
    handleUpdate: () => {},
    updateModalVisible: () => {},
  };

  constructor(props) {
    super(props);

    this.formLayout = {
      labelCol: { span: 7 },
      wrapperCol: { span: 13 },
    };
  }

  handleConfirmBlur = e => {
    const { value } = e.target;
    const { confirmDirty } = this.state;
    this.setState({ confirmDirty: confirmDirty || !!value });
  };

  render() {
    const {
      dataSource,
      updateModalVisible,
      handleUpdateModalVisible,
      form,
      handleUpdate,
    } = this.props;
    const OkUpdateHandle = () => {
      form.validateFields((err, fieldsValue) => {
        if (err) return;
        form.resetFields();
        handleUpdate(fieldsValue);
      });
    };

    const { key } = dataSource;
    return (
      <Modal
        destroyOnClose
        title={dataSource.title}
        visible={updateModalVisible}
        onOk={OkUpdateHandle}
        onCancel={() => handleUpdateModalVisible()}
      >
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label={dataSource.label}>
          {form.getFieldDecorator(key, {})(
            <Input placeholder="请输入" onBlur={this.handleConfirmBlur} />,
          )}
        </FormItem>
      </Modal>
    );
  }
}

// eslint-disable-next-line react/no-multi-comp
@connect(({ account }) => ({ securitySettings: account.securitySettings }))
@Form.create()
class SecurityView extends Component {
  state = {
    updateModalVisible: false,
    updatePhoneVisible: false,
    updateEmailVisible: false,
  };

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'account/fetch',
      payload: {},
    });
  }

  handleUpdate = fields => {
    const { dispatch } = this.props;

    dispatch({
      type: 'account/updatePassword',
      payload: {
        body: {
          ...fields,
        },
      },
    });

    message.success('修改成功');
    this.handleUpdateModalVisible();
  };

  handleUpdatePhone = fields => {
    const { dispatch, securitySettings } = this.props;

    dispatch({
      type: 'account/updateSettings',
      payload: {
        ...securitySettings,
        ...fields,
      },
    });

    message.success('修改成功');
    this.handleUpdatePhoneVisible();
  };

  handleUpdateEmail = fields => {
    const { dispatch, securitySettings } = this.props;

    dispatch({
      type: 'account/updateSettings',
      payload: {
        ...securitySettings,
        ...fields,
      },
    });

    message.success('修改成功');
    this.handleUpdateEmailVisible();
  };

  handleUpdateModalVisible = flag => {
    this.setState({
      updateModalVisible: !!flag,
    });
  };

  handleUpdatePhoneVisible = flag => {
    this.setState({
      updatePhoneVisible: !!flag,
    });
  };

  handleUpdateEmailVisible = flag => {
    this.setState({ updateEmailVisible: !!flag });
  };

  getData = () => {
    const { a, securitySettings } = this.props;
    const { email, passwordStrength, phone } = securitySettings || {};
    let strength = passwordStrengthText.weak;
    if (passwordStrength > 2) {
      strength = passwordStrengthText.strong;
    } else if (passwordStrength > 1) {
      strength = passwordStrengthText.medium;
    }

    const data = [
      {
        title: formatMessage({ id: 'app.settings.security.password' }, {}),
        description: (
          <Fragment>
            {formatMessage({ id: 'app.settings.security.password-description' })}：{strength}
          </Fragment>
        ),
        actions: [
          <a onClick={() => this.handleUpdateModalVisible(true)}>
            <FormattedMessage id="app.settings.security.modify" defaultMessage="Modify" />
          </a>,
        ],
      },
      {
        title: formatMessage({ id: 'app.settings.security.phone' }, {}),
        description: phone ? (
          <Fragment>
            {formatMessage({ id: 'app.settings.security.phone-description' })}：
            <Phone text={phone} />
          </Fragment>
        ) : (
          <Fragment>{formatMessage({ id: 'app.settings.security.no-phone-description' })}</Fragment>
        ),
        actions: [
          <a onClick={() => this.handleUpdatePhoneVisible(true)}>
            <FormattedMessage id="app.settings.security.modify" defaultMessage="Modify" />
          </a>,
        ],
      },
      {
        title: formatMessage({ id: 'app.settings.security.question' }, {}),
        description: formatMessage({ id: 'app.settings.security.question-description' }, {}),
        actions: [
          <a>
            <FormattedMessage id="app.settings.security.set" defaultMessage="Set" />
          </a>,
        ],
      },
      {
        title: formatMessage({ id: 'app.settings.security.email' }, {}),
        description: email ? (
          <Fragment>
            {formatMessage({ id: 'app.settings.security.email-description' })}：
            <Email text={email} />
          </Fragment>
        ) : (
          <Fragment>{formatMessage({ id: 'app.settings.security.no-email-description' })}</Fragment>
        ),
        actions: [
          <a onClick={() => this.handleUpdateEmailVisible(true)}>
            <FormattedMessage id="app.settings.security.modify" defaultMessage="Modify" />
          </a>,
        ],
      },
      {
        title: formatMessage({ id: 'app.settings.security.mfa' }, {}),
        description: formatMessage({ id: 'app.settings.security.mfa-description' }, {}),
        actions: [
          <a>
            <FormattedMessage id="app.settings.security.bind" defaultMessage="Bind" />
          </a>,
        ],
      },
    ];
    return data;
  };

  render() {
    const { updateModalVisible, updatePhoneVisible, updateEmailVisible } = this.state;
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };

    const updatePhoneMethods = {
      handleUpdateModalVisible: this.handleUpdatePhoneVisible,
      handleUpdate: this.handleUpdatePhone,
    };

    const updateEmailMethods = {
      handleUpdateModalVisible: this.handleUpdateEmailVisible,
      handleUpdate: this.handleUpdateEmail,
    };

    return (
      <Fragment>
        <List
          itemLayout="horizontal"
          dataSource={this.getData()}
          renderItem={item => (
            <List.Item actions={item.actions}>
              <List.Item.Meta title={item.title} description={item.description} />
            </List.Item>
          )}
        />
        <UpdatePasswordForm {...updateMethods} updateModalVisible={updateModalVisible} />
        <UpdatePhoneForm
          dataSource={{
            title: '绑定手机',
            key: 'phone',
            label: '手机号码',
          }}
          {...updatePhoneMethods}
          updateModalVisible={updatePhoneVisible}
        />
        <UpdatePhoneForm
          dataSource={{
            title: '绑定邮箱',
            key: 'email',
            label: '电子邮箱',
          }}
          {...updateEmailMethods}
          updateModalVisible={updateEmailVisible}
        />
      </Fragment>
    );
  }
}

export default SecurityView;
