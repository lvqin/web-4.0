import React, { PureComponent } from 'react';
import { List, Icon, Tag } from 'antd';
import { connect } from 'dva';
import ArticleListContent from '@/components/ArticleListContent';
import styles from './Articles.less';

@connect(({ newsManage }) => ({
  index: newsManage.data.index,
}))
class RiskWarning extends PureComponent {
  render() {
    const { index } = this.props;
    const IconText = ({ type, text }) => (
      <span>
        <Icon type={type} style={{ marginRight: 8 }} />
        {text}
      </span>
    );
    return (
      <List
        size="large"
        className={styles.articleList}
        rowKey="id"
        itemLayout="vertical"
        dataSource={index.warnLists}
        renderItem={item => {
          let link = '/index/news/'.concat(item.newsId);
          return (
            <List.Item key={item.newsId}>
              <List.Item.Meta
                title={
                  <a className={styles.listItemMetaTitle} href={link} target="_blank">
                    {item.newsTitle}
                  </a>
                }
              />
              <ArticleListContent data={item} />
            </List.Item>
          );
        }}
      />
    );
  }
}

export default RiskWarning;
