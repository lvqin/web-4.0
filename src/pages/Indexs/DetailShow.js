import React, { PureComponent, Fragment , Component } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import router from 'umi/router';
import Link from 'umi/link';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Steps,
  Radio,
  Carousel,
  Tag,
  List,
  Tabs,
  Typography,
  Avatar,
} from 'antd';

import styles from './DetailShow.less';
import logo_gwzc from './logo/logo_gwzc.jpg';

const data = [
  {title: '用户名xxxxx 1',},
  {title: '用户名xxxxx 2',},
  {title: '用户名xxxxx 3',},
  {title: '用户名xxxxx 4',},
  {title: '用户名xxxxx 5',},
  {title: '用户名xxxxx 6',},
  {title: '用户名xxxxx 7',},
  {title: '用户名xxxxx 8',},
  {title: '用户名xxxxx 9',},
  {title: '用户名xxxxx 10',},
];

const listConDiv= (
  <Link to='/'  target="_blank">
    <div className = {styles.list_con}>
      <div className = {styles.list_con_l}>
       <p><b><span>用户名xxxxx</span></b></p>
       <p>
       评价评价评价评价评价评价评价评价评价评价评价
       评价评价评价评价评价评价评价评价评价评价评价
       评价评价评价评价评价评价评价评价评价评价评价评价评价评价..
       </p>
      </div>
      <div className = {styles.list_con_r}>
        <p>发表于<br/>2019年6月4日</p>
      </div>
    </div>
  </Link>
);

const { TabPane } = Tabs;
const evaluateDiv =(
  <Fragment>
    <div className={styles.evaluate}>
      <div className={styles.eva_top}>
        <div className={styles.top_l}>
          <span>好评度</span>
          <p className={styles.num}>97%</p>
        </div>  
        <div className={styles.top_r}>
          <Button size='small'>标签xx</Button>&nbsp;&nbsp;&nbsp;&nbsp;
          <Button size='small'>标签xx</Button>&nbsp;&nbsp;&nbsp;&nbsp;
          <Button size='small'>标签xx</Button>&nbsp;&nbsp;&nbsp;&nbsp;
          <Button size='small'>标签xx</Button>&nbsp;&nbsp;&nbsp;&nbsp;
          <Button size='small'>标签xx</Button>&nbsp;&nbsp;&nbsp;&nbsp;
          <Button size='small'>标签xx</Button>&nbsp;&nbsp;&nbsp;&nbsp;
          <Button size='small'>标签xx</Button>&nbsp;&nbsp;&nbsp;&nbsp;
          <br></br><br></br>
          <Button size='small'>标签xx</Button>&nbsp;&nbsp;&nbsp;&nbsp;
          <Button size='small'>标签xx</Button>&nbsp;&nbsp;&nbsp;&nbsp;
          <Button size='small'>标签xx</Button>&nbsp;&nbsp;&nbsp;&nbsp;
          <Button size='small'>标签xx</Button>&nbsp;&nbsp;&nbsp;&nbsp;
       </div> 
      </div>  
      <div className={styles.con}>
       <div className={styles.eva_title}>
          <Button size='small'>全部评价</Button>&nbsp;&nbsp;&nbsp;&nbsp;
          <Button size='small'>好评</Button>&nbsp;&nbsp;&nbsp;&nbsp;
          <Button size='small'>中评</Button>&nbsp;&nbsp;&nbsp;&nbsp;
          <Button size='small'>差评</Button>&nbsp;&nbsp;&nbsp;&nbsp;
       </div>
       <div className={styles.eva_list}>
       <List
          itemLayout="horizontal"
          dataSource={data}
          renderItem={item => (
            <List.Item>
              <List.Item.Meta
                avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                title={<a href="https://ant.design">{item.title}</a>}
                description="评价评价评价评价评价评价评价评价评价评价评价
                评价评价评价评价评价评价评价评价评价评价评价
                评价评价评价评价评价评价评价评价评价评价评价评价评价评价.."
              />
            </List.Item>
          )}
        />
       </div>
      </div>
    </div>
  </Fragment>
)
@connect(({ newsManage, loading }) => ({
  data: newsManage.data,
  loading: loading.models.newsManage,
  recommend : newsManage.data.recommend,
  index : newsManage.data.index
}))

class DetailShow extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      // activeKey: {}
    };
  }

  componentWillMount  () {
    // const { dispatch } = this.props;
    // // console.log(params);
    // dispatch({
    //   type: 'newsManage/newsIndex',
    //   // payload: {
    //   //   ...params
    //   // },
    // });
  }

  moreBut = () => {
    const url = '/index/columns/'+tabsKey;
    const h=window.open('about:blank');
    h.location.href=url;
  }

  onTabChange = tabType => {
    tabsKey = tabType;
    console.log(tabsKey);
  };

  render() {
    const{index,recommend} = this.props;
    // console.log(index);
    return (
      <Fragment>
        <div className = {styles.bodyDiv}>
          <div className = {styles.conn}>
            {/* top */}
            <div className = {styles.top}>
              <Link to="/index"><b><span>首页</span></b></Link>
              <span> > </span>
              <Link to="/index/news"><span>综合服务</span></Link>
            </div>
            {/* top */}
            <div className = {styles.info}>
              {/* list */}
              <div className = {styles.list}>
                <div className = {styles.introduce}>
                  <div className = {styles.introduce_l}>
                    <img src={logo_gwzc}></img><br/><br/><br/>
                    <p>讨淘网是一个提供追债、诉讼、调查任务发布和竞标报价的平台。通过认证的用户可以发布任务或参与任务的竞标。还有不良资产转让信息的发布。</p>
                  </div>
                  <div className = {styles.introduce_r}>
                    <p>星级:<br/>☆☆☆☆</p>
                    <u>进入</u>
                  </div>
                </div>
                 <Tabs 
                  // onChange={this.onTabChange}
                  // tabBarExtraContent={ <Button onClick={() => this.moreBut()} >更多</Button>}
                  >
                  <TabPane tab="介绍" key="2019003">
                  </TabPane>
                  <TabPane tab="评价" key="evaluate">
                    {evaluateDiv}
                  </TabPane>
                </Tabs>
              </div>
              {/* list */}
              {/* sidebar */}
              <div className = {styles.sidebar}>
              </div>
              {/* sidebar */}
            </div>
          </div>
        </div>
      </Fragment>
      );
  }
}  
export default DetailShow;