import React, { PureComponent, Fragment, Component } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import router from 'umi/router';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Steps,
  Radio,
  Carousel,
} from 'antd';

// import StandardTable from '@/components/StandardTable';
// import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import { Link } from 'umi';

import styles from './Detail.less';
import logo_bsw from './logo/logo_bsw.gif';
import logo_jka from './logo/logo_jka.png';
import logo_sl from './logo/logo_sl.jpg';
import logo_gw from './logo/logo_gw.png';
import logo_tfs from './logo/logo_tfs.png';
import logo_cdzx from './logo/logo_cdzx.png';
import logo_gwzc from './logo/logo_gwzc.jpg';

const gwDiv = (
  <Fragment>
    <div className={styles.logo} onClick={() => this.toHref('http://www.gdwz.com.cn/')}>
    <img src={logo_gw}></img>
    </div>
  </Fragment>
)

class Index extends PureComponent {
  constructor(props) {
    super(props);
  }
  componentDidMount () {
    // const { dispatch } = this.props;
    const {hash} = this.props.location;
    // console.log(hash);
    this.scrollToAnchor(hash.replace("#",""));
  }

  scrollToAnchor = (anchorName) => {
    // console.log(anchorName.re);
    if (anchorName) {
        let anchorElement = document.getElementById(anchorName);// 找到锚点
        // 如果对应id的锚点存在，就跳转到锚点
        if(anchorElement) { anchorElement.scrollIntoView({block: 'start', behavior: 'smooth'}); }
    }
  }

  toHref = (url) =>{
    const h=window.open('about:blank');
    h.location.href=url;
  }

  render() {
    return (
      <Fragment>
        <div className={styles.bodyDiv}>
          <div className={styles.content}>
            {/* top */}
            <div className = {styles.top}>
              <Link to="/index"><b><span>首页</span></b></Link>
              <span> > </span>
              {/* <Link to="/index/news"><span>信息服务</span></Link> */}
            </div>
            <div>&nbsp;</div>
            <div id='Zh' className={styles.detailzhh} >
              <h1>综合服务</h1>
              {/* {gwDiv} */}
              <Link to='/index/detail-show' target="_blank">
                <div className={styles.logogwzc}
                // onClick={() => this.toHref('http://www.itaotaowang.com/')}
                > 
                  <img src={logo_gwzc}></img>
                </div>
              </Link>
              <Link to='/index/detail-show' target="_blank">
                <div className={styles.logosl}
                // onClick={() => this.toHref('http://www.itaotaowang.com/')}
                > 
                  <img src={logo_sl}></img>
                </div>
              </Link>
              <Link to='/index/detail-show' target="_blank">
                <div className={styles.logotfs}
                // onClick={() => this.toHref('http://www.itaotaowang.com/')}
                > 
                  <img src={logo_tfs}></img>
                </div>
              </Link>
              <Link to='/index/detail-show' target="_blank">
                <div className={styles.logobsw}
                // onClick={() => this.toHref('http://www.itaotaowang.com/')}
                > 
                  <img src={logo_bsw}></img>
                </div>
              </Link>
            </div>
            <div id='Jr' className={styles.detailjr} >
              <h1 >金融服务</h1>
              <Link to='/index/detail-show' target="_blank">
                <div className={styles.logosl}
                // onClick={() => this.toHref('http://www.itaotaowang.com/')}
                > 
                  <img src={logo_sl}></img>
                </div>
              </Link>
            </div>  
            <div  id='Wl' className={styles.detailwl} >
              <h1 >物流服务</h1>
              <Link to='/index/detail-show' target="_blank">
                <div className={styles.logojka}
                // onClick={() => this.toHref('http://www.itaotaowang.com/')}
                > 
                  <img src={logo_jka}></img>
                </div>
              </Link>
              <Link to='/index/detail-show' target="_blank">
                <div className={styles.logocdzx}
                // onClick={() => this.toHref('http://www.itaotaowang.com/')}
                > 
                  <img src={logo_cdzx}></img>
                </div>
              </Link>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}
export default Index;