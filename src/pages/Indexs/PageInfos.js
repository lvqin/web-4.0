import React, { Component } from 'react';
import Styles from './PageInfos.less';
import { connect } from 'dva';
import { Anchor } from 'antd';
import { Tabs } from 'antd';

import logo_cdzx from './logo/logo_cdzx.png';

import e from './logo/e.png';
import a from './logo/a.png';
import aa from './logo/aa.png';
import { Link } from 'umi';


function ListItem(props) {
    var { index } = props;
    const { Link } = Anchor;
    return (

        <Link href={`#${index}`} title={props.value} />
    );

}

class MenuList extends Component {
    render() {
        const title = this.props.title;
        return (
            <ul className={Styles.subMenu} ref={r => this._ul = r}>
                <Anchor getContainer={() => window} affix="true">
                    {title.length > 0 ? title.map((item, index) =>
                        <ListItem key={index} index={index}

                            value={item.titleName} />
                    ) : ''}
                </Anchor>
            </ul>
        )
    }
}

function ContentItem(props) {
    return <li><a>{props.value}</a></li>;
}

class ContentList extends Component {

    getLength = (webs) => {
        var arrNum = [];
        if (webs) {
            for (var item of webs) {
                arrNum.push(item.web.length);
            }

            return Math.max.apply(null, arrNum) * 16;
        }
    }
    render() {
        const { web0, web1, title } = this.props;
        var length=web0>web1?web0:web1;
        var length0 = this.getLength(web0)+80;
        var margin0 = 918 % (length0) / (Math.floor(918 / (length0)) * 2);

        // var length0 = this.getLength(web0)+40;
        // var margin0 = 918 % (length0) / (Math.floor(918 / (length0)) * 2);
        // var length1 = this.getLength(web0)+40;
        // var margin1 = 918 % (length0) / (Math.floor(918 / (length0)) * 2);
        return (
            <div>
                

                {web0.length > 0 ?
                    <div className={Styles.contentList}>
                        <div className={Styles.contentHead}>
                            <div className={Styles.contentTitle}>
                                国内
                            </div>
                        </div>
                        <div className={Styles.contentContent}>
                            {web0 ? web0.map((item, index) =>
                                <div key={index} className={Styles.contentLink} style={{ width: length0, marginLeft: margin0, marginRight: margin0 }}>
                                   {item.picUrl&&item.picUrl!=null&&item.picUrl!=''?
                                   <img src={require(`./pic/${item.picUrl}`)}  style={{width:25,height:25,marginRight:5}} />:
                                
                                //    <div style={{width:25,height:25,float:'left',marginRight:5}}></div>
                                <img src={require('./logo/aa.png')} style={{width:25,height:25,marginRight:5}} />
                                }
                                
                                    <a href={item.url} target="_blank">{item.web}</a></div>
                            ) : ''}
                            <div style={{ clear: 'both' }}></div>
                        </div>
                    </div>
                    : ''}




                {web1.length > 0 ?
                    <div className={Styles.contentList}>
                        <div className={Styles.contentHead}>
                            <div className={Styles.contentTitle}>
                                国外
                    </div>
                        </div>
                        <div className={Styles.contentContent}>
                            {web1 ? web1.map((item, index) =>
                                <div key={index} className={Styles.contentLink} style={{ width: length0, marginLeft: margin0, marginRight: margin0 }}>
                                    
                                    {item.picUrl&&item.picUrl!=null&&item.picUrl!=''?
                                   <img src={require(`./pic/${item.picUrl}`)} style={{width:25,height:25,marginRight:5}} />:
                                // <img src={a} style={{width:25,height:25,marginRight:5}} />:
                                //    <div style={{width:25,height:25,float:'left',marginRight:5}}></div>
                                <img src={aa} style={{width:25,height:25,marginRight:5}} />
                                }
                                <a href={item.url} target="_blank">{item.web}</a></div>
                            ) : ''}
                            <div style={{ clear: 'both' }}></div>
                        </div>
                    </div>
                    : ''}


</div>

            
        )
    }
}

@connect(({ pageInfos, loading }) => ({
    data: pageInfos.data,
    loading: loading.models.pageInfos,
    title: pageInfos.data.title,
    web0: pageInfos.data.web0,
    web1: pageInfos.data.web1
}))

class PageInfos extends Component {

    componentWillMount() {
        const { dispatch } = this.props;

        dispatch({
            type: 'pageInfos/fetch',
            payload: {}
        });
    }



    constructor(props) {
        super(props);
        this.state = {
            showMenu: true,
        }
    }

    MenuClick = () => {
        let showMenu = this.state.showMenu;
        // console.log(this.child._ul.style)
        let ul = this.child._ul;
        if (showMenu) {
            ul.style = 'display : none';
        } else {
            ul.style = 'display : block';
        }
        this.setState({ showMenu: !showMenu })
    }

    render() {
        const { title, web0, web1 } = this.props;

        const { TabPane } = Tabs;
        return (
            
            <div className={Styles.bodyDiv}>
            <div className={Styles.content}>
            <div className = {Styles.top}>
              <Link to="/index"><b><span>首页</span></b></Link>
              <span> > </span>
              <Link to="/index/pages"><span>外贸黄页</span></Link>
              {/* <img src="././././public/icons/icon-128x128.png" height="25px"></img> */}
            </div></div>
                <div className={Styles.main}>
                    <Tabs defaultActiveKey="0">
                        {web0.map((item, index) => <TabPane tab={title[index].titleName} key={index}>
                            <div className={Styles.content}>
                                <ContentList web0={item} web1={web1[index]} title={title[index]} />
                            </div>
                        </TabPane>)}

                    </Tabs>

                </div></div>
        );
    }
}

export default PageInfos;