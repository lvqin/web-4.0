import React, { PureComponent, Fragment, Component } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import router from 'umi/router';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  Modal,
  message,
  Badge,
  Divider,
  Steps,
  Radio,
  Carousel,
} from 'antd';

import { Link } from 'umi';
import Redirect from 'umi/redirect';

import styles from './Index.less';
import bannerImg from './image/auxiliary_banner_new.png';
import finaImg from './image/auxiliary_service_fina_icon.png';
import comImg from './image/auxiliary_service_com_icon.png';
import dateImg from './image/auxiliary_service_data_icon.png';
import logImg from './image/auxiliary_service_logistics_icon.png';
import infoImg from './image/auxiliary_service_infor_icon.png';
import foreignImg from './image/auxiliary_service_foreign_icon.png';
import serviceIcon from './image/service_icon_0.png';

const bannerDivStyle = {
  display: 'inline-block',
  position: 'relative',
  height: '450px',
  width:'100%',
  background: `url(${bannerImg})  no-repeat center` 
}
const bannerDiv = (
  <Carousel autoplay>
    <div >
      <Link to='/index'> <div style={bannerDivStyle}></div></Link>
    </div>
    <div>
      <Link to='/index'> <div style={bannerDivStyle}></div></Link>
    </div>
    <div>
      <Link to='/index'> <div style={bannerDivStyle}></div></Link>
    </div>
    <div>
      <Link to='/index'> <div style={bannerDivStyle}></div></Link>
    </div>
  </Carousel>
);

const finaBoxDiv = (
  <Fragment>
    {/* box */}
    <Link to='/index/detail#Jr' target="_blank" href>
      <div className={styles.info_box}>
        <div className={styles.info_box_top}>
          <div className={styles.info_box_t_l}>
            <img src={finaImg}></img>
          </div>
          <div className={styles.info_box_t_r}>
            <p className={styles.info_p_title}>金融服务</p>
            <p className={styles.info_p_cont}>外贸金融工具箱，想您所想、急您所急，助外贸企业缓解资金压力，实现业务升级</p>
          </div>
        </div>
        <div className={styles.info_box_bot}>
          {/* <p>进入服务→&nbsp;&nbsp;&nbsp;&nbsp;</p> */}
        </div>
      </div>
    </Link>
    {/* box */}
  </Fragment>
);

const dateBoxDiv = (
  <Fragment>
    {/* box */}
    <Link to='/index/dashboard/analysis' target="_blank">
      <div className={styles.info_box}>
        <div className={styles.info_box_top}>
          <div className={styles.info_box_t_l}>
            <img src={dateImg}></img>
          </div>
          <div className={styles.info_box_t_r}>
            <p className={styles.info_p_title}>数据服务</p>
            <p className={styles.info_p_cont}>以海量大数据为基础，揭示行业背后真相，深度挖掘外贸商机，助力企业经营决策</p>
          </div>
        </div>
        <div className={styles.info_box_bot}>
          {/* <p>进入服务→&nbsp;&nbsp;&nbsp;&nbsp;</p> */}
        </div>
      </div>
    </Link>
    {/* box */}
  </Fragment>
);

const infoBoxDiv = (
  <Fragment>
    {/* box */}
    <Link to='/index/news' target="_blank">
      <div className={styles.info_box}>
        <div className={styles.info_box_top}>
          <div className={styles.info_box_t_l}>
            <img src={infoImg}></img>
          </div>
          <div className={styles.info_box_t_r}>
            <p className={styles.info_p_title}>信息服务</p>
            <p className={styles.info_p_cont}>实时更新的外贸大资讯，为您展现全球外贸政策和新闻，一站尽揽天下外贸大势</p>
          </div>
        </div>
        <div className={styles.info_box_bot}>
          {/* <p>进入服务→&nbsp;&nbsp;&nbsp;&nbsp;</p> */}
        </div>
      </div>
    </Link>
    {/* box */}
  </Fragment>
);

const comBoxDiv = (
  <Fragment>
    {/* box */}
    <Link to='/index/detail#Zh'target="_blank" >
      <div className={styles.info_box}>
        <div className={styles.info_box_top}>
          <div className={styles.info_box_t_l}>
            <img src={comImg}></img>
          </div>
          <div className={styles.info_box_t_r}>
            <p className={styles.info_p_title}>综合服务</p>
            <p className={styles.info_p_cont}>外贸服务百宝袋，随时随地、省心省力，为外贸企业提供全流程、一站式服务</p>
          </div>
        </div>
        <div className={styles.info_box_bot}>
          {/* <p>进入服务→&nbsp;&nbsp;&nbsp;&nbsp;</p> */}
        </div>
      </div>
    </Link>
    {/* box */}
  </Fragment>
);

const logBoxDiv = (
  <Fragment>
    {/* box */}
    <Link to='/index/detail#Wl' target="_blank">
      <div className={styles.info_box}>
        <div className={styles.info_box_top}>
          <div className={styles.info_box_t_l}>
            <img src={logImg}></img>
          </div>
          <div className={styles.info_box_t_r}>
            <p className={styles.info_p_title}>物流平台</p>
            <p className={styles.info_p_cont}>外贸物流筋斗云，便捷高效、快速通达，帮外贸企业“更高、更快、更强”</p>
          </div>
        </div>
        <div className={styles.info_box_bot}>
          {/* <p>进入服务→&nbsp;&nbsp;&nbsp;&nbsp;</p> */}
        </div>
      </div>
    </Link>
    {/* box */}
  </Fragment>
);

const foreignBoxDiv = (
  <Fragment>
    {/* box */}
    <Link to='/index/pages' target="_blank">
      <div className={styles.info_box}>
        <div className={styles.info_box_top}>
          <div className={styles.info_box_t_l}>
            <img src={foreignImg}></img>
          </div>
          <div className={styles.info_box_t_r}>
            <p className={styles.info_p_title}>外贸黄页</p>
            <p className={styles.info_p_cont}>最新最全的外贸相关网站，政府、商业一应俱全，帮助您快速定位所需服务</p>
          </div>
        </div>
        <div className={styles.info_box_bot}>
          {/* <p>进入服务→&nbsp;&nbsp;&nbsp;&nbsp;</p> */}
        </div>
      </div>
    </Link>
    {/* box */}
  </Fragment>
);

const titleDiv = (
  <Fragment>
    <div className={styles.title} >
      <p> · 服务 · </p>
      <h1>SERVICE</h1>
      <img src={serviceIcon}></img>
    </div>
  </Fragment>
);

class Index extends PureComponent {
  constructor(props) {
    super(props);
  }
  toDetil = (record) =>{//查看详情
    const url = '/index/detail/#'+record;
    const h=window.open('about:blank');
    h.location.href=url;
    var id = url.substring( url.lastIndexOf("#")+"Jr");
    document.getElementById(id).scrollIntoView(true)
    }
  render() {
    return (
      <Fragment>
        <div className={styles.indexBody}>
          <div className={styles.banner}>
            {bannerDiv}
          </div>
          <div className={styles.content}>
            {titleDiv}
            <div className={styles.info}>
              {comBoxDiv}
              {dateBoxDiv}
              {finaBoxDiv}
              {infoBoxDiv}
              {logBoxDiv}
              {foreignBoxDiv}
            </div>
            {/* <div className={styles.detailzh} >
              <h1 id='Zh'>综合服务</h1>
            </div>
            <div className={styles.detailjr} >
              <h1 id='Jr'>金融服务</h1>
            </div>  
            <div className={styles.detailwl} >
              <h1 id='Wl'>物流服务</h1>
            </div> */}
          </div>
        </div>
      </Fragment>
    );
  }
}
export default Index;