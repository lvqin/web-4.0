import React, { Component, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Col, Row, Icon, Statistic, List, Typograph, Tag } from 'antd';
import { Link } from 'umi';
import {
  G2,
  Chart,
  Geom,
  Axis,
  Tooltip,
  Coord,
  Label,
  Legend,
  View,
  Guide,
  Shape,
  Facet,
  Util,
} from 'bizcharts';
import DataSet from '@antv/data-set';
import styles from './HomePage.less';

const StatisticsTile = (
  <div className={styles.firstCardTitle}>
    <img src={require('./image/localland_icon_0.png')} className={styles.chartIcon}></img>
    <span className={styles.cardTitle}>当月统计</span>
  </div>
);
const CustomsTile = (
  <div className={styles.firstCardTitle}>
    <img src={require('./image/localland_icon_5.png')} className={styles.chartIcon}></img>
    <span className={styles.cardTitle}>近一年申报趋势</span>
  </div>
);
const FunctionTile = (
  <div className={styles.firstCardTitle}>
    <img src={require('./image/localland_icon_1.png')} className={styles.chartIcon}></img>
    <span className={styles.cardTitle}>常用功能</span>
  </div>
);
const InfoTile = (
  <div className={styles.firstCardTitle}>
    <img src={require('./image/localland_icon_6.png')} className={styles.chartIcon}></img>
    <span className={styles.cardTitle}>我的资讯</span>
  </div>
);
const NoticeTile = (
  <div className={styles.firstCardTitle}>
    <img src={require('./image/localland_icon_7.png')} className={styles.chartIcon}></img>
    <span className={styles.cardTitle}>我的通知</span>
  </div>
);
const CenterTile = (
  <div className={styles.firstCardTitle}>
    <img src={require('./image/localland_icon_8.png')} className={styles.chartIcon}></img>
    <span className={styles.cardTitle}>中央标准应用</span>
  </div>
);
const LocalTile = (
  <div className={styles.firstCardTitle}>
    <img src={require('./image/localland_icon_9.png')} className={styles.chartIcon}></img>
    <span className={styles.cardTitle}>地方特色应用</span>
  </div>
);

/**
 * 返回自定义条数的数组
 * @param {*} arr
 * @param {*} size
 */
const getArray = (arr, size) => {
  if (arr.length > size) {
    return arr.slice(0, size);
  }
  return arr;
};

/**
 * 获取数组中的最小值
 * @param {*} array
 */
const getArrayMin = array => {
  if (array.length > 0) {
    let min = array[0].value;
    for (let item of array) {
      if (item.value < min) {
        min = item.value;
      }
    }
    return min;
  }
  return 0;
};

/**
 * 获取数组中的最大值
 * @param {*} array
 */
const getArrayMax = array => {
  if (array.length > 0) {
    let max = array[0].value;
    for (let item of array) {
      if (item.value > max) {
        max = item.value;
      }
    }
    return max;
  }
  return 0;
};

const getSerialNumber = str => {
  if (str.length >= 12) {
    let start = str.substring(0, 6);
    let end = str.substring(str.length - 6, str.length);
    return start + '**' + end;
  }
  return str;
};

const getNewsUrl = arr => {
  if (arr.length < 0) {
    return '';
  }
  let str = arr[0].newsUrl;
  let index = str.lastIndexOf('/');
  if (index > -1) {
    return str.substring(0, index);
  }
  return '';
};

const StatisticCard = props => (
  <Card
    title={StatisticsTile}
    bordered={false}
    bodyStyle={{ padding: '10px 0 0 0' }}
    extra={
      <a className={styles.linkFont} target="_blank" href="/index/dashboard/analysis">
        更多>
      </a>
    }
  >
    <div className={styles.ststCardCtnt}>
      <div className={styles.divBar}>
        <div className={styles.barLeftContent}>
          <img src={require('./image/localland_icon_2.png')} className={styles.chartIcon} />
        </div>
        <div className={styles.barRightContent}>
          <Statistic
            className={styles.statisticText}
            title="当月申报（笔）"
            value={props.ststcData.declareNum}
            valueStyle={{ marginTop: '-10PX', textAlign: 'right', paddingRight: '10px' }}
          />
        </div>
      </div>
      <div className={styles.divBar}>
        <div className={styles.barLeftContent}>
          <img src={require('./image/localland_icon_3.png')} className={styles.chartIcon} />
        </div>
        <div className={styles.barRightContent}>
          <Statistic
            className={styles.statisticText}
            title="当月回退（笔）"
            value={props.ststcData.backNum}
            valueStyle={{ marginTop: '-10PX', textAlign: 'right', paddingRight: '10px' }}
          />
        </div>
      </div>
      <div className={styles.divBar}>
        <div className={styles.barLeftContent}>
          <img src={require('./image/localland_icon_4.png')} className={styles.chartIcon} />
        </div>
        <div className={styles.barRightContent}>
          <Statistic
            className={styles.statisticText}
            title="通过率（%）"
            value={props.ststcData.passRate}
            valueStyle={{ marginTop: '-10PX', textAlign: 'right', paddingRight: '10px' }}
          />
        </div>
      </div>
    </div>
  </Card>
);

const LineCard = props => {
  const minValue = getArrayMin(props.trendData);
  const maxValue = getArrayMax(props.trendData);
  let cols = {};
  if (maxValue > 4) {
    cols = {
      value: {
        min: minValue,
        alias: '申报数（笔）',
      },
      year: {
        range: [0, 1],
        tickCount: 6,
      },
    };
  } else {
    cols = {
      value: {
        min: minValue,
        alias: '申报数（笔）',
        tickInterval: 1,
      },
      year: {
        range: [0, 1],
        tickCount: 6,
      },
    };
  }
  return (
    <Card title={CustomsTile} bordered={false} bodyStyle={{ padding: '10px 15px 10px 47px' }}>
      <Chart
        height={200}
        width={780}
        data={props.trendData}
        scale={cols}
        padding={[20, 'auto', 30, 'auto']}
      >
        <Axis name="year" />
        <Axis name="value" />
        <Tooltip
          crosshairs={{
            type: 'y',
          }}
        />
        <Geom type="area" position="year*value" shape="smooth" />
        <Geom type="line" position="year*value" shape="smooth" size={2}>
          <Label
            content={[
              'year*value',
              (year, value) => {
                return `${value}`;
              },
            ]}
            position="bottom"
            textStyle={{
              textAlign: 'start', // 文本对齐方向，可取值为： start middle end
              fill: '#404040', // 文本的颜色
              fontSize: '12', // 文本大小
              fontWeight: 'bold', // 文本粗细
              rotate: 0,
              textBaseline: 'top', // 文本基准线，可取 top middle bottom，默认为middle
            }}
          />
        </Geom>
        <Geom
          type="point"
          position="year*value"
          size={4}
          shape={'circle'}
          style={{
            stroke: '#fff',
            lineWidth: 1,
          }}
        />
      </Chart>
    </Card>
  );
};

const FuncCard = props => (
  <Card
    title={FunctionTile}
    bordered={false}
    bodyStyle={{ padding: '7.5px 0 7.5px 0', overflow: 'hidden' }}
    extra={
      <a className={styles.linkFont} target="_blank" href="#">
        编辑>
      </a>
    }
  >
    <div>
      {getArray(props.offenApp, 5).map(item => (
        <a href={item.url} target="_blank" className={styles.offenFun} key={item.url}>
          {item.name}
        </a>
      ))}
    </div>
  </Card>
);

const InfoCard = props => (
  <Card
    title={InfoTile}
    bordered={false}
    bodyStyle={{ padding: '18px 0 18px 36px' }}
    extra={
      <a className={styles.linkFont} target="_blank" href="/index/news">
        更多>
      </a>
    }
  >
    <List
      split={false}
      dataSource={getArray(props.infoData, 10)}
      renderItem={item => (
        <List.Item key={item.id} className={styles.listItem}>
          <img src={require('./image/point.png')} width="15px" />
          <a href={item.newsClassUrl} target="_blank" className={styles.newsClass}>
            [{item.newsClass}]
          </a>
          <a href={item.newsUrl} target="_blank" className={styles.newsTitle}>
            {item.newsTitle}
          </a>
        </List.Item>
      )}
    />
  </Card>
);

const NoticeCard = props => {
  const getTag = noticeFlag => {
    let color, content;
    switch (noticeFlag) {
      case '0':
        color = '#EE3073';
        content = '未通过';
        break;
      case '1':
        color = '#28B878';
        content = '已通过';
        break;
      default:
        color = '#C7C7C7';
        content = '审核中';
    }
    return (
      <Tag className={styles.noticeTagIn} color={color}>
        {content}
      </Tag>
    );
  };

  return (
    <Card
      title={NoticeTile}
      bordered={false}
      bodyStyle={{ padding: '0' }}
      extra={
        <a className={styles.linkFont} target="_blank" href="#">
          更多>
        </a>
      }
    >
      <List
        split={false}
        dataSource={getArray(props.noticeData, 8)}
        renderItem={item => (
          <List.Item key={item.noticeId} className={styles.noticeList}>
            <span className={styles.noticeblank}></span>
            <div className={styles.noticeIcon}></div>
            <span className={styles.noticeClass}>[{item.noticeClass}]</span>
            <span className={styles.noticeNumber}>{getSerialNumber(item.serialNumber)}</span>
            <span className={styles.noticeContent}>{item.noticeContent}</span>
            <span className={styles.noticeDate}>{item.noticeTime}</span>
            <span className={styles.noticeTag}>{getTag(item.noticeFlag)}</span>
          </List.Item>
        )}
      />
    </Card>
  );
};

const CenterCard = props => (
  <Card title={CenterTile} bordered={false} bodyStyle={{ padding: '18px 0' }}>
    <iframe
      src={props.centerApp}
      style={{ width: '100%', height: '205px' }}
      scrolling="no"
      frameBorder="0"
    />
  </Card>
);

const LoaclApps = props => (
  <Card title={LocalTile} bordered={false} bodyStyle={{ padding: '18px 0' }}>
    <iframe
      src={props.localApp}
      style={{ width: '100%', height: '205px' }}
      scrolling="no"
      frameBorder="0"
    />
  </Card>
);

@connect(({ personal, loading }) => ({
  data: personal.data,
  loading: loading.models.personal,
  // recommend : newsManage.data.recommend,
}))
class HomePage extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    // 获得URL参数
    const query_params = new URLSearchParams(this.props.location.search);
    const token = query_params.get('token');
    // 存储
    sessionStorage.setItem('access_token', token);
    const { dispatch } = this.props;
    dispatch({
      type: 'personal/fetch',
    });
  }
  render() {
    const { data, loading } = this.props;
    const { ststcData, trendData, offenApp, infoData, noticeData, appUrls } = data;

    return (
      <div className={styles.layOut}>
        <div className={styles.chartLayout}>
          <div className={styles.statisticCard}>
            <StatisticCard ststcData={ststcData} />
            <div className={styles.linePaddingtop}>
              <LineCard trendData={trendData} />
            </div>
          </div>
          <div className={styles.funcCard}>
            <FuncCard offenApp={offenApp} />
          </div>
        </div>
        <div className={styles.listLayout}>
          <div className={styles.myInfo}>
            <InfoCard infoData={infoData} />
          </div>
          <div className={styles.myNotice}>
            <NoticeCard noticeData={noticeData} />
          </div>
        </div>
        <div className={styles.centerLayout}>
          <CenterCard centerApp={appUrls.centerApp} />
        </div>
        <div className={styles.localLayout}>
          <LoaclApps localApp={appUrls.localApp} />
        </div>
      </div>
    );
  }
}
export default HomePage;
