import React, { Component, Suspense } from 'react';
import { connect } from 'dva';
import GridContent from '@/components/PageHeaderWrapper/GridContent';
import { getTimeDistance } from '@/utils/utils';
import styles from './Report.less';
import { Link } from 'umi';

const PortionCard = React.lazy(() => import('./PortionCard'));
const MarketCard = React.lazy(() => import('./MarketCard'));
const IEQuotaRank = React.lazy(() => import('./IEQuotaRank'));
const TrendCard = React.lazy(() => import('./TrendCard'));

@connect(({ report, loading }) => ({
  report,
  loading: loading.effects['report/fetch'],
}))
class Report extends Component {
  state = {
    loading: true,
    datePickerValue: "one-month",
    type:"all",
    flag:0,
    date:"1",
    place:"province",
    title1:"本公司进口额在全省同行业的份额历史趋势",
    title2:"本公司进口额在全省同行业的份额",
    title3:"本公司出口额在全省同行业的份额历史趋势",
    title4:"本公司出口额在全省同行业的份额",
    userId:"ff97be22-34be-41f7-b9f4-fbc88a0834a9",
    scope:"province",
  };

  

  componentDidMount() {
    const { dispatch } = this.props;
    const {date,userId,scope,type}=this.state;
    this.reqRef = requestAnimationFrame(() => {
      dispatch({
        // type: 'report/fetchSortData',
        type: 'report/fetch',
        payload:
      {
        type,
        date,
        userId,
        scope,
      },
      });
    });

    setTimeout(() => {
      this.setState({
        loading: false,
      });
    }, 2000);
  }

  componentWillUnmount() {
    console.log(this.props)
    const { dispatch } = this.props;
    dispatch({
      type: 'report/clear',
    });
    cancelAnimationFrame(this.reqRef);
  }

  handleDatePickerChange = datePickerValue => {
    const { dispatch } = this.props;
    this.setState({
      datePickerValue,
    });

    dispatch({
      type: 'report/fetchPickData',
      payload: {datePickerValue},
    });
  };

  handleSortChange = type=>{
    const {dispatch} =this.props;
    const {date,userId,scope}=this.state;
    this.setState({
        type,
    })
    dispatch({
      // type:'report/fetchSortData',
      type: 'report/fetch',
      payload:
      {
        type,
        date,
        userId,
        scope,
      },
    })
  }

  handleDateChange = date=>{
    const {dispatch} =this.props;
    const{type,userId,scope}=this.state;
    this.setState({
        date,
    })
    dispatch({
      // type:'report/fetchSortData',
      type: 'report/fetch',
      payload:
      {
        date,
        type,
        userId,
        scope,

      },
    })
  }

  handlePlaceChange = scope=>{
    console.log(scope);
    const {dispatch} =this.props;
    const{title,type,userId,date}=this.state;
    if(scope=="province"){
      this.setState({
        title1:"本公司进口额在全省同行业的份额历史趋势",
        title2:"本公司进口额在全省同行业的份额",
        title3:"本公司出口额在全省同行业的份额历史趋势",
        title4:"本公司出口额在全省同行业的份额",
    })

    }
    if(scope=="country"){
      this.setState({
        title1:"本公司进口额在全国同行业的份额历史趋势",
        title2:"本公司进口额在全国同行业的份额",
        title3:"本公司出口额在全国同行业的份额历史趋势",
        title4:"本公司出口额在全国同行业的份额",
    })

    }
   
    dispatch({
      // type:'report/fetchSortData',
      type: 'report/fetch',
      payload:
      {
        date,
        type,
        userId,
        scope,

      },
    })
  }

  

  render() {
    const { datePickerValue, type, date, title1,title2,title3,title4, loading: stateLoading } = this.state;
    const { report, loading: propsLoading } = this.props;
    const loading = stateLoading || propsLoading;
    const {
      portionData,
      portionData1,
      pieChartData,
      pieChartData1,
      importData,
      exportData,
      quotaData,
      quotaRankingData,
      trendImportData,
      trendExportData,
    } = report;

    return (
        <div className={styles.bodyDiv}>
          <div className={styles.content}>
          <div className = {styles.top}>
              <Link to="/index"><b><span>首页</span></b></Link>
              <span> > </span>
              <Link to="/index/dashboard/analysis"><span>数据服务</span></Link>
            </div>
            <GridContent>
              <Suspense fallback={null}>
                <PortionCard 
                portionData={portionData} 
                portionData1={portionData1} 
                pieChartData={pieChartData} 
                pieChartData1={pieChartData1} 
                loading={loading} 
                type={type}
                title1={title1}
                title2={title2}
                title3={title3}
                title4={title4}
                date={date}
                handlePlaceChange={this.handlePlaceChange}
                handleSortChange={this.handleSortChange}
                handleDateChange={this.handleDateChange}
                />
              </Suspense>
              <br />
              <br />
              <Suspense fallback={null}>
                <MarketCard importData={importData} exportData={exportData} loading={loading} />
              </Suspense>
              <br />
              <br />
              <Suspense fallback={null}>
                <IEQuotaRank
                  quotaData={quotaData}
                  quotaRankingData={quotaRankingData}
                  datePickerValue={datePickerValue}
                  handleDatePickerChange={this.handleDatePickerChange}
                  loading={loading}
                />
              </Suspense>
              <br />
              <br />
              <Suspense fallback={null}>
                <TrendCard
                  portionData={portionData}
                  pieChartData={pieChartData}
                  trendImportData={trendImportData}
                  trendExportData={trendExportData}
                  loading={loading}
                />
              </Suspense>
            </GridContent>
          </div>
        </div>
    );
  }
}

export default Report;
