import React, { memo } from 'react';
import { Row, Col, Card, Tabs, DatePicker } from 'antd';
import { FormattedMessage, formatMessage } from 'umi-plugin-react/locale';
import numeral from 'numeral';
import styles from './Analysis.less';
import { Bar, LineChart, PieChart, TrendLineChart } from '@/components/Charts';

const { RangePicker } = DatePicker;
const { TabPane } = Tabs;

const TrendCard = memo(({ trendImportData, trendExportData, loading }) => (
  <Card loading={loading} bordered={false} bodyStyle={{ padding: 0 }}>
    <div className={styles.salesCard}>
      <Tabs size="large" tabBarStyle={{ marginBottom: 24 }}>
        <TabPane
          tab={<FormattedMessage id="app.analysis.province" defaultMessage="全省" />}
          key="sales"
        >
          <Row>
            <Col xl={11} lg={12} md={12} sm={24} xs={24}>
              <div className={styles.salesBar}>
                <h4 className={styles.rankingTitle}>
                  <FormattedMessage
                    id="app.analysis.province.import-portion"
                    defaultMessage="本公司进口额与全省同行业的对照"
                  />
                </h4>
                <TrendLineChart
                  data={trendImportData}
                  titleMap={{
                    y1: formatMessage({ id: 'app.analysis.company.import' }),
                    y2: formatMessage({ id: 'app.analysis.province.import' }),
                  }}
                />
              </div>
            </Col>
            <Col xl={13} lg={12} md={12} sm={24} xs={24}>
              <div className={styles.salesRank}>
                <h4 className={styles.rankingTitle}>
                  <FormattedMessage
                    id="app.analysis.province.export-portion"
                    defaultMessage="本公司出口额与全省同行业的对照"
                  />
                </h4>

                <TrendLineChart
                  data={trendExportData}
                  titleMap={{
                    y1: formatMessage({ id: 'app.analysis.company.export' }),
                    y2: formatMessage({ id: 'app.analysis.province.export' }),
                  }}
                />
              </div>
            </Col>
          </Row>
        </TabPane>
        <TabPane
          tab={<FormattedMessage id="app.analysis.country" defaultMessage="全国" />}
          key="views"
        >
          <Row>
            <Col xl={12} lg={12} md={12} sm={24} xs={24}>
              <div className={styles.salesBar}>
                <h4 className={styles.rankingTitle}>
                  <FormattedMessage
                    id="app.analysis.country.import-portion"
                    defaultMessage="本公司进口额与全国同行业的对照"
                  />
                </h4>
                <TrendLineChart
                  data={trendImportData}
                  titleMap={{
                    y1: formatMessage({ id: 'app.analysis.company.import' }),
                    y2: formatMessage({ id: 'app.analysis.country.import' }),
                  }}
                />
              </div>
            </Col>
            <Col xl={12} lg={12} md={12} sm={24} xs={24}>
              <div className={styles.salesRank}>
                <h4 className={styles.rankingTitle}>
                  <FormattedMessage
                    id="app.analysis.country.export-portion"
                    defaultMessage="本公司出口额与全国同行业的对照"
                  />
                </h4>

                <TrendLineChart
                  data={trendExportData}
                  titleMap={{
                    y1: formatMessage({ id: 'app.analysis.company.export' }),
                    y2: formatMessage({ id: 'app.analysis.country.export' }),
                  }}
                />
              </div>
            </Col>
          </Row>
        </TabPane>
      </Tabs>
    </div>
  </Card>
));

export default TrendCard;
