import React, { memo } from 'react';
import { Row, Col, Card, Tabs, DatePicker } from 'antd';
import { FormattedMessage, formatMessage } from 'umi-plugin-react/locale';
import numeral from 'numeral';
import styles from './Analysis.less';
import { Bar, LineChart, PieChart,RingChart } from '@/components/Charts';

const { RangePicker } = DatePicker;
const { TabPane } = Tabs;

const MarketCard = memo(({ importData, exportData, loading }) => (
  <Card loading={loading} bordered={false} bodyStyle={{ padding: 0 }}>
    <div className={styles.salesCard}>
      <Tabs size="large" tabBarStyle={{ marginBottom: 24 }}>
        <TabPane
          tab={<FormattedMessage id="app.analysis.company" defaultMessage="本公司" />}
          key="sales"
        >
          <Row>
            <Col xl={12} lg={12} md={12} sm={24} xs={24}>
            <div className={styles.salesRank}>
                <h4 className={styles.rankingTitle} >
                  <FormattedMessage
                    id="app.analysis.import-place"
                    defaultMessage="本公司产品进口目的地分布"
                  />
                </h4>
                <RingChart data={importData}/>
              </div>
            </Col>
            <Col xl={12} lg={12} md={12} sm={24} xs={24}>
              <div className={styles.salesRank}>
                <h4 className={styles.rankingTitle}>
                  <FormattedMessage
                    id="app.analysis.export-place"
                    defaultMessage="本公司产品出口目的地分布"
                  />
                </h4>

                <RingChart data={exportData} />
              </div>
            </Col>
          </Row>
        </TabPane>
        <TabPane
          tab={<FormattedMessage id="app.analysis.province" defaultMessage="全省" />}
          key="views"
        >
          <Row>
            <Col xl={12} lg={12} md={12} sm={24} xs={24}>
            <div className={styles.salesRank}>
                <h4 className={styles.rankingTitle}>
                  <FormattedMessage
                    id="app.analysis.province.import-place"
                    defaultMessage="全省同类型产品进口目的地分布"
                  />
                </h4>

                <RingChart data={importData} />
              </div>
            </Col>
            <Col xl={12} lg={12} md={12} sm={24} xs={24}>
              <div className={styles.salesRank}>
                <h4 className={styles.rankingTitle}>
                  <FormattedMessage
                    id="app.analysis.province.export-place"
                    defaultMessage="全省同类型产品出口目的地分布"
                  />
                </h4>
                <RingChart data={exportData} />
              </div>
            </Col>
          </Row>
        </TabPane>
        <TabPane
          tab={<FormattedMessage id="app.analysis.country" defaultMessage="全国" />}
          key="views1"
        >
          <Row>
            <Col xl={12} lg={12} md={12} sm={24} xs={24}>
            <div className={styles.salesRank}>
                <h4 className={styles.rankingTitle}>
                  <FormattedMessage
                    id="app.analysis.country.import-place"
                    defaultMessage="全国同类型产品进口目的地分布"
                  />
                </h4>

                <RingChart data={importData} />
              </div>
            </Col>
            <Col xl={12} lg={12} md={12} sm={24} xs={24}>
              <div className={styles.salesRank}>
                <h4 className={styles.rankingTitle}>
                  <FormattedMessage
                    id="app.analysis.country.export-place"
                    defaultMessage="全国同类型产品出口目的地分布"
                  />
                </h4>
                <RingChart data={exportData} />
              </div>
            </Col>
          </Row>
        </TabPane>
      </Tabs>
    </div>
  </Card>
));

export default MarketCard;
