import React, { memo } from 'react';
import { Row, Col, Card, Tabs, DatePicker,Select } from 'antd';
import { FormattedMessage, formatMessage } from 'umi-plugin-react/locale';
import numeral from 'numeral';
import styles from './Analysis.less';
import { Bar, LineChart, PieChart,RingChart } from '@/components/Charts';


const { RangePicker } = DatePicker;
const { TabPane } = Tabs;
const { Option } = Select;


const PortionCard = memo(({ portionData, portionData1,pieChartData,pieChartData1, loading , handleSortChange,
  handleDateChange,handlePlaceChange,title1,title2,title3,title4}) => (
  <Card loading={loading} bordered={false} bodyStyle={{ padding: 0 }}>
    <div className={styles.salesCard}>
      <Tabs 
      tabBarExtraContent={
        <div>
          <Select defaultValue="province" style={{ width: 100 }} onChange={handlePlaceChange} >
          <Option value="province" >全省</Option>
          <Option value="country" >全国</Option>
        </Select>
        <Select defaultValue="1" style={{ width: 100 }} onChange={handleDateChange}>
          <Option value="1" >近一个月</Option>
          <Option value="3" >近三个月</Option>
          <Option value="6" >近六个月</Option>
          <Option value="12" >近一年</Option>
        </Select>
        <Select defaultValue="all" style={{ width: 100 }} onChange={handleSortChange}>
          <Option value="all" >产品类别</Option>
          <Option value="1" >1</Option>
          <Option value="2" >2</Option>
        </Select>
      </div>
      }
      size="large" 
      tabBarStyle={{ marginBottom: 24 }}>
        <TabPane
          tab={<FormattedMessage id="app.analysis.in-portion" defaultMessage="Sales" />}
          key="sales" 
        >
          <Row>
            <Col xl={12} lg={12} md={12} sm={24} xs={24}>
              <div className={styles.salesBar}>
                <LineChart
                  height={295}
                  // title={
                  //   <FormattedMessage
                  //     id="app.analysis.province.in-portion-history"
                  //     defaultMessage="Sales Trend"
                  //   />
                  // }
                  title={title1}
                  data={portionData}
                />
              </div>
            </Col>
            <Col xl={12} lg={12} md={12} sm={24} xs={24}>
              <div className={styles.salesRank}>
                <h4 className={styles.rankingTitle}>
                  {/* <FormattedMessage
                    id="app.analysis.province.in-portion-now"
                    defaultMessage="Sales Ranking"
                  /> */}
                  <FormattedMessage
                    id={title2}
                    
                  />
                </h4>

                <RingChart data={pieChartData} />
              </div>
            </Col>
          </Row>
        </TabPane>
        <TabPane
          tab={<FormattedMessage id="app.analysis.out-portion" defaultMessage="Visits" />}
          key="views" 
        >
          <Row>
            <Col xl={12} lg={12} md={12} sm={24} xs={24}>
              <div className={styles.salesBar}>
                <LineChart
                  height={295}
                  // title={
                  //   <FormattedMessage
                  //     id="app.analysis.province.out-portion-history"
                  //     defaultMessage="Sales Trend"
                  //   />
                  // }
                  title={title3}
                  data={portionData1}
                />
              </div>
            </Col>
            <Col xl={12} lg={12} md={12} sm={24} xs={24}>
              <div className={styles.salesRank}>
                <h4 className={styles.rankingTitle}>
                  {/* <FormattedMessage
                    id="app.analysis.province.out-portion-now"
                    defaultMessage="Visits Ranking"
                  /> */}
                  <FormattedMessage
                    id={title4}
                    
                  />
                </h4>
                <RingChart data={pieChartData1} />
              </div>
            </Col>
          </Row>
        </TabPane>
      </Tabs>
    </div>
  </Card>
));

export default PortionCard;
