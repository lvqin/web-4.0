import { fakeChartData, reportPickData,reportSortData,getAllData } from '@/services/report';

export default {
  namespace: 'report',

  state: {
    loading: false,
    
  },

  effects: {
    // *fetch(_, { call, put }) {
    //   // const response = yield call(fakeChartData);
    //   const response=yield call(getAllData);  //从后台拿固定数据
    //   console.log("==");
    //   console.log(response);
    //   console.log("===");

    //   yield put({
    //     type: 'save',
    //     payload: response,
    //   });
    // },
    *fetch(_, { call, put }) {
      // const response = yield call(fakeChartData);
      const response=yield call(getAllData);  //从后台拿固定数据
      console.log("==");
      console.log(response);
      console.log("===");
      // for
      


      yield put({
        type: 'save',
        payload: response,
      });
    },


    
    *fetchPickData({payload}, { call, put }) {
      const response = yield call(reportPickData, payload);
      yield put({
        type: 'save',
        payload: response,
      });
    },
    *fetchSortData({payload}, { call, put }) {
      const response = yield call(fakeChartData);
      
      response.pieChartData=[];
      response.pieChartData1=[];
      response.portionData=[];
      response.portionData1=[];

      const data = yield call(reportSortData, payload);
      console.log("====");
      console.log(data);
      console.log("====");

      for (let i = 0; i < data.inport.length; i += 1) {
        response.pieChartData.push({
          x: data.inport[i].company,
          y: data.inport[i].value,
        });
      }

      for (let i = 0; i < data.export.length; i += 1) {
        response.pieChartData1.push({
          x: data.export[i].company,
          y: data.export[i].value,
        });
      }

      for (let i = 0; i < data.import_rank.length; i += 1) {
        response.portionData.push({
          x: data.import_rank[i].date,
          y: data.import_rank[i].value,
        });
      }
      
      for (let i = 0; i < data.export_rank.length; i += 1) {
        response.portionData1.push({
          x: data.export_rank[i].date,
          y: data.export_rank[i].value,
        });
      }

      yield put({
        type: 'save',
        payload: response,
      });
    },
  },
  reducers: {
    save(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
    clear() {
      return {
        portionData: [],
        portionData1: [],
        pieChartData: [],
        pieChartData1: [],
        importData: [],
        exportData: [],
        trendImportData: [],
        trendExportData: [],
        quotaData: [],
        quotaRankingData: [],
        quotaSelectDate: [],
      };
    },
  },
};
