import React, { memo } from 'react';
import { Row, Col, Card, Tabs, Select } from 'antd';
import { FormattedMessage } from 'umi-plugin-react/locale';
// import { Chart, Geom, Axis, Tooltip, Coord, Guide } from 'bizcharts';
import styles from './Report.less';
import {
  G2,
  Chart,
  Geom,
  Axis,
  Tooltip,
  Coord,
  Label,
  Legend,
  View,
  Guide,
  Shape,
  Facet,
  Util
  
} from "bizcharts";
import DataSet from '@antv/data-set';

const cols = {
  y: {
    min: 1,
    minLimit:1,
    alias:'排名'
  },
  x: {
    range: [0, 1],
    tickCount: 5,
  }
};


const data = [
  { label: '2019-02', 公司份额: 2, 全省份额: 20, 排名: 6.8 },
  { label: '2019-03', 公司份额: 4, 全省份额: 24, 排名: 4.8 },
  { label: '2019-04', 公司份额: 6, 全省份额: 26,  排名: 1 },
  { label: '2019-05', 公司份额: 4, 全省份额: 24,  排名: 4.8 },
  { label: '2019-06', 公司份额: 2, 全省份额: 20, 排名: 6.8 },
  
];
const ds = new DataSet();
const dv = ds.createView().source(data);
dv.transform({
  type: 'fold',
  fields: ['公司份额', '全省份额'], // 展开字段集
  key: 'type', // key字段
  value: 'value', // value字段
});
const scale = {
  排名: {
    type: 'linear',
    min: 0,
    // max: 10,
  },
};

let chartIns = null;

const getG2Instance = (chart) => {
  chartIns = chart;
};

const { TabPane } = Tabs;
const { Line } = Guide;
const { Option } = Select;
const IEQuotaRank = memo(({ quotaData, quotaRankingData, datePickerValue, handleDatePickerChange, loading }) => (
  <Card loading={loading} bordered={false} bodyStyle={{ padding: 0 }}>
    <div className={styles.salesCard}>
      <Tabs 
        tabBarExtraContent={
          <div className={styles.salesExtraWrap}>
            <div className={styles.salesExtra}>
            {/* <Select defaultValue={datePickerValue} style={{ width: 120 }} onChange={handleDatePickerChange}>
              <Option value="one-month">近一个月</Option>
              <Option value="three-month">近三个月</Option>
              <Option value="six-month">近六个月</Option>
              <Option value="one-year">近一年</Option>
            </Select> */}
            </div>
          </div>
        }
        size="large" 
        tabBarStyle={{ marginBottom: 24 }}>
        <TabPane
          tab={<FormattedMessage id="app.report.in-quota" defaultMessage="进口" />}
          key="Import Quota"
        >
          <Row>
            <Col xl={12} lg={12} md={12} sm={24} xs={24}>
              <div className={styles.salesBar}>
                <h4 style={{ marginBottom: 20 }}>{`本公司进口额在全省同行业的历史排名`}</h4>

<Chart height={300}  forceFit data={dv} scale={scale} padding="auto" onGetG2Instance={getG2Instance}>
      <Legend
        custom
        allowAllCanceled
        items={[
          { value: '公司份额', marker: { symbol: 'square', fill: '#41a2fc' } },
          { value: '全省份额', marker: { symbol: 'square', fill: '#04ba65' } },
         
          { value: '排名', marker: { symbol: 'hyphen', stroke: '#fad248',  lineWidth: 3 } },
        ]}
        onClick={(ev) => {
          const item = ev.item;
          const value = item.value;
          const checked = ev.checked;
          const geoms = chartIns.getAllGeoms();
          for (let i = 0; i < geoms.length; i++) {
            const geom = geoms[i];
            if (geom.getYScale().field === value && value === '排名') {
              if (checked) {
                geom.show();
              } else {
                geom.hide();
              }
            } else if (geom.getYScale().field === 'value' && value !== '排名') {
              geom.getShapes().map((shape) => {
                if (shape._cfg.origin._origin.type == value) {
                  shape._cfg.visible = !shape._cfg.visible;
                }
                shape.get('canvas').draw();
                return shape;
              });
            }
          }
        }}
      />
      <Axis name="label" />
      <Axis name="value" position={'left'} />
      <Tooltip />
      <Geom
        type="interval"
        position="label*value"
        color={['type', (value) => {
          if (value === '公司份额') {
            return '#41a2fc';
          }
          if (value === '全省份额') {
            return '#04ba65';
          }
         
        }]}
        adjust={[{
          type: 'dodge',
          marginRatio: 1 / 32,
        }]}
      />
      <Geom type="line" position="label*排名" color="#fad248" size={2} shape={"smooth"}/>
    </Chart>

              </div>
            </Col>
            <Col xl={12} lg={12} md={12} sm={24} xs={24}>
              <div className={styles.salesBar}>
                <h4 className={styles.rankingTitle}>{`全省同行业进口额排名`}</h4>
                <Chart height={300} data={quotaData} padding={[10, 30, 30, 50]}>
                  <Coord transpose />
                  <Axis
                    name="country"
                    label={{
                      offset: 12,
                    }}
                  />
                  <Axis name="population" />
                  <Tooltip />
                  <Geom
                    type="interval"
                    position="country*population"
                    color="population"
                    tooltip={[
                      'country*population',
                      (country, population) => {
                        return {
                          // 自定义 tooltip 上显示的 title 显示内容等。
                          name: '万元',
                          title: country,
                          value: population,
                        };
                      },
                    ]}
                  />
                </Chart>
              </div>
            </Col>
          </Row>
        </TabPane>
        <TabPane
          tab={<FormattedMessage id="app.report.out-quota" defaultMessage="出口" />}
          key="Exit Quota"
        >
          <Row>
            <Col xl={12} lg={12} md={12} sm={24} xs={24}>
              <div className={styles.salesBar}>
                <h4 style={{ marginBottom: 20 }}>{`本公司出口额在全省同行业的历史排名`}</h4>
                <Chart height={300}  forceFit data={dv} scale={scale} padding="auto" onGetG2Instance={getG2Instance}>
      <Legend
        custom
        allowAllCanceled
        items={[
          { value: '公司份额', marker: { symbol: 'square', fill: '#41a2fc' } },
          { value: '全省份额', marker: { symbol: 'square', fill: '#04ba65' } },
         
          { value: '排名', marker: { symbol: 'hyphen', stroke: '#fad248',  lineWidth: 3 } },
        ]}
        onClick={(ev) => {
          const item = ev.item;
          const value = item.value;
          const checked = ev.checked;
          const geoms = chartIns.getAllGeoms();
          for (let i = 0; i < geoms.length; i++) {
            const geom = geoms[i];
            if (geom.getYScale().field === value && value === '排名') {
              if (checked) {
                geom.show();
              } else {
                geom.hide();
              }
            } else if (geom.getYScale().field === 'value' && value !== '排名') {
              geom.getShapes().map((shape) => {
                if (shape._cfg.origin._origin.type == value) {
                  shape._cfg.visible = !shape._cfg.visible;
                }
                shape.get('canvas').draw();
                return shape;
              });
            }
          }
        }}
      />
      <Axis name="label" />
      <Axis name="value" position={'left'} />
      <Tooltip />
      <Geom
        type="interval"
        position="label*value"
        color={['type', (value) => {
          if (value === '公司份额') {
            return '#41a2fc';
          }
          if (value === '全省份额') {
            return '#04ba65';
          }
         
        }]}
        adjust={[{
          type: 'dodge',
          marginRatio: 1 / 32,
        }]}
      />
      <Geom type="line" position="label*排名" color="#fad248" size={2} shape={"smooth"}/>
    </Chart>
              </div>
            </Col>
            <Col xl={12} lg={12} md={12} sm={24} xs={24}>
              <div className={styles.salesBar}>
                <h4 className={styles.rankingTitle}>{`全省同行业出口额排名`}</h4>
                <Chart height={300} data={quotaData} padding={[10, 30, 30, 50]}>
                  <Coord transpose />
                  <Axis
                    name="country"
                    label={{
                      offset: 12,
                    }}
                  />
                  <Axis name="population" />
                  <Tooltip />
                  <Geom 
                    type="interval" 
                    position="country*population"
                    color="population"
                    tooltip={[
                      'country*population',
                      (country, population) => {
                        return {
                          // 自定义 tooltip 上显示的 title 显示内容等。
                          name: '万元',
                          title: country,
                          value: population,
                        };
                      },
                    ]}
                  />
                </Chart>
              </div>
            </Col>
          </Row>
        </TabPane>
      </Tabs>
    </div>
  </Card>
));

export default IEQuotaRank;
