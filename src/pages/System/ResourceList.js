import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  Modal,
  message,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';

import styles from './UserList.less';

const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible, listRoles } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };

  const roles = listRoles() || [];
  const children = [];
  for (let i = 0; i < roles.length; i += 1) {
    children.push(<Option key={roles[i].id}>{roles[i].name}</Option>);
  }

  return (
    <Modal
      destroyOnClose
      title="新建资源"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="资源代码">
        {form.getFieldDecorator('code', {
          rules: [
            { required: true, message: '请输入至少4个字符的资源代码！', min: 4 },
            { required: true, message: '请输入不多于50个字符的资源代码！', max: 50 },
          ],
        })(<Input placeholder="请输入" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="资源名称">
        {form.getFieldDecorator('name', {
          rules: [
            { required: true, message: '请输入至少4个字符的资源名称！', min: 4 },
            { required: true, message: '请输入不多于20个字符的资源名称！', max: 20 },
          ],
        })(<Input placeholder="请输入" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="请求方法">
        {form.getFieldDecorator('method', {
          rules: [{ required: true, message: '请选择资源的请求方法！', min: 3 }],
        })(
          <Select placeholder="请选择" style={{ width: '100%' }}>
            <Option value="GET">GET</Option>
            <Option value="POST">POST</Option>
            <Option value="PUT">PUT</Option>
            <Option value="DELETE">DELETE</Option>
          </Select>,
        )}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="访问地址">
        {form.getFieldDecorator('url', {
          rules: [
            { required: true, message: '请输入至少3个字符的资源请求地址！', min: 3 },
            { required: true, message: '请输入不多于50个字符的资源请求地址！', max: 50 },
          ],
        })(<Input placeholder="请输入" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="资源描述">
        {form.getFieldDecorator('description', {
          rules: [{ message: '请输入不多于50个字符的资源描述！', max: 50 }],
        })(<Input.TextArea placeholder="请输入" />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="角色列表">
        {form.getFieldDecorator('roles', {})(
          <Select
            mode="multiple"
            size="default"
            placeholder="Please select"
            style={{ width: '100%' }}
          >
            {children}
          </Select>,
        )}
      </FormItem>
    </Modal>
  );
});

@Form.create()
class UpdateForm extends PureComponent {
  static defaultProps = {
    handleUpdate: () => {},
    handleUpdateModalVisible: () => {},
    listRoles: () => {},
    values: {},
  };

  constructor(props) {
    super(props);

    this.formLayout = {
      labelCol: { span: 7 },
      wrapperCol: { span: 13 },
    };
  }

  render() {
    const {
      updateModalVisible,
      handleUpdateModalVisible,
      values,
      form,
      handleUpdate,
      listRoles,
    } = this.props;
    const OkUpdateHandle = () => {
      form.validateFields((err, fieldsValue) => {
        if (err) return;
        form.resetFields();
        handleUpdate(fieldsValue);
      });
    };

    const children = [];
    const roles = listRoles() || [];
    for (let i = 0; i < roles.length; i += 1) {
      children.push(<Option key={roles[i].id}>{roles[i].name}</Option>);
    }

    const selected = [];
    if (values.roles)
      for (let i = 0; i < values.roles.length; i += 1) {
        selected.push(values.roles[i].id);
      }

    return (
      <Modal
        destroyOnClose
        title="修改资源"
        visible={updateModalVisible}
        onOk={OkUpdateHandle}
        onCancel={() => handleUpdateModalVisible()}
      >
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="资源代码">
          {form.getFieldDecorator('code', {
            rules: [{ required: true }],
            initialValue: values.code,
          })(<Input disabled />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="资源名称">
          {form.getFieldDecorator('name', {
            rules: [
              { required: true, message: '请输入至少4个字符的资源名称！', min: 4 },
              { required: true, message: '请输入不多于20个字符的资源名称！', max: 20 },
            ],
            initialValue: values.name,
          })(<Input placeholder="请输入" />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="请求方法">
          {form.getFieldDecorator('method', {
            rules: [{ required: true, message: '请选择资源的请求方法！', min: 3 }],
            initialValue: values.method,
          })(
            <Select placeholder="请选择" style={{ width: '100%' }}>
              <Option value="GET">GET</Option>
              <Option value="POST">POST</Option>
              <Option value="PUT">PUT</Option>
              <Option value="DELETE">DELETE</Option>
            </Select>,
          )}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="访问地址">
          {form.getFieldDecorator('url', {
            rules: [
              { required: true, message: '请输入至少3个字符的资源请求地址！', min: 3 },
              { required: true, message: '请输入不多于50个字符的资源请求地址！', max: 50 },
            ],
            initialValue: values.url,
          })(<Input placeholder="请输入" />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="资源描述">
          {form.getFieldDecorator('description', {
            rules: [{ message: '请输入不多于50个字符的资源描述！', max: 50 }],
            initialValue: values.description,
          })(<Input.TextArea placeholder="请输入" />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="角色列表">
          {form.getFieldDecorator('roles', {
            // initialValue: ['a10', 'c12'],
            initialValue: selected,
          })(
            <Select
              mode="multiple"
              size="default"
              placeholder="Please select"
              // onChange={handleChange}
              style={{ width: '100%' }}
            >
              {children}
            </Select>,
          )}
        </FormItem>
      </Modal>
    );
  }
}

/* eslint react/no-multi-comp:0 */
@connect(({ resources, loading }) => ({
  data: resources.data,
  loading: loading.models.resources,
}))
@Form.create()
class ResourceList extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    record: {},
    page: 0,
    size: 10,
  };

  columns = [
    {
      title: '资源代码',
      dataIndex: 'code',
      sorter: true,
      // render: text => <a onClick={() => this.previewItem(text)}>{text}</a>,
    },
    {
      title: '资源名称',
      dataIndex: 'name',
      sorter: true,
    },
    {
      title: '资源描述',
      dataIndex: 'description',
    },
    {
      title: '请求方法',
      dataIndex: 'method',
    },
    {
      title: '请求地址',
      dataIndex: 'url',
    },
    {
      title: '操作',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>
        </Fragment>
      ),
    },
  ];

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'resources/fetch',
      payload: {},
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      page: pagination.current - 1,
      size: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    this.setState({
      page: pagination.current - 1,
      size: pagination.pageSize,
    });

    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'resources/fetch',
      payload: params,
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    this.setState({
      formValues: {},
    });
    dispatch({
      type: 'resources/fetch',
      payload: {},
    });
  };

  toggleForm = () => {
    const { expandForm } = this.state;
    this.setState({
      expandForm: !expandForm,
    });
  };

  handleMenuClick = e => {
    const { dispatch } = this.props;
    const { selectedRows, page, size } = this.state;
    const params = { page, size };

    if (selectedRows.length === 0) return;
    switch (e.key) {
      case 'remove':
        dispatch({
          type: 'resources/remove',
          payload: {
            id: selectedRows.map(row => row.id),
          },
          callback: () => {
            this.setState({
              selectedRows: [],
            });

            // 刷新列表
            dispatch({
              type: 'resources/fetch',
              payload: params,
            });
          },
        });

        break;
      default:
        break;
    }
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      const values = {
        ...fieldsValue,
        updatedAt: fieldsValue.updatedAt && fieldsValue.updatedAt.valueOf(),
      };

      this.setState({
        formValues: values,
      });

      dispatch({
        type: 'resources/fetch',
        payload: values,
      });
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    this.setState({
      updateModalVisible: !!flag,
      record,
    });
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    const { page, size } = this.state;
    const params = { page, size };

    const roles = [];
    if (fields.roles)
      for (let i = 0; i < fields.roles.length; i += 1) {
        roles.push({ id: fields.roles[i] });
      }

    dispatch({
      type: 'resources/add',
      payload: {
        ...fields,
        roles,
      },

      callback: () => {
        // 刷新列表
        dispatch({
          type: 'resources/fetch',
          payload: params,
        });
      },
    });

    message.success('添加成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record, page, size } = this.state;
    const params = {
      page,
      size,
    };

    const roles = [];
    if (fields.roles)
      for (let i = 0; i < fields.roles.length; i += 1) {
        roles.push({ id: fields.roles[i] });
      }

    dispatch({
      type: 'resources/update',
      payload: {
        body: {
          id: record.id,
          ...fields,
          roles,
        },
      },

      callback: () => {
        dispatch({
          type: 'resources/fetch',
          payload: params,
        });
      },
    });

    message.success('修改成功');
    this.handleUpdateModalVisible();
  };

  listRoles = () => {
    const { data } = this.props;
    return data.roles;
  };

  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="资源代码">
              {getFieldDecorator('code')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="资源名称">
              {getFieldDecorator('name')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              <a style={{ marginLeft: 8 }} onClick={this.toggleForm}>
                展开 <Icon type="down" />
              </a>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  renderAdvancedForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="资源代码">
              {getFieldDecorator('code')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="资源名称">
              {getFieldDecorator('name')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="资源描述">
              {getFieldDecorator('description')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="请求方法">
              {getFieldDecorator('method')(
                <Select placeholder="请选择" style={{ width: '100%' }}>
                  <Option value="GET">GET</Option>
                  <Option value="POST">POST</Option>
                  <Option value="PUT">PUT</Option>
                  <Option value="DELETE">DELETE</Option>
                </Select>,
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="请求地址">
              {getFieldDecorator('url')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              <a style={{ marginLeft: 8 }} onClick={this.toggleForm}>
                收起 <Icon type="up" />
              </a>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  renderForm() {
    const { expandForm } = this.state;
    return expandForm ? this.renderAdvancedForm() : this.renderSimpleForm();
  }

  render() {
    const { data, loading } = this.props;
    const { selectedRows, modalVisible, updateModalVisible, record } = this.state;
    const menu = (
      <Menu onClick={this.handleMenuClick} selectedKeys={[]}>
        <Menu.Item key="remove">删除</Menu.Item>
        <Menu.Item key="approval">批量审批</Menu.Item>
      </Menu>
    );
    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
      listRoles: this.listRoles,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
      listRoles: this.listRoles,
    };

    return (
      <PageHeaderWrapper title="资源管理">
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderForm()}</div>
            <div className={styles.tableListOperator}>
              <Button icon="plus" type="primary" onClick={() => this.handleModalVisible(true)}>
                新建
              </Button>
              {selectedRows.length > 0 && (
                <span>
                  <Button>批量操作</Button>
                  <Dropdown overlay={menu}>
                    <Button>
                      更多操作 <Icon type="down" />
                    </Button>
                  </Dropdown>
                </span>
              )}
            </div>
            <StandardTable
              rowKey="id"
              selectedRows={selectedRows}
              loading={loading}
              data={data}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
            />
          </div>
        </Card>
        <CreateForm {...parentMethods} modalVisible={modalVisible} />
        <UpdateForm {...updateMethods} updateModalVisible={updateModalVisible} values={record} />
      </PageHeaderWrapper>
    );
  }
}

export default ResourceList;
