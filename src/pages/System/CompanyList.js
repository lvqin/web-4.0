import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  Modal,
  message,
} from 'antd';
import StandardTable from '@/components/StandardTable';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import GeographicView from './GeographicView';

import styles from './RoleList.less';

const FormItem = Form.Item;
const { TextArea } = Input;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const validatorGeographic = (rule, value, callback) => {
  const { province, city } = value;
  if (!province.key) {
    callback('Please input your province!');
  }
  if (!city.key) {
    callback('Please input your city!');
  }
  callback();
};

const CreateForm = Form.create()(props => {
  const { modalVisible, form, handleAdd, handleModalVisible } = props;
  const okHandle = () => {
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleAdd(fieldsValue);
    });
  };
  return (
    <Modal
      destroyOnClose
      title="新建企业"
      visible={modalVisible}
      onOk={okHandle}
      onCancel={() => handleModalVisible()}
    >
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="企业名称">
        {form.getFieldDecorator('name', {
          rules: [{ required: true }],
        })(<Input style={{ width: '100%' }} />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="信用代码">
        {form.getFieldDecorator('socialCreditCode', {
          rules: [{ required: true }],
        })(<Input style={{ width: '100%' }} />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="注册编号">
        {form.getFieldDecorator('cusRegNo', {
          rules: [{ required: true }],
        })(<Input style={{ width: '100%' }} />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="国家">
        {form.getFieldDecorator('country', {
          rules: [{ required: true }],
        })(<Input style={{ width: '100%' }} />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="行政区划">
        {form.getFieldDecorator('geographic', {
          rules: [
            {
              required: true,
              message: '请输入您的国家或地区!',
            },
            {
              validator: validatorGeographic,
            },
          ],
        })(<GeographicView />)}
      </FormItem>
      {/* <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="类型">
          {form.getFieldDecorator('type', {
            rules: [{ required: true }],
          })(<Input style={{ width: '100%' }} />)}
        </FormItem> */}
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="地址">
        {form.getFieldDecorator('address', {
          rules: [{ required: true }],
        })(<Input style={{ width: '100%' }} />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="联系人">
        {form.getFieldDecorator('contact', {
          rules: [{ required: true }],
        })(<Input style={{ width: '100%' }} />)}
      </FormItem>
      <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="电话">
        {form.getFieldDecorator('phone', {
          rules: [{ required: true }],
        })(<Input style={{ width: '100%' }} />)}
      </FormItem>
      {/* <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="企业标签">
        {form.getFieldDecorator('tags', {
          rules: [{ required: false }],
        })(<Input style={{ width: '100%' }} />)}
      </FormItem> */}
    </Modal>
  );
});

@Form.create()
class UpdateForm extends PureComponent {
  // componentDidMount(){
  //   let { dispatch,values } = this.props;
  //   dispatch({
  //     type: 'geographic/fetchCity',
  //     payload: values.province.key
  //   });
  // }

  static defaultProps = {
    handleUpdate: () => {},
    handleUpdateModalVisible: () => {},
    values: {},
  };

  constructor(props) {
    super(props);

    this.formLayout = {
      labelCol: { span: 7 },
      wrapperCol: { span: 13 },
    };
  }

  render() {
    const { updateModalVisible, handleUpdateModalVisible, values, form, handleUpdate } = this.props;
    const children = [];
    for (let i = 10; i < 36; i += 1) {
      children.push(<Option value={i.toString(36) + i}>{i.toString(36) + i}</Option>);
    }
    const OkUpdateHandle = () => {
      form.validateFields((err, fieldsValue) => {
        if (err) return;
        form.resetFields();
        handleUpdate(fieldsValue);
      });
    };

    return (
      <Modal
        destroyOnClose
        title="修改企业"
        visible={updateModalVisible}
        onOk={OkUpdateHandle}
        onCancel={() => handleUpdateModalVisible()}
      >
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="企业名称">
          {form.getFieldDecorator('name', {
            rules: [{ required: true }],
            initialValue: values.name,
          })(<Input style={{ width: '100%' }} />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="信用代码">
          {form.getFieldDecorator('socialCreditCode', {
            rules: [{ required: true }],
            initialValue: values.socialCreditCode,
          })(<Input style={{ width: '100%' }} />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="注册编号">
          {form.getFieldDecorator('cusRegNo', {
            rules: [{ required: true }],
            initialValue: values.cusRegNo,
          })(<Input style={{ width: '100%' }} />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="国家">
          {form.getFieldDecorator('country', {
            rules: [{ required: true }],
            initialValue: values.country,
          })(<Input style={{ width: '100%' }} />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="行政区划">
          {form.getFieldDecorator('geographic', {
            rules: [
              {
                required: true,
                message: '请输入您的国家或地区!',
              },
              {
                validator: validatorGeographic,
              },
            ],
            initialValue: values.geographic ? JSON.parse(values.geographic) : '',
            // initialValue:values.geographic,
          })(<GeographicView />)}
        </FormItem>
        {/* <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="类型">
          {form.getFieldDecorator('type', {
            rules: [{ required: true }],
            initialValue: values.type,
          })(<Input style={{ width: '100%' }} />)}
        </FormItem> */}
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="地址">
          {form.getFieldDecorator('address', {
            rules: [{ required: true }],
            initialValue: values.address,
          })(<Input style={{ width: '100%' }} />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="联系人">
          {form.getFieldDecorator('contact', {
            rules: [{ required: true }],
            initialValue: values.contact,
          })(<Input style={{ width: '100%' }} />)}
        </FormItem>
        <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="电话">
          {form.getFieldDecorator('phone', {
            rules: [{ required: true }],
            initialValue: values.phone,
          })(<Input style={{ width: '100%' }} />)}
        </FormItem>
        {/* <FormItem labelCol={{ span: 5 }} wrapperCol={{ span: 15 }} label="企业标签">
          {form.getFieldDecorator('tags', {
            rules: [{ required: false }],
          })(<Input style={{ width: '100%' }} />)}
        </FormItem> */}
      </Modal>
    );
  }
}

/* eslint react/no-multi-comp:0 */
@connect(({ companyManage, loading }) => ({
  data: companyManage.data,
  loading: loading.models.companyManage,
}))
@Form.create()
class RoleList extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    page: 0,
    size: 10,
  };

  columns = [
    {
      title: '企业名称',
      dataIndex: 'name',
      sorter: true,
    },
    {
      title: '社会信用代码',
      dataIndex: 'socialCreditCode',
      // sorter: true,
    },
    {
      title: '海关注册编号',
      dataIndex: 'cusRegNo',
    },
    // {
    //   title: '国家',
    //   dataIndex: 'country',
    // },
    {
      title: '操作',
      render: record => (
        <Fragment>
          <a onClick={() => this.handleUpdateModalVisible(true, record)}>修改</a>
        </Fragment>
      ),
    },
  ];

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'companyManage/fetch',
      payload: {},
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      page: pagination.current - 1,
      size: pagination.pageSize,
      ...formValues,
      ...filters,
    };
    this.setState({
      page: pagination.current - 1,
      size: pagination.pageSize,
    });

    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    dispatch({
      type: 'companyManage/fetch',
      payload: params,
    });
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    this.setState({
      formValues: {},
    });
    dispatch({
      type: 'companyManage/fetch',
      payload: {},
    });
  };

  toggleForm = () => {
    const { expandForm } = this.state;
    this.setState({
      expandForm: !expandForm,
    });
  };

  handleMenuClick = e => {
    const { dispatch } = this.props;
    const { selectedRows, page, size } = this.state;
    const params = {
      page,
      size,
    };
    if (selectedRows.length === 0) return;
    switch (e.key) {
      case 'remove':
        dispatch({
          type: 'companyManage/remove',
          payload: {
            id: selectedRows.map(row => row.id),
          },
          callback: () => {
            this.setState({
              selectedRows: [],
            });

            // 刷新列表
            dispatch({
              type: 'companyManage/fetch',
              payload: params,
            });
          },
        });
        break;
      default:
        break;
    }
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleSearch = e => {
    e.preventDefault();
    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const values = {
        ...fieldsValue,
        updatedAt: fieldsValue.updatedAt && fieldsValue.updatedAt.valueOf(),
      };

      this.setState({
        formValues: values,
      });

      dispatch({
        type: 'companyManage/fetch',
        payload: values,
      });
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleUpdateModalVisible = (flag, record) => {
    this.setState({
      updateModalVisible: !!flag,
      record,
    });
  };

  handleAdd = fields => {
    const { dispatch } = this.props;
    const { page, size } = this.state;
    const params = { page, size };
    dispatch({
      type: 'companyManage/add',
      payload: {
        // code: fields.code,
        // name: fields.name,
        // description: fields.description,
        ...fields,
      },
      callback: () => {
        dispatch({
          type: 'companyManage/fetch',
          payload: params,
        });
      },
    });

    message.success('添加成功');
    this.handleModalVisible();
  };

  handleUpdate = fields => {
    const { dispatch } = this.props;
    const { record, page, size } = this.state;
    const params = {
      page,
      size,
    };
    dispatch({
      type: 'companyManage/update',
      payload: {
        body: {
          id: record.id,
          type: fields.type,
          name: fields.name,
          address: fields.address,
          contact: fields.contact,
          country: fields.country,
          cusRegNo: fields.cusRegNo,
          geographic: fields.geographic,
          phone: fields.phone,
          socialCreditCode: fields.socialCreditCode,
          tags: fields.tags,
        },
      },
      callback: () => {
        dispatch({
          type: 'companyManage/fetch',
          payload: params,
        });
      },
    });

    message.success('修改成功');
    this.handleUpdateModalVisible();
  };

  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="企业名称">
              {getFieldDecorator('name')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="信用代码">
              {getFieldDecorator('socialCreditCode')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              <a style={{ marginLeft: 8 }} onClick={this.toggleForm}>
                展开 <Icon type="down" />
              </a>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  renderAdvancedForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="企业名称">
              {getFieldDecorator('name')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="信用代码">
              {getFieldDecorator('socialCreditCode')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="注册编号">
              {getFieldDecorator('cusRegNo')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
        </Row>
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              <a style={{ marginLeft: 8 }} onClick={this.toggleForm}>
                收起 <Icon type="up" />
              </a>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  renderForm() {
    const { expandForm } = this.state;
    return expandForm ? this.renderAdvancedForm() : this.renderSimpleForm();
  }

  render() {
    const { data, loading } = this.props;
    const { selectedRows, modalVisible, updateModalVisible, record } = this.state;
    const menu = (
      <Menu onClick={this.handleMenuClick} selectedKeys={[]}>
        <Menu.Item key="remove">删除</Menu.Item>
        <Menu.Item key="approval">批量审批</Menu.Item>
      </Menu>
    );
    const parentMethods = {
      handleAdd: this.handleAdd,
      handleModalVisible: this.handleModalVisible,
    };
    const updateMethods = {
      handleUpdateModalVisible: this.handleUpdateModalVisible,
      handleUpdate: this.handleUpdate,
    };

    return (
      <PageHeaderWrapper title="企业管理">
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderForm()}</div>
            <div className={styles.tableListOperator}>
              <Button icon="plus" type="primary" onClick={() => this.handleModalVisible(true)}>
                新建
              </Button>
              {selectedRows.length > 0 && (
                <span>
                  <Button>批量操作</Button>
                  <Dropdown overlay={menu}>
                    <Button>
                      更多操作 <Icon type="down" />
                    </Button>
                  </Dropdown>
                </span>
              )}
            </div>
            <StandardTable
              rowKey="id"
              selectedRows={selectedRows}
              loading={loading}
              data={data}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleStandardTableChange}
            />
          </div>
        </Card>
        <CreateForm {...parentMethods} modalVisible={modalVisible} />
        <UpdateForm {...updateMethods} updateModalVisible={updateModalVisible} values={record} />
      </PageHeaderWrapper>
    );
  }
}

export default RoleList;
