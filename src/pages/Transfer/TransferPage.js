import React, { Component } from 'react';
import { connect } from 'dva';

@connect(({ login }) => ({
  login,
}))
class TransferPage extends Component {
  componentDidMount() {
    // 获得URL参数
    const query_params = new URLSearchParams(this.props.location.search);
    const token = query_params.get('token');
    const { dispatch } = this.props;
    dispatch({
      type: 'login/swLogin',
      payload: { token },
    });
  }

  render() {
    return <div></div>;
  }
}
export default TransferPage;
