import React, { Component, Fragment } from 'react';
// import { formatMessage } from 'umi-plugin-react/locale';
import { connect } from 'dva';
import Link from 'umi/link';
import { Icon } from 'antd';
import DocumentTitle from 'react-document-title';
// import GlobalFooter from '@/components/GlobalFooter';
// import SelectLang from '@/components/SelectLang';
// import getPageTitle from '@/utils/getPageTitle';

import styles from './IndexsLayout.less';
import logo from '../assets/web/auxiliary_logo.png';
import userIcon from '../assets/web/service_user_icon.png';
import wxImg from '../assets/web/wx.jpg';

const token = sessionStorage.getItem('access_token');
const loginText = (
  <Fragment>请登录</Fragment>
);
const registerText = (
  <Fragment>注册</Fragment>
);
const gdsrmkabgs = "广东省人民口岸办公室";
const loginDiv = (
  <div className={styles.login}>
    <a href="/user/login">
      <img alt="logo" src={userIcon} />
      <span>{loginText}</span>
    </a>
    <Link to="/user/register">
      <span>{registerText}</span>
    </Link>
  </div>
)
// const userinfoDiv = (
//   <div className={styles.login}>
//     <Link to="/account/center">
//       <img alt="logo" src='https://gw.alipayobjects.com/zos/antfincdn/XAosXuNZyF/BiazfanxmamNRoxxVxka.png' />
//       <span>零四度</span>
//     </Link>
//   </div>
// )
// const topDiv = (
//   <Fragment>
//     {/* head */}
//     <div className={styles.top}>
//       <div className={styles.logoDiv}>
//         <Link to="/index">
//           <img alt="logo" className={styles.logo} src={logo} />
//         </Link>
//       </div>
//       {/* {loginDiv} */}
//       {userinfoDiv}
//     </div>
//     {/* head */}
//   </Fragment>
// );

@connect(({ loading,  user}) => ({
  currentUser: user.currentUser,
  currentUserLoading: loading.effects['user/fetchCurrent'],
  // project,
  // projectLoading: loading.effects['project/fetchNotice'],
  // index: newsManage.data.index,
  // listLoading: loading.effects['newsManage/newsIndex'],
}))
class Indexs extends Component {
  componentDidMount () {
    // const token = sessionStorage.getItem('access_token');
    // console.log(token);
    const { dispatch } = this.props;
    dispatch({
      type: 'user/fetchCurrent',
    });
  }

  logout = () => {
    const { dispatch } = this.props;
    dispatch({
      type: 'login/logout',
    });
  };

  toHref = (url) =>{
    const h=window.open('about:blank');
    h.location.href=url;
  }

  render() {
    const {
      currentUser,
      children,
      location: { pathname },
      breadcrumbNameMap,
    } = this.props;
    return (
      <DocumentTitle title="中国（广东）国际贸易单一窗口业务辅助平台">
        <div className={styles.container}>
          <div className={styles.content}>
            {/* head */}
            <div className={styles.top}>
              <div className={styles.logoDiv}>
                <Link to="/index">
                  <img alt="logo" className={styles.logo} src={logo} />
                </Link>
              </div>
              {token!= null && token !== ''?
              <div className={styles.login}>
                <Link to="/account/center">
                  <img alt="" src={currentUser.avatar} />
                  <span>{currentUser.name}</span>
                </Link>
                <span><a onClick={this.logout}>退出</a></span>
              </div>
              :loginDiv}
            </div>
            {/* head */}
            {children}
          </div>
          {/* bottom*/}
          <div className={styles.bottom}>
            <div className={styles.bottom_top}>
              <div className={styles.bottom_t_info}>
                <div className={styles.bt_info_top}>
                  网站信息
                </div>
                  <Link to="/"><span>网站介绍</span></Link><br/>
                  {/* <Link to="/"><span>隐私政策</span></Link><br/>
                  <Link to="/"><span>服务建议</span></Link><br/> */}
              </div>
              <div className={styles.bottom_t_info}>
                <div className={styles.bt_info_top}>
                  关于我们
                </div>
                <Link to="/"><span>联系信息</span></Link><br/>
                {/* <Link to="/"><span className={styles.tel}>400-888-1888</span></Link><br/> */}
              </div>
              <div className={styles.bottom_t_info}>
                <div className={styles.bt_info_top}>
                  友情链接
                </div>
                  {/* <Link to="/"><span>广东省人民政务</span></Link><br/> */}
                  <span  className={styles.dyck}>
                    <a onClick={() => this.toHref('https://www.singlewindow.gd.cn/')}>中国（广东）国际贸易单一窗口</a>
                    </span>
              </div>
              <div className={styles.bottom_t_wxImg}>
                <img alt="二维码" src={wxImg} />
              </div>
            </div>
            {/* <div className={styles.bottom_bot}>
              <div className={styles.bottom_b_left}>
                <span>主办：{gdsrmkabgs}</span>
                <span>承办：{gdsrmkabgs}</span>
                <span>版权所有 ：@{gdsrmkabgs}</span>
              </div>
              <div className={styles.bottom_b_right}>
                <span>京ICP证090265号</span>
                <span>京公安网备号 1101052035921</span>
              </div>
            </div> */}
          </div>
          {/* bottom*/}
        </div>
      </DocumentTitle>
    );
  }
}

export default connect(({ menu: menuModel }) => ({
  // menuData: menuModel.menuData,
  // breadcrumbNameMap: menuModel.breadcrumbNameMap,
}))(Indexs);
