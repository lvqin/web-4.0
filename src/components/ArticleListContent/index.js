import React from 'react';
import moment from 'moment';
import { Avatar } from 'antd';
import styles from './index.less';

const ArticleListContent = ({ data: { newsContent, newsTime, newsSource, href } }) => (
  <div className={styles.listContent}>
    <div className={styles.description}>
      {newsContent
        .replace(/<[^>]+>/g, '')
        .replace(/&nbsp;/gi, '')
        .replace(/\s+/g, '')
        .substr(0, 214)
        .concat('...')}
    </div>
    <div className={styles.extra}>
      来源：<a href={href}>{newsSource}</a>
      <em>时间：{moment(newsTime).format('YYYY-MM-DD HH:mm')}</em>
    </div>
  </div>
);

export default ArticleListContent;
