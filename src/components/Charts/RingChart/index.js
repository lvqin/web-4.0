import React from "react";
import {
  G2,
  Chart,
  Geom,
  Axis,
  Tooltip,
  Coord,
  Label,
  Legend,
  View,
  Guide,
  Shape,
  Facet,
  Util
} from "bizcharts";
import DataSet from "@antv/data-set";

class RingChart extends React.Component {
  render() {
    const { DataView } = DataSet;
    const { Html } = Guide;
    const { data } = this.props;
    const dv = new DataView();
    dv.source(data).transform({
      type: "percent",
      field: "y",
      dimension: "x",
      as: "percent"
    });
    const cols = {
      percent: {
        formatter: val => {
          val = (val * 100).toFixed(2) + "%";
          return val;
        }
      }
    };
    return (
      
        <Chart
          height={295}
          data={dv}
          scale={cols}
          padding={[20, -30, 0, -80]}
          forceFit
        >
          <Coord type={"theta"} radius={0.8} innerRadius={0.55} />
          <Axis name="percent" />
          <Legend
            position="right"
            offsetX={-100}
          />
          <Tooltip
            showTitle={false}
            itemTpl="<li><span style=&quot;background-color:{color};&quot; class=&quot;g2-tooltip-marker&quot;></span>{name}: {value}</li>"
          />
          <Geom
            type="intervalStack"
            position="percent"
            color="x"
            tooltip={[
              "x*percent",
              (x, percent) => {
                percent = (percent * 100).toFixed(2) + "%";
                return {
                  name: x,
                  value: percent
                };
              }
            ]}
            style={{
              lineWidth: 1,
              stroke: "#fff"
            }}
          >
            <Label
              content="percent"
              formatter={(val, x) => {
                return x.point.x + ": " + val;
              }}
            />
          </Geom>
        </Chart>
      
    );
  }
}

export default RingChart;
