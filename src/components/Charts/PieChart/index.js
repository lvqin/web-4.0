import React from 'react';
import {
  G2,
  Chart,
  Geom,
  Axis,
  Tooltip,
  Coord,
  Label,
  Legend,
  View,
  Guide,
  Shape,
  Facet,
  Util,
} from 'bizcharts';
import DataSet from '@antv/data-set';

class PieChart extends React.Component {
  render() {
    const { data } = this.props;
    const { DataView } = DataSet;
    const dv = new DataView();
    dv.source(data).transform({
      type: 'percent',
      field: 'y',
      dimension: 'x',
      as: 'percent',
    });
    const cols = {
      percent: {
        formatter: val => {
          val = val * 100 + '%';
          return val;
        },
      },
    };
    return (
      <Chart
        height={295}
        data={dv}
        scale={cols}
        padding={[20, 50, 0, 0]}
        forceFit
      >
        <Coord type="theta" radius={0.8} />
        <Axis name="percent" />
        {/* <Legend position="right" offsetY={-295 / 2 + 120} offsetX={-10} /> */}
        <Legend position="right"  offsetX={-10} />
        <Tooltip
          showTitle={false}
          itemTpl='<li><span style="background-color:{color};" class="g2-tooltip-marker"></span>{name}: {value}</li>'
        />
        <Geom
          type="intervalStack"
          position="percent"
          color="x"
          style={{
            lineWidth: 1,
            stroke: '#fff',
          }}
        >
          <Label
            content="percent"
            formatter={(val, x) => {
              return x.point.x + ': ' + val;
            }}
          />
        </Geom>
      </Chart>
    );
  }
}

export default PieChart;
